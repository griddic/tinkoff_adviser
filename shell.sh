docker build -t tinkoff_adviser_shell --cache-from tinkoff_adviser_shell .
find . | grep -E "(__pycache__|\.pyc|\.pyo$)" | xargs rm -rf
docker run -it -v $(pwd):$(pwd) -w $(pwd) tinkoff_adviser_shell bash
find . | grep -E "(__pycache__|\.pyc|\.pyo$)" | xargs rm -rf