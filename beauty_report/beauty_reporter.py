from typing import Dict

import babel.numbers
from prettytable import PrettyTable

from money import Money
from temporary import DO_NOT_BY_CURRENCIES

ACTUAL = "act"

EXPECTED = "exp"

RUB = 'rub'
USD = 'usd'


class PrettyTableDivided(PrettyTable):
    def _stringify_row(self, row, options, hrule):
        for v in row:
            if v.strip():
                return super()._stringify_row(row, options, hrule)
        return self._hrule


DEFAULT = ""


def format_money(amount):
    return babel.numbers.format_decimal(int(amount), locale='ru_RU').replace('\xa0', ' ')


def format_percentage(ratio):
    return f"{100 * ratio:.1f}%"


class PrettyReport:
    def __init__(self):
        self.free_rub = DEFAULT
        self.free_usd = DEFAULT

        self.usd_percentage_actual = DEFAULT
        self.rub_percentage_actual = DEFAULT
        self.usd_percentage_expected = DEFAULT
        self.rub_percentage_expected = DEFAULT

        self.usd_shares_actual = DEFAULT
        self.usd_bonds_actual = DEFAULT
        self.usd_shares_expected = DEFAULT
        self.usd_bonds_expected = DEFAULT
        self.rub_shares_actual = DEFAULT
        self.rub_bonds_actual = DEFAULT
        self.rub_shares_expected = DEFAULT
        self.rub_bonds_expected = DEFAULT

        self.transfer_from = DEFAULT
        self.transfer_to = DEFAULT
        self.transfer_amount: Money = Money(0, 'rub')

        self.usd_spend_on_shares = DEFAULT
        self.usd_spend_on_bonds = DEFAULT

        self.rub_spend_on_shares = DEFAULT
        self.rub_spend_on_bonds = DEFAULT

        self.rub_spend_left = DEFAULT
        self.usd_spend_left = DEFAULT

        self.positions = []

    def set_free_cash(self, free_cash: Dict[str, Money]):
        self.free_usd = format_money(free_cash[USD].amount)
        self.free_rub = format_money(free_cash[RUB].amount)

    def set_actual_currencies_balance(self, in_usd, in_rub):
        total = in_rub + in_usd
        self.rub_percentage_actual = format_percentage(in_rub / total)
        self.usd_percentage_actual = format_percentage(in_usd / total)

    def set_expected_currencies_balance(self, in_usd, in_rub):
        total = in_rub + in_usd
        self.rub_percentage_expected = format_percentage(in_rub / total)
        self.usd_percentage_expected = format_percentage(in_usd / total)

    def set_buy_shares_bonds(self, cur, to_shares: Money, to_bonds: Money):
        if cur == 'rub':
            assert self.rub_spend_on_shares == DEFAULT
            self.rub_spend_on_shares = format_money(to_shares.amount)
            self.rub_spend_on_bonds = format_money(to_bonds.amount)
            return
        if cur == 'usd':
            assert self.usd_spend_on_shares == DEFAULT
            self.usd_spend_on_shares = format_money(to_shares.amount)
            self.usd_spend_on_bonds = format_money(to_bonds.amount)
            return
        raise ValueError()

    def set_buy_left(self, cur, left: Money):
        if cur == 'rub':
            assert self.rub_spend_left == DEFAULT
            self.rub_spend_left = format_money(left.amount)
            return
        if cur == 'usd':
            assert self.usd_spend_left == DEFAULT
            self.usd_spend_left = format_money(left.amount)
            return
        raise ValueError()

    def set_balance_shares_bonds(self, cur, exp_sh, exp_b, act_sh, act_b):
        if cur == 'rub':
            assert self.rub_shares_expected == DEFAULT
            exp_total = exp_b + exp_sh
            self.rub_shares_expected = format_percentage(exp_sh / exp_total)
            self.rub_bonds_expected = format_percentage(exp_b / exp_total)
            act_total = act_b + act_sh
            self.rub_bonds_actual = format_percentage(act_b / act_total)
            self.rub_shares_actual = format_percentage(act_sh / act_total)
            return
        if cur == 'usd':
            assert self.usd_shares_expected == DEFAULT
            exp_total = exp_b + exp_sh
            self.usd_shares_expected = format_percentage(exp_sh / exp_total)
            self.usd_bonds_expected = format_percentage(exp_b / exp_total)
            act_total = act_b + act_sh
            self.usd_bonds_actual = format_percentage(act_b / act_total)
            self.usd_shares_actual = format_percentage(act_sh / act_total)
            return
        raise ValueError()

    def telegram_report(self):
        table = PrettyTableDivided(header=False,
                                   border=True,
                                   max_table_width=35,
                                   vertical_char=':',
                                   horizontal_char='·',
                                   )
        table.add_column(object(), [], 'r')
        table.add_column(object(), [], 'r')
        table.add_column(object(), [], 'r')
        divider = ['', '', '']

        # x = PrettyTable(["", "usd", "rub"])
        table.add_row(["", "usd", "rub"])
        table.add_row(['free', self.free_usd, self.free_rub])
        table.add_row([EXPECTED, self.usd_percentage_expected, self.rub_percentage_expected])
        table.add_row([ACTUAL, self.usd_percentage_actual, self.rub_percentage_actual])
        # print(x.get_string())

        # er = PrettyTable(["usd", "expected", "actual"])
        table.add_row(divider)
        table.add_row(["rub", 'shares', 'bonds'])
        table.add_row([EXPECTED, self.rub_shares_expected, self.rub_bonds_expected])
        table.add_row([ACTUAL, self.rub_shares_actual, self.rub_bonds_actual])

        table.add_row(divider)
        table.add_row(["usd", 'shares', 'bonds'])
        table.add_row([EXPECTED, self.usd_shares_expected, self.usd_bonds_expected])
        table.add_row([ACTUAL, self.usd_shares_actual, self.usd_bonds_actual])

        if not DO_NOT_BY_CURRENCIES:
            table.add_row(divider)
            table.add_row([self.transfer_from, '=>', self.transfer_to])
            table.add_row([format_money(self.transfer_amount.to(self.transfer_from).amount),
                           '=>',
                           format_money(self.transfer_amount.to(self.transfer_to).amount)])

        table.add_row(divider)
        table.add_row(['to buy', 'usd', 'rub'])
        table.add_row(['shares', self.usd_spend_on_shares, self.rub_spend_on_shares])
        table.add_row(['bonds', self.usd_spend_on_bonds, self.rub_spend_on_bonds])
        table.add_row(['left', self.usd_spend_left, self.rub_spend_left])
        parts = [table.get_string()]

        for (name, side, amount) in self.positions:
            parts.append('')
            parts.append(name)
            parts.append(f'{side} {amount}')
        return '\n'.join(parts)


if __name__ == '__main__':
    print(PrettyReport().telegram_report())
