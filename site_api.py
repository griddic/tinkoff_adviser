import logging
import re
from copy import deepcopy
from datetime import datetime
from functools import lru_cache, cached_property
from typing import List

import curlify
import requests
from requests import HTTPError

from money import Money

LOGGER = logging.getLogger(__name__)


class SiteApiError(Exception):
    pass


def company_name(show_name: str):
    name = show_name.lower()
    name = name.replace('выпуск', '')
    name = name.replace('привилегированные', '')
    name = name.replace('акции', '')
    name = name.replace('выпуск', '')
    parts = name.split()
    parts = [p for p in parts if not any(c.isdigit() for c in p)]
    parts = [p for p in parts if any(c.isalpha() for c in p)]
    name = ' '.join(parts)
    assert name, name
    return name


class Instrument:
    def __init__(self, data):
        self._raw_data = data
        if "payload" in data:
            self._raw_data = data["payload"]

    @cached_property
    def _symbol(self):
        return self._raw_data["symbol"]

    @cached_property
    def currency(self):
        return self._symbol["currency"].lower()

    @cached_property
    def show_name(self):
        return self._symbol["showName"]

    @cached_property
    def company_name(self):
        return company_name(self.show_name)

    @cached_property
    def price(self):
        return Money(self._raw_data["prices"]["last"]["value"], self._raw_data["prices"]["last"]["currency"])

    @cached_property
    def lot_size(self):
        return self._symbol["lotSize"]

    @cached_property
    def lot_price(self):
        return self.price * self.lot_size

    @cached_property
    def ticker(self):
        return self._symbol["ticker"]

    def is_valid(self):
        if "symbol" not in self._raw_data:
            LOGGER.error(f"Failed to get symbol {self._raw_data}")
            return False
        if "prices" not in self._raw_data:
            LOGGER.error(f"Failed to get prices {self._raw_data}")
            return False
        return True


class Bond(Instrument):
    @cached_property
    def maturity_date(self):
        def extract_forward(date_str):
            return datetime.strptime(date_str, '%Y-%m-%d').date()

        def extract_backward(date_str):
            return datetime.strptime(date_str, '%d.%m.%Y').date()

        dates = []
        match = re.search(r"Дата погашения: (\d\d.\d\d.\d\d\d\d)", self.full_description)
        if match:
            dates.append(extract_backward(match.group(1)))
        for k in ["matDate", "endDate", "dateToClient", "buyBackDate"]:
            if k in self._raw_data:
                dates.append(extract_forward(self._raw_data[k][:10]))
        assert len(dates) > 0
        return max(dates)

    @cached_property
    def days_to_maturity(self):
        return (self.maturity_date - datetime.now().date()).days

    @cached_property
    def maturity_year(self):
        return self.maturity_date.year

    @cached_property
    def date_to_client(self):
        return int(self._raw_data["dateToClient"][:4])

    @cached_property
    def currency(self):
        return self._symbol["currency"].lower()

    @cached_property
    def show_name(self):
        return self._symbol["showName"]

    @cached_property
    def subordinated(self):
        return self._raw_data.get("subordinated", False)

    @cached_property
    def yield_to_maturity(self):
        return self._raw_data["yieldToMaturity"]

    @cached_property
    def yield_to_client(self):
        return self._raw_data["yieldToClient"]

    @cached_property
    def yield_(self):
        yields = []
        for k, v in self._raw_data.items():
            if "yield" in k.lower():
                yields.append(float(v))
        if len(yields) <= 0:
            LOGGER.error(f"No YIELD for {self} {self.currency}")
            return -100
        assert len(yields) > 0, repr(self)
        return min(yields)

    @cached_property
    def rate(self):
        return self._raw_data["rate"]

    @cached_property
    def reliable(self):
        return self._raw_data["reliable"]

    @cached_property
    def profitable(self):
        return self._raw_data["profitable"]

    @cached_property
    def tax_free(self):
        return self._raw_data["taxFree"]

    @cached_property
    def full_description(self):
        return self._raw_data["symbol"].get("fullDescription", "")

    @cached_property
    def floating_coupon(self):
        if "КУПОН - Переменный" in self.full_description:
            return True
        return self._raw_data["floatingCoupon"]

    @cached_property
    def lot_size(self):
        return self._symbol["lotSize"]

    @cached_property
    def nkd(self):
        return Money(self._raw_data["nkd"], self.currency)

    @cached_property
    def price(self):
        return super().price + self.nkd

    @cached_property
    def price_no_nkd(self):
        return super().price

    @cached_property
    def is_serial(self):
        return self._raw_data['serial']

    @cached_property
    def has_amortization(self):
        return self.is_serial

    def __repr__(self):
        return f"Bond({self.show_name})"


class Share(Instrument):
    def __repr__(self):
        return f"Share({self.show_name})"


class Currency(Instrument):
    def __init__(self, data, openapi_instrument):
        super().__init__(data)
        self.openapi_instrument = openapi_instrument

    def __repr__(self):
        return f"Currency({self.show_name})"

    @cached_property
    def lot_size(self):
        return self.openapi_instrument["lot"]

    @cached_property
    def ticker(self):
        return self.openapi_instrument["ticker"]

    @cached_property
    def figi(self):
        return self.openapi_instrument["figi"]


class SiteApi:
    base_url = "https://api.tinkoff.ru/trading"

    @classmethod
    def _url(cls, tail):
        return cls.base_url + tail

    @classmethod
    def _post(cls, url, data):
        try:
            resp = requests.post(cls._url(url), json=data)
        except HTTPError as e:
            LOGGER.error(str(e))
            LOGGER.error(f"Get request to {url} with json {data} failed.")
            raise
        LOGGER.debug(f"curl: {curlify.to_curl(resp.request)}")
        LOGGER.debug(f"Response code: {resp.status_code}; Response text: {resp.text}")
        return resp.json()["payload"]

    @classmethod
    @lru_cache()
    def get_all_bonds(cls) -> List[Bond]:
        url = "/bonds/list"

        touch_json = {"start": 0, "end": 1,
                      "country": "All",
                      "orderType": "Desc", "sortType": "ByYieldToClient"}
        total = cls._post(url, data=touch_json)["total"]
        all_bonds_json = deepcopy(touch_json)
        all_bonds_json["end"] = total + 10
        return [Bond(d) for d in cls._post(url, data=all_bonds_json)["values"]]

    @classmethod
    @lru_cache()
    def usd_rub_exchange(cls):
        url = "/currency/get"
        data = {"ticker": "USDRUB"}
        return cls._post(url, data)["prices"]["last"]["value"]

    @classmethod
    @lru_cache()
    def usd_rub(cls):
        url = "/currency/get"
        data = {"ticker": "USDRUB"}
        return cls._post(url, data)

    @classmethod
    @lru_cache()
    def share_by_ticker(cls, ticker):
        url = "/stocks/get"
        data = {"ticker": ticker}
        return cls._post(url, data)

    @classmethod
    @lru_cache()
    def etf_by_ticker(cls, ticker):
        url = "/etfs/get"
        data = {"ticker": ticker}
        return cls._post(url, data)

    @classmethod
    @lru_cache()
    def bond_by_ticker(cls, ticker):
        all_bonds = cls.get_all_bonds()
        bond = [b for b in all_bonds if b.ticker == ticker]
        assert len(bond) == 1, ticker
        return bond[0]
