# pylint: disable=redefined-outer-name
import random
import string

import pytest

from money import Money


@pytest.fixture(autouse=True)
def clean_exchange():
    Money.exchange = {}


@pytest.fixture
def currency():
    return ''.join(random.sample(string.ascii_lowercase, 3))


@pytest.fixture
def currency2(currency):
    while True:
        cur2 = ''.join(random.sample(string.ascii_lowercase, 3))
        if cur2 != currency:
            return cur2


def test_addition(currency):
    assert Money(1, currency) + Money(2, currency) == Money(3, currency)
    assert Money(1, currency) + Money(2., currency) == Money(3, currency)
    assert Money(2, currency) + 0 == Money(2, currency)
    assert 0 + Money(2, currency) == Money(2, currency)


def test_subtraction(currency):
    assert Money(3, currency) - Money(2, currency) == Money(1, currency)
    assert Money(3, currency) - Money(2., currency) == Money(1, currency)
    assert Money(2, currency) - 0 == Money(2, currency)
    # assert 0 - Money(2, currency) == Money(-2, currency)


def test_multiplication(currency):
    assert 5 * Money(3, currency) == Money(15, currency)
    assert 5. * Money(3, currency) == Money(15, currency)
    assert Money(3, currency) * 5 == Money(15, currency)


def test_exchange(currency, currency2):
    Money(1, currency) << Money(3, currency2)
    assert Money(1, currency) == 3 * Money(1, currency2)
    assert Money(1, currency).to(currency2) == 3 * Money(1, currency2)
    assert Money(1, currency).to(currency2).amount == 3


def test_all(currency, currency2):
    exch = 1.1
    Money(1, currency) << exch * Money(1, currency2)
    cur1 = Money(1, currency)
    cur2 = Money(1, currency2)
    assert cur1 > cur2
    # pylint: disable=comparison-with-itself
    assert cur1 >= cur1
    assert cur1 > 0
    assert cur2 > 0
    assert cur1 - cur2 == 0.1 * cur2
    assert cur1 - cur2 == (0.1 * cur2).to(cur1.currency)
