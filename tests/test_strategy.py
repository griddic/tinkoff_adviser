# pylint: disable=redefined-outer-name
from datetime import datetime, timedelta

import pytest

from money import RUB, USD
from strategy import Strategy


@pytest.fixture()
def goal():
    begin = (datetime.now() - timedelta(days=2)).date()
    end = (datetime.now() + timedelta(days=2)).date()
    goal_ = {
        "goal1": {
            "time": {
                "to": end,
                "from": begin
            },
            "price": {
                "amount": 100,
                "currency": "rub"
            }
        }
    }
    return goal_


def test_limits():
    raw = {
        "relative limits": {
            "overall": 0.5,
            "monthly": 0.1
        }
    }
    strategy = Strategy(raw, 100 * RUB)
    assert strategy.limits.monthly == 10 * RUB
    assert strategy.limits.overall == 50 * RUB


def test_currencies_weights():
    raw = {
        "weights":
            {
                "rub": {
                    "w": 60,
                    "bonds": 80,
                    "shares": 120,
                },
                "usd": {
                    "w": 140,
                    "bonds": 20,
                    "shares": 180
                }}
    }
    strategy = Strategy(raw, 100 * RUB)
    USD << 60 * RUB
    assert strategy.total["rub"].total == 30 * RUB
    assert strategy.total["usd"].total == 70 * RUB


def test_bond_shares_weights():
    raw = {
        "weights": {
            "rub": {
                "w": 70,
                "bonds": 80,
                "shares": 120,
            },
            "usd": {
                "w": 0,
            }
        }
    }
    strategy = Strategy(raw, 100 * RUB)
    assert strategy.total["rub"].bonds == 40 * RUB
    assert strategy.total["rub"].shares == 60 * RUB


def test_goals_affects_bond_share_distribution(goal):
    raw = {
        "weights": {
            "rub": {
                "w": 70,
                "bonds": 50,
                "shares": 50,
            },
            "usd": {
                "w": 0
            }
        },
        "goals": goal
    }
    strategy = Strategy(raw, 150 * RUB)
    assert strategy.total["rub"].bonds == 100 * RUB
    assert strategy.total["rub"].shares == 50 * RUB


def test_goals_affects_currencies_distribution(goal):
    raw = {
        "weights": {
            "usd": {
                "w": 1,
            },
            "rub": {
                "w": 1,
            }
        },
        "goals": goal
    }
    USD << 60 * RUB
    strategy = Strategy(raw, 150 * RUB)
    assert strategy.total["rub"].total == 100 * RUB
    assert strategy.total["usd"].total == 50 * RUB


def test_goals_affects_everything(goal):
    raw = {
        "weights": {
            "usd": {
                "w": 1,
            },
            "rub": {
                "w": 1,
                "shares": 1,
                "bonds": 1,
            }
        },
        "goals": goal
    }
    USD << 60 * RUB
    strategy = Strategy(raw, 150 * RUB)
    assert strategy.total["rub"].total == 100 * RUB
    assert strategy.total["usd"].total == 50 * RUB
    assert strategy.total["rub"].shares == 25 * RUB
    assert strategy.total["rub"].bonds == 75 * RUB
