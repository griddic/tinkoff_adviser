import pytest

from advise import split_message_into_chunks


@pytest.mark.parametrize("message",
                         [
                             "Hello, World!",
                             "Kukcha \n" * 10,
                             "Kukcha \n" * 8000,
                         ])
def test_splitting(message):
    chunks = split_message_into_chunks(message)
    if message != ''.join(chunks):
        raise AssertionError('Chunks are not equal to string.')
    for chunk in chunks:
        if len(chunk.encode('utf-8')) > 4000:
            raise AssertionError('Chunk is too long.')
