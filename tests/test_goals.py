from datetime import datetime, timedelta

from money import RUB
from strategy import Goal


def test_maturity():
    now = datetime.now()
    begin = (now - timedelta(days=2)).date()
    end = (now + timedelta(days=2)).date()
    raw = {
        "time": {
            "to": end,
            "from": begin,
        },
        "price": {
            "amount": 100,
            "currency": "rub",
        }
    }
    goal = Goal("test goal", raw)
    assert goal.current_reserve == 50 * RUB
