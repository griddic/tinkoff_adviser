import babel.numbers


class Money:
    exchange = {}

    def __init__(self, amount, currency: str):
        self.amount = amount
        self.currency = currency.lower()

    def to(self, currency):
        if self.currency == currency:
            return Money(self.amount, self.currency)
        new_amount = self.amount * self.exchange[".".join([self.currency, currency])]
        return self.__class__(new_amount, currency)

    def __lshift__(self, other):
        self.exchange[".".join([self.currency, other.currency])] = other.amount / self.amount
        self.exchange[".".join([other.currency, self.currency])] = self.amount / other.amount

    def __rmul__(self, multiplier):
        if not isinstance(multiplier, (int, float)):
            raise ValueError("Can multiply Money only on int or float.")
        return self.__class__(self.amount * multiplier, self.currency)

    def __mul__(self, multiplier):
        return self.__class__(self.amount * multiplier, self.currency)

    def __add__(self, other):
        if other == 0:
            return self
        if not isinstance(other, Money):
            raise ValueError(f"Can add only Money to Money. Got {other}")
        if self.currency != other.currency:
            other_in_self_currency = other.to(self.currency)
            return self.__class__(self.amount + other_in_self_currency.amount, self.currency)
        return self.__class__(self.amount + other.amount, self.currency)

    def __radd__(self, other):
        if other == 0:
            return self
        raise ValueError(f"Can add only Money to Money. Got {other}")

    def __eq__(self, other):
        if isinstance(other, int):
            if other == 0:
                return self.amount == 0
            raise ValueError(f"Can not compare with {other}")
        other_in_self_currency = other.to(self.currency)
        return abs(other_in_self_currency.amount - self.amount) < 10**(-6)

    def __truediv__(self, other):
        if isinstance(other, Money):
            other_in_self_currency = other.to(self.currency)
            return self.amount / other_in_self_currency.amount
        if isinstance(other, (float, int)):
            return self.__class__(self.amount / other, self.currency)
        raise ValueError("Can divide Money only on float or int or Money.")

    def __le__(self, other):
        if other == 0:
            return self.amount <= 0
        other_in_self_currency = other.to(self.currency)
        return self.amount <= other_in_self_currency.amount

    def __lt__(self, other):
        if other == 0:
            return self.amount < 0
        other_in_self_currency = other.to(self.currency)
        return self.amount < other_in_self_currency.amount

    def __gt__(self, other):
        if other == 0:
            return self.amount > 0
        other_in_self_currency = other.to(self.currency)
        return self.amount > other_in_self_currency.amount

    def __ge__(self, other):
        if other == 0:
            return self.amount >= 0
        other_in_self_currency = other.to(self.currency)
        return self.amount >= other_in_self_currency.amount

    def __sub__(self, other):
        if other == 0:
            return self.__class__(self.amount, self.currency)
        other_in_self_currency = other.to(self.currency)
        return self.__class__(self.amount - other_in_self_currency.amount, self.currency)

    def copy(self):
        return Money(self.amount, self.currency)

    def __str__(self):
        return babel.numbers.format_currency(self.amount, self.currency, locale='ru_RU').replace('\xa0', ' ')

    def __repr__(self):
        return self.__str__()

    def __hash__(self):
        return hash((self.amount, self.currency))


RUB = Money(1, "rub")
USD = Money(1, "usd")


def test():
    assert 100 * RUB + 100 * RUB == 200 * RUB

    assert 5 * 100 * RUB == 500 * RUB

    USD << 10 * RUB
    assert 10 * USD == 100 * RUB


if __name__ == "__main__":
    test()
