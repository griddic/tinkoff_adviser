import logging
import time
from datetime import datetime, timedelta
from functools import lru_cache, cached_property
from json import JSONDecodeError
from typing import List

import curlify
import pytz
import requests
from requests.auth import AuthBase
from requests.exceptions import HTTPError

from money import Money
from settings import CONFIG
from site_api import SiteApi, Currency, company_name
from temporary import AVERAGE_POSITIONS_PRICES, EQUITY_WITHOUT_PRICES

LOGGER = logging.getLogger(__name__)


class InstrumentType:
    BOND = "Bond"
    STOCK = "Stock"
    CURRENCY = "Currency"


class AuthBearer(AuthBase):

    def __init__(self, token):
        self.token = token

    def __call__(self, request):
        request.headers["Authorization"] = f"Bearer {self.token}"
        return request


class Position:
    def __init__(self, data, client):
        self.data = data
        self.empty = False
        self.client = client

    @cached_property
    def _average_position_price(self):
        if app := self.data.get("averagePositionPrice"):
            return app
        if app := AVERAGE_POSITIONS_PRICES.get(self.ticker):
            return app
        raise OpenApiError(f"No average price for position {self.ticker} {self.name}")

    @cached_property
    def currency(self):
        return self._average_position_price["currency"]

    @cached_property
    def abs_yield(self):
        return Money(self.data["expectedYield"]["value"], self.currency)

    @cached_property
    def average_price(self):
        return Money(self._average_position_price["value"], self.currency)

    @cached_property
    def balance(self):
        return self.data["balance"]

    @cached_property
    def lots(self):
        return self.data["lots"]

    @cached_property
    def lot_size(self):
        return self.balance / self.lots

    @cached_property
    def ticker(self):
        return self.data.get("ticker")

    @cached_property
    def figi(self):
        return self.data.get("figi")

    @cached_property
    def name(self) -> str:
        return self.data.get("name")

    @cached_property
    def company_name(self):
        return company_name(self.name)

    @cached_property
    def cost_no_nkd(self) -> Money:
        try:
            price = self.price_from_site
        except (requests.HTTPError, AssertionError):  # TODO: fix AssertionError
            LOGGER.debug(f"Can't extract price from site for : {self.ticker}")
            price = self.price_from_candles
        # return self.balance * self.price_from_site
        return self.balance * price

    @cached_property
    def price_from_site(self):
        if self.type_ == "Stock":
            data = SiteApi.share_by_ticker(self.ticker)
            price_data = data["prices"]["last"]
            return Money(price_data["value"], price_data["currency"])
        if self.type_ == "Bond":
            bond = SiteApi.bond_by_ticker(self.ticker)
            return bond.price
        if self.type_ == "Etf":
            etf = SiteApi.etf_by_ticker(self.ticker)
            price_data = etf["prices"]["last"]
            return Money(price_data["value"], price_data["currency"])
        msg = f"Unknown instrument type: {self.type_}"
        LOGGER.error(msg)
        raise ValueError(msg)

    @cached_property
    def price_from_candles(self) -> Money:
        today = datetime.now(tz=pytz.timezone("Europe/Moscow"))
        tomorrow = today + timedelta(days=1)
        month_ago = today - timedelta(days=31)
        candles = self.client.candles(from_=month_ago, to=tomorrow, figi=self.figi)
        if len(candles) == 0:
            message = f'No candles found for ticker: {self.ticker} {self.name} {self.currency}.'
            LOGGER.error(message)
            if self.ticker in EQUITY_WITHOUT_PRICES:
                amount = EQUITY_WITHOUT_PRICES[self.ticker]
            else:
                raise OpenApiError(message)
        else:
            amount = candles[-1]['c']
        return Money(amount, self.currency)

    @cached_property
    def payed(self):
        return self.balance * self.average_price

    @cached_property
    def type_(self):
        return self.data.get("instrumentType")

    def __repr__(self):
        return f"Position({self.ticker})"


class PositionsComposer:
    def __init__(self, positions):
        self._positions = positions

    @cached_property
    def cost_no_nkd(self):
        return sum(p.cost_no_nkd for p in self._positions)

    @cached_property
    def payed(self):
        return sum(p.payed for p in self._positions)


class Operation:
    def __init__(self, data):
        self._data = data

    @cached_property
    def currency(self):
        return self._data['currency']

    @cached_property
    def date(self):
        time_, tz = self._data['date'].split('+')
        if '.' in time_:
            time_, mil_sec = time_.split('.')
        else:
            mil_sec = None
        iso = time_
        if mil_sec:
            iso = time_ + '.' + mil_sec.ljust(6, '0')
        if tz:
            iso = iso + '+' + tz
        return datetime.fromisoformat(iso)

    @cached_property
    def figi(self):
        return self._data['figi']

    @cached_property
    def id(self):
        return self._data['id']

    @cached_property
    def instrument_type(self):
        return InstrumentType(self._data['instrumentType'])

    @cached_property
    def is_margin_call(self):
        map_ = {'False': False,
                'True': True}
        return map_[self._data['isMarginCall']]

    @cached_property
    def operation_type(self):
        return self._data['operationType']

    @cached_property
    def payment(self):
        amount = self._data['payment']
        return Money(amount, self.currency)

    @cached_property
    def status(self):
        return self._data['status']

    def __getitem__(self, item):
        return self._data[item]

    def __repr__(self):
        return f'Operation<{self.operation_type}, {self.date.date()}, {self.payment}>'


class OpenApiError(Exception):
    pass


class Client:
    def __init__(self, token, base_url):
        self.base_url = base_url
        self.session = requests.sessions.Session()
        self.session.auth = AuthBearer(token)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.session.close()

    @classmethod
    def sandbox(cls):
        return cls(CONFIG.tinkoff_sandbox_token, "https://api-invest.tinkoff.ru/openapi/sandbox")

    @classmethod
    def broker(cls):
        return cls(CONFIG.tinkoff_main_token, "https://api-invest.tinkoff.ru/openapi")

    # @retry(wait_exponential_multiplier=1000, wait_exponential_max=10000, stop_max_delay=30000)
    # @retry(wait_fixed=10000, stop_max_attempt_number=3)
    def get(self, url, params=None):
        for i in range(10):
            try:
                resp = self.session.get(url, params=params)
            except HTTPError as e:
                LOGGER.error(str(e))
                LOGGER.error(f"Get request to {url} with params {params} failed.")
                raise
            LOGGER.debug(f"curl: {curlify.to_curl(resp.request)}")
            LOGGER.debug(f"Response code: {resp.status_code}; Response text: {resp.text}")
            if resp.status_code == 429:
                # TODO: нужна более общая обработка ошибок
                duration = 3 * (i + 1)
                LOGGER.error(f"Too many requests. Will sleep for {duration} seconds. URL: {url}")
                time.sleep(duration)
            elif resp.status_code == 503:
                duration = 3 * (i + 1)
                LOGGER.error(f"Service is temporarily unavailable. Will sleep for {duration} seconds.")
                time.sleep(duration)
            else:
                break

        try:
            data = resp.json()
        except JSONDecodeError as e:
            LOGGER.error(f"Response code: {resp.status_code}.")
            LOGGER.error(f"Can't convert to json: {resp.text}.")
            raise OpenApiError(f"Can't convert to json: {resp.text}") from e
        if data["status"].lower() == "error":
            raise OpenApiError(data)
        return data["payload"]

    def post(self, url, params=None, json=None):
        try:
            resp = self.session.post(url, params=params, json=json)
        except HTTPError as e:
            LOGGER.error(str(e))
            LOGGER.error(f"Post request to {url} with params {params} and json {json} failed.")
            raise
        LOGGER.debug(f"curl: {curlify.to_curl(resp.request)}")

        data = resp.json()
        if data["status"].lower() == "error":
            LOGGER.error(data)
            raise Exception("Api error.")
        return resp.json()["payload"]

    @lru_cache()
    def portfolio(self):
        url = self.base_url + "/portfolio"
        return self.get(url)

    def orders(self):
        url = self.base_url + "/orders"
        return self.get(url)

    def lots_requested_to_buy(self, figi):
        orders = self.orders()
        orders = [o for o in orders if o["figi"] == figi]
        orders = [o for o in orders if o["operation"] == "Buy"]
        lots_waited_to_be_bought = 0
        for order in orders:
            lots_waited_to_be_bought += order["requestedLots"] - order["executedLots"]
        return lots_waited_to_be_bought

    def place_limit_order(self, figi, operation, lots, price):
        LOGGER.info(f"Placing order {figi} {operation} {lots} {price} ")
        url = self.base_url + "/orders/limit-order"
        return self.post(url, params={"figi": figi}, json={
            "lots": lots,
            "operation": operation,
            "price": price
        })

    @lru_cache()
    def position(self, ticker):
        positions = [p for p in self.positions if p.ticker == ticker]
        if len(positions) == 0:
            return None
        if len(positions) == 1:
            return positions[0]
        return PositionsComposer(positions)

    @cached_property
    def positions(self):
        return [Position(p, self) for p in self.portfolio()["positions"]]

    @property
    def equities_positions(self):
        return filter(lambda p: p.type_.lower() != "currency", self.positions)

    def filter_positions(self, currency=None, instrument_type=None) -> List[Position]:
        positions = self.positions
        if currency:
            positions = [p for p in positions if p.currency.lower() == currency.lower()]
        if instrument_type:
            positions = [p for p in positions if p.type_.lower() == instrument_type.lower()]
        else:
            positions = [p for p in positions if p.type_.lower() != "currency"]
        return positions

    @lru_cache()
    def get_instrument_by_ticker(self, ticker):
        resp = self.get(self.base_url + "/market/search/by-ticker", params={"ticker": ticker})
        if resp["total"] == 0:
            msg = f"No instrument found for ticker: '{ticker}'."
            LOGGER.error(msg)
            raise OpenApiError(msg)
        if resp["total"] > 1:
            msg = f"Several instrument found for ticker: '{ticker}.'"
            LOGGER.error(msg)
            raise OpenApiError(msg)
        return resp["instruments"][0]

    @lru_cache()
    def figi_from_ticker(self, ticker):
        instrument = self.get_instrument_by_ticker(ticker)
        if not instrument["figi"]:
            msg = f"No instrument found for ticker: '{ticker}'."
            LOGGER.error(msg)
            raise OpenApiError(msg)
        return instrument["figi"]

    @lru_cache()
    def get_instrument_by_figi(self, figi):
        return self.get(self.base_url + "/market/search/by-figi", params={"figi": figi})

    def currency_by_figi(self, figi):
        return self.get_instrument_by_figi(figi)["currency"]

    def register_sandbox(self):
        url = self.base_url + "/sandbox/register"
        return self.post(url)

    def set_rub_balance(self, balance):
        url = self.base_url + "/sandbox/currencies/balance"
        data = {
            "currency": "RUB",
            "balance": balance
        }
        return self.post(url, json=data)

    def set_usd_balance(self, balance):
        url = self.base_url + "/sandbox/currencies/balance"
        data = {
            "currency": "USD",
            "balance": balance
        }
        return self.post(url, json=data)

    def currencies(self):
        url = self.base_url + "/portfolio/currencies"
        return self.get(url)

    def free_cash(self):
        ans = {}
        for cur in self.currencies()["currencies"]:
            currency = cur["currency"].lower()
            balance = cur["balance"]
            blocked = 0
            if "blocked" in cur:
                blocked = cur["blocked"]
            assert balance >= blocked
            ans[currency] = Money(balance - blocked, currency)
        return ans

    def free_cash_with_blocked(self):
        ans = {}
        for cur in self.currencies()["currencies"]:
            currency = cur["currency"].lower()
            balance = cur["balance"]
            ans[currency] = Money(balance, currency)
        return ans

    @lru_cache()
    def operations(self, from_=None, to=None, figi=None):
        now = datetime.now(pytz.timezone("Europe/Moscow"))
        from_ = from_ or datetime(2015, 1, 1)
        to = to or now + timedelta(days=1)

        time_format = "%Y-%m-%dT%H:%M:%S.000000+03:00"
        url = self.base_url + "/operations"
        params = {"from": from_.strftime(time_format), "to": to.strftime(time_format)}
        if figi is not None:
            params["figi"] = figi
        return [Operation(data) for data in self.get(url, params=params)["operations"]]

    def bought_last_month(self, figi):
        return self.bought_last(figi, timedelta(days=30))

    def bought_last(self, figi, time_shift):
        now = datetime.now(pytz.timezone("Europe/Moscow"))
        begin = now - time_shift
        end = now + timedelta(days=1)
        operations = self.operations(begin, end, figi)
        sum_ = 0
        currency = self.currency_by_figi(figi)
        for operation in operations:
            if "Buy" not in operation.operation_type:
                continue
            if operation["status"] != "Done":
                continue
            if currency != operation["currency"]:
                raise ValueError("Different currencies for instrument")
            sum_ += operation["payment"]
        return Money(-sum_, currency)

    def last_bought_date(self, figi, name=''):
        operations = sorted(self.operations(figi=figi),
                            key=lambda o: o.date)
        buy_operations = [o for o in operations if 'Buy' in o.operation_type]
        if len(buy_operations) == 0:
            LOGGER.warning(f'Never bought {figi=} {name=}')
            return None
        return buy_operations[-1].date

    def first_operation(self, figi):
        operations = sorted(self.operations(figi=figi),
                            key=lambda o: o.date)
        return operations[0]

    @cached_property
    def portfolio_cost_no_nkd(self):
        cost = sum(e.cost_no_nkd for e in self.equities_positions)
        for cash in self.free_cash().values():
            cost += cash
        return cost

    @lru_cache()
    def candles(self, *, figi, from_, to, interval='month'):
        today = datetime.now(tz=pytz.timezone("Europe/Moscow")).date()
        from_ = from_ or (today - timedelta(days=31))
        to = to or (today + timedelta(days=1))

        candles = self.get(self.base_url + "/market/candles", params={
            "figi": figi,
            "from": from_.isoformat(),
            "to": to.isoformat(),
            "interval": interval
        })["candles"]
        return candles


def usd_rub_instrument():
    ticker = "USD000UTSTOM"
    return Currency(SiteApi.usd_rub(), Client.broker().get_instrument_by_ticker(ticker))
