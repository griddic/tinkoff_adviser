FROM python:3.10-slim


COPY requirements.txt /tmp/requirements.txt
RUN pip install -r /tmp/requirements.txt
RUN echo "alias advise=\"python advise.py\"" >> ~/.bashrc
RUN echo "alias make=\"python advise.py\"" >> ~/.bashrc
RUN echo "alias lint=\"pylint --fail-under=9.9 **/*.py *.py --rcfile=pylint.ini\"" >> ~/.bashrc

