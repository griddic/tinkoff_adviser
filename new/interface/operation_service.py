from clients import operations_pb2 as messages
from clients import operations_pb2_grpc as operations_grpc


class OperationService:
    def __init__(self, stub: operations_grpc.OperationsServiceStub):
        self.stub = stub

    def get_portfolio(self, account_id) -> messages.PortfolioResponse:
        return self.stub.GetPortfolio(
            messages.PortfolioRequest(
                account_id=account_id
            )
        )