from typing import List, Collection

from clients import users_pb2_grpc as user_grpc
from clients import users_pb2 as messages


class UserService:
    def __init__(self, stub: user_grpc.UsersServiceStub):
        self.stub = stub

    def get_my_info(self) -> messages.GetInfoResponse:
        return self.stub.GetInfo(
            messages.GetInfoRequest(),
        )

    def get_accounts(self) -> Collection[messages.Account]:
        resp: messages.GetAccountsResponse = self.stub.GetAccounts(
            messages.GetAccountsRequest()
        )
        return resp.accounts