from functools import lru_cache

from clients import instruments_pb2 as messages
from clients import instruments_pb2_grpc as instruments_grpc


class InstrumentsService:
    def __init__(self, stub: instruments_grpc.InstrumentsServiceStub):
        self.stub = stub

    @lru_cache
    def get_by_figi(self, figi) -> messages.Instrument:
        req = messages.InstrumentRequest(
            id_type=messages.InstrumentIdType.INSTRUMENT_ID_TYPE_FIGI,
            id=figi,
        )
        resp: messages.InstrumentResponse = self.stub.GetInstrumentBy(req)
        return resp.instrument