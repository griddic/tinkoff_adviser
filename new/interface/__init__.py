from functools import cached_property

import grpc
from .user_service import UserService
from .operation_service import OperationService
from .instruments_service import InstrumentsService
from .marketdata_service import MarketdataService

from clients import users_pb2_grpc as user_grpc, operations_pb2_grpc as operations_grpc, \
    instruments_pb2_grpc as instruments_grpc, marketdata_pb2_grpc as marketdata_grpc


class Interface:
    def __init__(self, channel: grpc.Channel):
        self.channel = channel

    @cached_property
    def user(self):
        return UserService(user_grpc.UsersServiceStub(self.channel))

    @cached_property
    def operations(self):
        return OperationService(operations_grpc.OperationsServiceStub(self.channel))

    @cached_property
    def instruments(self):
        return InstrumentsService(instruments_grpc.InstrumentsServiceStub(self.channel))

    @cached_property
    def marketdata(self):
        return MarketdataService(marketdata_grpc.MarketDataServiceStub(self.channel))
