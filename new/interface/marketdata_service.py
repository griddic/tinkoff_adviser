from typing import Collection

from clients import marketdata_pb2 as messages
from clients import marketdata_pb2_grpc as marketdata_grpc


class MarketdataService:
    def __init__(self, stub: marketdata_grpc.MarketDataServiceStub):
        self.stub = stub

    def get_last_prices(self, figi) -> Collection[messages.LastPrice]:
        req = messages.GetLastPricesRequest(
            figi=[figi],
        )
        resp: messages.GetLastPricesResponse = self.stub.GetLastPrices(
            req
        )
        return resp.last_prices

    def get_closes_prices(self, figi) -> Collection[messages.InstrumentClosePriceResponse]:
        req = messages.GetClosePricesRequest(
            instruments=[messages.InstrumentClosePriceRequest(
                instrument_id=figi
            )],
        )
        resp: messages.GetClosePricesResponse = self.stub.GetClosePrices(
            req
        )
        return resp.close_prices
