from __future__ import annotations
import logging
from functools import cached_property, lru_cache
from typing import List, Union

import grpc
import yaml
import clients.users_pb2 as user_messages
import clients.operations_pb2 as operations_messages
import clients.instruments_pb2 as instruments_messages
import clients.marketdata_pb2 as marketdata_messages
import clients.common_pb2 as common_messages
from interface import Interface


def flotify(
        quotation: Union[common_messages.Quotation, common_messages.MoneyValue, common_messages.MoneyValue]) -> float:
    return quotation.units + quotation.nano / (10 ** 9)


def position_current_price(position: operations_messages.PortfolioPosition) -> float:
    return flotify(position.current_price) * flotify(position.quantity)


def create_channel() -> grpc.Channel:
    with open('../secrets/decrypted/secrets.yaml') as f:
        secrets = yaml.safe_load(f)
    token = secrets['tinkoff']['tokens']['main']
    scc = grpc.ssl_channel_credentials()
    tok = grpc.access_token_call_credentials(token)
    ccc = grpc.composite_channel_credentials(scc, tok)
    return grpc.secure_channel('invest-public-api.tinkoff.ru:443', credentials=ccc)


class Adviser:
    usd_rub_figi = 'BBG0013HGFT4'

    def __init__(self, interface: Interface):
        self.interface = interface
        self._wanted_usd_ratio = None

    @classmethod
    def default(cls) -> Adviser:
        return cls(Interface(create_channel()))

    @cached_property
    def main_account_id(self):
        accounts = self.interface.user.get_accounts()
        for ac in accounts:
            if ac.name == "Брокерский счёт":
                return ac.id
        raise RuntimeError("Основной счёт не найден.")

    def get_portfolio(self) -> operations_messages.PortfolioResponse:
        return self.interface.operations.get_portfolio(self.main_account_id)

    def print_portfolio(self):
        portf = self.get_portfolio()
        for position in portf.positions:
            inst = self.interface.instruments.get_by_figi(position.figi)

            def price(pos: operations_messages.PortfolioPosition):
                return pos.current_price.units + pos.current_price.nano / (10 ** 9)

            def quantity(pos: operations_messages.PortfolioPosition):
                return pos.quantity.units + pos.quantity.nano / (10 ** 9)

            print(inst.name, price(position) * quantity(position), inst.currency)

    def get_usd_price(self):
        # usd = self.get_instrument_by_figi(self.usd_rub_figi)
        prices = self.interface.marketdata.get_last_prices(self.usd_rub_figi)
        prices = [p for p in prices if p.HasField('price')]
        if prices:
            prices = sorted(prices, key=lambda p: (p.time.seconds, p.time.nanos), reverse=True)
            price = prices[0]
            return flotify(price.price)
        else:
            logging.warning('No last prices found for: USD. Closing price will be used.')
        closes = self.interface.marketdata.get_closes_prices(self.usd_rub_figi)
        closes = [c for c in closes if c.HasField('price')]
        closes = sorted(closes, key=lambda p: (p.time.seconds, p.time.nanos), reverse=True)
        return flotify(closes[0].price)

    def get_free_money_with_blocked(self):
        req = operations_messages.WithdrawLimitsRequest(
            account_id=self.main_account_id
        )
        resp: operations_messages.WithdrawLimitsResponse = self.interface.operations.stub.GetWithdrawLimits(req)
        return resp.money

    def calculate_currencies_distribution(self):
        usd_price = self.get_usd_price()
        usd_costs_in_usd = 0
        rub_costs = 0
        portfolio = self.get_portfolio()
        for position in portfolio.positions:
            match position.instrument_type:
                case 'bond' | 'share':
                    match position.current_price.currency:
                        case 'rub':
                            rub_costs += position_current_price(position)
                        case 'usd':
                            usd_costs_in_usd += position_current_price(position)
                        case _:
                            raise ValueError(f'Unexpected currency: {position.current_price.currency}')
                case 'currency':
                    pass  # will be handle as currencies
                case _:
                    raise ValueError(f'Unexpected instrument type: {position.instrument_type}')
        for curr_position in self.get_free_money_with_blocked():
            match curr_position.currency:
                case 'rub':
                    rub_costs += flotify(curr_position)
                case 'usd':
                    usd_costs_in_usd += flotify(curr_position)
                case _:
                    raise NotImplementedError(f'Can not handle currancy {curr_position.currency}')
        usd_costs_in_rub = usd_price * usd_costs_in_usd
        total = rub_costs + usd_costs_in_rub
        logging.info(f'Equities in RUB(rub): {rub_costs}')
        logging.info(f'Equities in USD(usd): {usd_costs_in_usd}')
        logging.info(f'Equities in USD(rub): {usd_costs_in_rub}')
        logging.info(f'Total(rub): {total}')
        return rub_costs / total, usd_costs_in_rub / total

    def all_instruments(self) -> List[instruments_messages.InstrumentShort]:
        req = instruments_messages.FindInstrumentRequest(query='BOND')
        resp: instruments_messages.FindInstrumentResponse = self.interface.instruments.stub.FindInstrument(req)
        return resp.instruments


def main():
    api = Adviser.default()
    print('info: ', api.interface.user.get_my_info())

    print('USD', api.get_usd_price())
    print('distribution', api.calculate_currencies_distribution())
    for i in api.all_instruments():
        print(i.name, i.api_trade_available_flag)


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    main()
