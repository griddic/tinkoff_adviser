from typing import Optional

from invest_api import Adviser


def balance_currencies(adviser: Optional[Adviser] = None):
    adviser = adviser or Adviser.default()
    print(adviser.calculate_currencies_distribution())


if __name__ == '__main__':
    balance_currencies()