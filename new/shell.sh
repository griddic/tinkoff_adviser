docker build -t tinkoff_adviser_new_shell --cache-from tinkoff_adviser_new_shell .
find . | grep -E "(__pycache__|\.pyc|\.pyo$)" | xargs rm -rf
dir="$(pwd)"
parentdir="$(dirname "$dir")"
echo ${parentdir}
docker run -it -v ${parentdir}:${parentdir} -w $(pwd) tinkoff_adviser_new_shell bash
find . | grep -E "(__pycache__|\.pyc|\.pyo$)" | xargs rm -rf