from google.protobuf import timestamp_pb2 as _timestamp_pb2
import common_pb2 as _common_pb2
from google.protobuf.internal import containers as _containers
from google.protobuf.internal import enum_type_wrapper as _enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Iterable as _Iterable, Mapping as _Mapping, Optional as _Optional, Union as _Union

DESCRIPTOR: _descriptor.FileDescriptor
STOP_ORDER_DIRECTION_BUY: StopOrderDirection
STOP_ORDER_DIRECTION_SELL: StopOrderDirection
STOP_ORDER_DIRECTION_UNSPECIFIED: StopOrderDirection
STOP_ORDER_EXPIRATION_TYPE_GOOD_TILL_CANCEL: StopOrderExpirationType
STOP_ORDER_EXPIRATION_TYPE_GOOD_TILL_DATE: StopOrderExpirationType
STOP_ORDER_EXPIRATION_TYPE_UNSPECIFIED: StopOrderExpirationType
STOP_ORDER_TYPE_STOP_LIMIT: StopOrderType
STOP_ORDER_TYPE_STOP_LOSS: StopOrderType
STOP_ORDER_TYPE_TAKE_PROFIT: StopOrderType
STOP_ORDER_TYPE_UNSPECIFIED: StopOrderType

class CancelStopOrderRequest(_message.Message):
    __slots__ = ["account_id", "stop_order_id"]
    ACCOUNT_ID_FIELD_NUMBER: _ClassVar[int]
    STOP_ORDER_ID_FIELD_NUMBER: _ClassVar[int]
    account_id: str
    stop_order_id: str
    def __init__(self, account_id: _Optional[str] = ..., stop_order_id: _Optional[str] = ...) -> None: ...

class CancelStopOrderResponse(_message.Message):
    __slots__ = ["time"]
    TIME_FIELD_NUMBER: _ClassVar[int]
    time: _timestamp_pb2.Timestamp
    def __init__(self, time: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ...) -> None: ...

class GetStopOrdersRequest(_message.Message):
    __slots__ = ["account_id"]
    ACCOUNT_ID_FIELD_NUMBER: _ClassVar[int]
    account_id: str
    def __init__(self, account_id: _Optional[str] = ...) -> None: ...

class GetStopOrdersResponse(_message.Message):
    __slots__ = ["stop_orders"]
    STOP_ORDERS_FIELD_NUMBER: _ClassVar[int]
    stop_orders: _containers.RepeatedCompositeFieldContainer[StopOrder]
    def __init__(self, stop_orders: _Optional[_Iterable[_Union[StopOrder, _Mapping]]] = ...) -> None: ...

class PostStopOrderRequest(_message.Message):
    __slots__ = ["account_id", "direction", "expiration_type", "expire_date", "figi", "price", "quantity", "stop_order_type", "stop_price"]
    ACCOUNT_ID_FIELD_NUMBER: _ClassVar[int]
    DIRECTION_FIELD_NUMBER: _ClassVar[int]
    EXPIRATION_TYPE_FIELD_NUMBER: _ClassVar[int]
    EXPIRE_DATE_FIELD_NUMBER: _ClassVar[int]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    PRICE_FIELD_NUMBER: _ClassVar[int]
    QUANTITY_FIELD_NUMBER: _ClassVar[int]
    STOP_ORDER_TYPE_FIELD_NUMBER: _ClassVar[int]
    STOP_PRICE_FIELD_NUMBER: _ClassVar[int]
    account_id: str
    direction: StopOrderDirection
    expiration_type: StopOrderExpirationType
    expire_date: _timestamp_pb2.Timestamp
    figi: str
    price: _common_pb2.Quotation
    quantity: int
    stop_order_type: StopOrderType
    stop_price: _common_pb2.Quotation
    def __init__(self, figi: _Optional[str] = ..., quantity: _Optional[int] = ..., price: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., stop_price: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., direction: _Optional[_Union[StopOrderDirection, str]] = ..., account_id: _Optional[str] = ..., expiration_type: _Optional[_Union[StopOrderExpirationType, str]] = ..., stop_order_type: _Optional[_Union[StopOrderType, str]] = ..., expire_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ...) -> None: ...

class PostStopOrderResponse(_message.Message):
    __slots__ = ["stop_order_id"]
    STOP_ORDER_ID_FIELD_NUMBER: _ClassVar[int]
    stop_order_id: str
    def __init__(self, stop_order_id: _Optional[str] = ...) -> None: ...

class StopOrder(_message.Message):
    __slots__ = ["activation_date_time", "create_date", "currency", "direction", "expiration_time", "figi", "lots_requested", "order_type", "price", "stop_order_id", "stop_price"]
    ACTIVATION_DATE_TIME_FIELD_NUMBER: _ClassVar[int]
    CREATE_DATE_FIELD_NUMBER: _ClassVar[int]
    CURRENCY_FIELD_NUMBER: _ClassVar[int]
    DIRECTION_FIELD_NUMBER: _ClassVar[int]
    EXPIRATION_TIME_FIELD_NUMBER: _ClassVar[int]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    LOTS_REQUESTED_FIELD_NUMBER: _ClassVar[int]
    ORDER_TYPE_FIELD_NUMBER: _ClassVar[int]
    PRICE_FIELD_NUMBER: _ClassVar[int]
    STOP_ORDER_ID_FIELD_NUMBER: _ClassVar[int]
    STOP_PRICE_FIELD_NUMBER: _ClassVar[int]
    activation_date_time: _timestamp_pb2.Timestamp
    create_date: _timestamp_pb2.Timestamp
    currency: str
    direction: StopOrderDirection
    expiration_time: _timestamp_pb2.Timestamp
    figi: str
    lots_requested: int
    order_type: StopOrderType
    price: _common_pb2.MoneyValue
    stop_order_id: str
    stop_price: _common_pb2.MoneyValue
    def __init__(self, stop_order_id: _Optional[str] = ..., lots_requested: _Optional[int] = ..., figi: _Optional[str] = ..., direction: _Optional[_Union[StopOrderDirection, str]] = ..., currency: _Optional[str] = ..., order_type: _Optional[_Union[StopOrderType, str]] = ..., create_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., activation_date_time: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., expiration_time: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., price: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., stop_price: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ...) -> None: ...

class StopOrderDirection(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []

class StopOrderExpirationType(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []

class StopOrderType(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
