from google.protobuf import timestamp_pb2 as _timestamp_pb2
import common_pb2 as _common_pb2
from google.protobuf.internal import containers as _containers
from google.protobuf.internal import enum_type_wrapper as _enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Iterable as _Iterable, Mapping as _Mapping, Optional as _Optional, Union as _Union

DESCRIPTOR: _descriptor.FileDescriptor
INSTRUMENT_TYPE_BOND: InstrumentType
INSTRUMENT_TYPE_CURRENCY: InstrumentType
INSTRUMENT_TYPE_ETF: InstrumentType
INSTRUMENT_TYPE_FUTURES: InstrumentType
INSTRUMENT_TYPE_OPTION: InstrumentType
INSTRUMENT_TYPE_SHARE: InstrumentType
INSTRUMENT_TYPE_SP: InstrumentType
INSTRUMENT_TYPE_UNSPECIFIED: InstrumentType
OPERATION_STATE_CANCELED: OperationState
OPERATION_STATE_EXECUTED: OperationState
OPERATION_STATE_PROGRESS: OperationState
OPERATION_STATE_UNSPECIFIED: OperationState
OPERATION_TYPE_ACCRUING_VARMARGIN: OperationType
OPERATION_TYPE_BENEFIT_TAX: OperationType
OPERATION_TYPE_BENEFIT_TAX_PROGRESSIVE: OperationType
OPERATION_TYPE_BOND_REPAYMENT: OperationType
OPERATION_TYPE_BOND_REPAYMENT_FULL: OperationType
OPERATION_TYPE_BOND_TAX: OperationType
OPERATION_TYPE_BOND_TAX_PROGRESSIVE: OperationType
OPERATION_TYPE_BROKER_FEE: OperationType
OPERATION_TYPE_BUY: OperationType
OPERATION_TYPE_BUY_CARD: OperationType
OPERATION_TYPE_BUY_MARGIN: OperationType
OPERATION_TYPE_COUPON: OperationType
OPERATION_TYPE_DELIVERY_BUY: OperationType
OPERATION_TYPE_DELIVERY_SELL: OperationType
OPERATION_TYPE_DIVIDEND: OperationType
OPERATION_TYPE_DIVIDEND_TAX: OperationType
OPERATION_TYPE_DIVIDEND_TAX_PROGRESSIVE: OperationType
OPERATION_TYPE_DIVIDEND_TRANSFER: OperationType
OPERATION_TYPE_DIV_EXT: OperationType
OPERATION_TYPE_INPUT: OperationType
OPERATION_TYPE_INPUT_SECURITIES: OperationType
OPERATION_TYPE_MARGIN_FEE: OperationType
OPERATION_TYPE_OUTPUT: OperationType
OPERATION_TYPE_OUTPUT_SECURITIES: OperationType
OPERATION_TYPE_OVERNIGHT: OperationType
OPERATION_TYPE_SELL: OperationType
OPERATION_TYPE_SELL_CARD: OperationType
OPERATION_TYPE_SELL_MARGIN: OperationType
OPERATION_TYPE_SERVICE_FEE: OperationType
OPERATION_TYPE_SUCCESS_FEE: OperationType
OPERATION_TYPE_TAX: OperationType
OPERATION_TYPE_TAX_CORRECTION: OperationType
OPERATION_TYPE_TAX_CORRECTION_COUPON: OperationType
OPERATION_TYPE_TAX_CORRECTION_PROGRESSIVE: OperationType
OPERATION_TYPE_TAX_PROGRESSIVE: OperationType
OPERATION_TYPE_TAX_REPO: OperationType
OPERATION_TYPE_TAX_REPO_HOLD: OperationType
OPERATION_TYPE_TAX_REPO_HOLD_PROGRESSIVE: OperationType
OPERATION_TYPE_TAX_REPO_PROGRESSIVE: OperationType
OPERATION_TYPE_TAX_REPO_REFUND: OperationType
OPERATION_TYPE_TAX_REPO_REFUND_PROGRESSIVE: OperationType
OPERATION_TYPE_TRACK_MFEE: OperationType
OPERATION_TYPE_TRACK_PFEE: OperationType
OPERATION_TYPE_UNSPECIFIED: OperationType
OPERATION_TYPE_WRITING_OFF_VARMARGIN: OperationType
PORTFOLIO_SUBSCRIPTION_STATUS_ACCOUNT_NOT_FOUND: PortfolioSubscriptionStatus
PORTFOLIO_SUBSCRIPTION_STATUS_INTERNAL_ERROR: PortfolioSubscriptionStatus
PORTFOLIO_SUBSCRIPTION_STATUS_SUCCESS: PortfolioSubscriptionStatus
PORTFOLIO_SUBSCRIPTION_STATUS_UNSPECIFIED: PortfolioSubscriptionStatus
POSITIONS_SUBSCRIPTION_STATUS_ACCOUNT_NOT_FOUND: PositionsAccountSubscriptionStatus
POSITIONS_SUBSCRIPTION_STATUS_INTERNAL_ERROR: PositionsAccountSubscriptionStatus
POSITIONS_SUBSCRIPTION_STATUS_SUCCESS: PositionsAccountSubscriptionStatus
POSITIONS_SUBSCRIPTION_STATUS_UNSPECIFIED: PositionsAccountSubscriptionStatus

class AccountSubscriptionStatus(_message.Message):
    __slots__ = ["account_id", "subscription_status"]
    ACCOUNT_ID_FIELD_NUMBER: _ClassVar[int]
    SUBSCRIPTION_STATUS_FIELD_NUMBER: _ClassVar[int]
    account_id: str
    subscription_status: PortfolioSubscriptionStatus
    def __init__(self, account_id: _Optional[str] = ..., subscription_status: _Optional[_Union[PortfolioSubscriptionStatus, str]] = ...) -> None: ...

class BrokerReport(_message.Message):
    __slots__ = ["aci_value", "broker_commission", "broker_status", "class_code", "clear_value_date", "delivery_type", "direction", "exchange", "exchange_clearing_commission", "exchange_commission", "execute_sign", "figi", "name", "order_amount", "order_id", "party", "price", "quantity", "repo_rate", "sec_value_date", "separate_agreement_date", "separate_agreement_number", "separate_agreement_type", "ticker", "total_order_amount", "trade_datetime", "trade_id"]
    ACI_VALUE_FIELD_NUMBER: _ClassVar[int]
    BROKER_COMMISSION_FIELD_NUMBER: _ClassVar[int]
    BROKER_STATUS_FIELD_NUMBER: _ClassVar[int]
    CLASS_CODE_FIELD_NUMBER: _ClassVar[int]
    CLEAR_VALUE_DATE_FIELD_NUMBER: _ClassVar[int]
    DELIVERY_TYPE_FIELD_NUMBER: _ClassVar[int]
    DIRECTION_FIELD_NUMBER: _ClassVar[int]
    EXCHANGE_CLEARING_COMMISSION_FIELD_NUMBER: _ClassVar[int]
    EXCHANGE_COMMISSION_FIELD_NUMBER: _ClassVar[int]
    EXCHANGE_FIELD_NUMBER: _ClassVar[int]
    EXECUTE_SIGN_FIELD_NUMBER: _ClassVar[int]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    ORDER_AMOUNT_FIELD_NUMBER: _ClassVar[int]
    ORDER_ID_FIELD_NUMBER: _ClassVar[int]
    PARTY_FIELD_NUMBER: _ClassVar[int]
    PRICE_FIELD_NUMBER: _ClassVar[int]
    QUANTITY_FIELD_NUMBER: _ClassVar[int]
    REPO_RATE_FIELD_NUMBER: _ClassVar[int]
    SEC_VALUE_DATE_FIELD_NUMBER: _ClassVar[int]
    SEPARATE_AGREEMENT_DATE_FIELD_NUMBER: _ClassVar[int]
    SEPARATE_AGREEMENT_NUMBER_FIELD_NUMBER: _ClassVar[int]
    SEPARATE_AGREEMENT_TYPE_FIELD_NUMBER: _ClassVar[int]
    TICKER_FIELD_NUMBER: _ClassVar[int]
    TOTAL_ORDER_AMOUNT_FIELD_NUMBER: _ClassVar[int]
    TRADE_DATETIME_FIELD_NUMBER: _ClassVar[int]
    TRADE_ID_FIELD_NUMBER: _ClassVar[int]
    aci_value: _common_pb2.Quotation
    broker_commission: _common_pb2.MoneyValue
    broker_status: str
    class_code: str
    clear_value_date: _timestamp_pb2.Timestamp
    delivery_type: str
    direction: str
    exchange: str
    exchange_clearing_commission: _common_pb2.MoneyValue
    exchange_commission: _common_pb2.MoneyValue
    execute_sign: str
    figi: str
    name: str
    order_amount: _common_pb2.MoneyValue
    order_id: str
    party: str
    price: _common_pb2.MoneyValue
    quantity: int
    repo_rate: _common_pb2.Quotation
    sec_value_date: _timestamp_pb2.Timestamp
    separate_agreement_date: str
    separate_agreement_number: str
    separate_agreement_type: str
    ticker: str
    total_order_amount: _common_pb2.MoneyValue
    trade_datetime: _timestamp_pb2.Timestamp
    trade_id: str
    def __init__(self, trade_id: _Optional[str] = ..., order_id: _Optional[str] = ..., figi: _Optional[str] = ..., execute_sign: _Optional[str] = ..., trade_datetime: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., exchange: _Optional[str] = ..., class_code: _Optional[str] = ..., direction: _Optional[str] = ..., name: _Optional[str] = ..., ticker: _Optional[str] = ..., price: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., quantity: _Optional[int] = ..., order_amount: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., aci_value: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., total_order_amount: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., broker_commission: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., exchange_commission: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., exchange_clearing_commission: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., repo_rate: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., party: _Optional[str] = ..., clear_value_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., sec_value_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., broker_status: _Optional[str] = ..., separate_agreement_type: _Optional[str] = ..., separate_agreement_number: _Optional[str] = ..., separate_agreement_date: _Optional[str] = ..., delivery_type: _Optional[str] = ...) -> None: ...

class BrokerReportRequest(_message.Message):
    __slots__ = ["generate_broker_report_request", "get_broker_report_request"]
    GENERATE_BROKER_REPORT_REQUEST_FIELD_NUMBER: _ClassVar[int]
    GET_BROKER_REPORT_REQUEST_FIELD_NUMBER: _ClassVar[int]
    generate_broker_report_request: GenerateBrokerReportRequest
    get_broker_report_request: GetBrokerReportRequest
    def __init__(self, generate_broker_report_request: _Optional[_Union[GenerateBrokerReportRequest, _Mapping]] = ..., get_broker_report_request: _Optional[_Union[GetBrokerReportRequest, _Mapping]] = ...) -> None: ...

class BrokerReportResponse(_message.Message):
    __slots__ = ["generate_broker_report_response", "get_broker_report_response"]
    GENERATE_BROKER_REPORT_RESPONSE_FIELD_NUMBER: _ClassVar[int]
    GET_BROKER_REPORT_RESPONSE_FIELD_NUMBER: _ClassVar[int]
    generate_broker_report_response: GenerateBrokerReportResponse
    get_broker_report_response: GetBrokerReportResponse
    def __init__(self, generate_broker_report_response: _Optional[_Union[GenerateBrokerReportResponse, _Mapping]] = ..., get_broker_report_response: _Optional[_Union[GetBrokerReportResponse, _Mapping]] = ...) -> None: ...

class DividendsForeignIssuerReport(_message.Message):
    __slots__ = ["currency", "dividend", "dividend_amount", "dividend_gross", "external_commission", "isin", "issuer_country", "payment_date", "quantity", "record_date", "security_name", "tax"]
    CURRENCY_FIELD_NUMBER: _ClassVar[int]
    DIVIDEND_AMOUNT_FIELD_NUMBER: _ClassVar[int]
    DIVIDEND_FIELD_NUMBER: _ClassVar[int]
    DIVIDEND_GROSS_FIELD_NUMBER: _ClassVar[int]
    EXTERNAL_COMMISSION_FIELD_NUMBER: _ClassVar[int]
    ISIN_FIELD_NUMBER: _ClassVar[int]
    ISSUER_COUNTRY_FIELD_NUMBER: _ClassVar[int]
    PAYMENT_DATE_FIELD_NUMBER: _ClassVar[int]
    QUANTITY_FIELD_NUMBER: _ClassVar[int]
    RECORD_DATE_FIELD_NUMBER: _ClassVar[int]
    SECURITY_NAME_FIELD_NUMBER: _ClassVar[int]
    TAX_FIELD_NUMBER: _ClassVar[int]
    currency: str
    dividend: _common_pb2.Quotation
    dividend_amount: _common_pb2.Quotation
    dividend_gross: _common_pb2.Quotation
    external_commission: _common_pb2.Quotation
    isin: str
    issuer_country: str
    payment_date: _timestamp_pb2.Timestamp
    quantity: int
    record_date: _timestamp_pb2.Timestamp
    security_name: str
    tax: _common_pb2.Quotation
    def __init__(self, record_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., payment_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., security_name: _Optional[str] = ..., isin: _Optional[str] = ..., issuer_country: _Optional[str] = ..., quantity: _Optional[int] = ..., dividend: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., external_commission: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., dividend_gross: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., tax: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., dividend_amount: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., currency: _Optional[str] = ...) -> None: ...

class GenerateBrokerReportRequest(_message.Message):
    __slots__ = ["account_id", "to"]
    ACCOUNT_ID_FIELD_NUMBER: _ClassVar[int]
    FROM_FIELD_NUMBER: _ClassVar[int]
    TO_FIELD_NUMBER: _ClassVar[int]
    account_id: str
    to: _timestamp_pb2.Timestamp
    def __init__(self, account_id: _Optional[str] = ..., to: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., **kwargs) -> None: ...

class GenerateBrokerReportResponse(_message.Message):
    __slots__ = ["task_id"]
    TASK_ID_FIELD_NUMBER: _ClassVar[int]
    task_id: str
    def __init__(self, task_id: _Optional[str] = ...) -> None: ...

class GenerateDividendsForeignIssuerReportRequest(_message.Message):
    __slots__ = ["account_id", "to"]
    ACCOUNT_ID_FIELD_NUMBER: _ClassVar[int]
    FROM_FIELD_NUMBER: _ClassVar[int]
    TO_FIELD_NUMBER: _ClassVar[int]
    account_id: str
    to: _timestamp_pb2.Timestamp
    def __init__(self, account_id: _Optional[str] = ..., to: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., **kwargs) -> None: ...

class GenerateDividendsForeignIssuerReportResponse(_message.Message):
    __slots__ = ["task_id"]
    TASK_ID_FIELD_NUMBER: _ClassVar[int]
    task_id: str
    def __init__(self, task_id: _Optional[str] = ...) -> None: ...

class GetBrokerReportRequest(_message.Message):
    __slots__ = ["page", "task_id"]
    PAGE_FIELD_NUMBER: _ClassVar[int]
    TASK_ID_FIELD_NUMBER: _ClassVar[int]
    page: int
    task_id: str
    def __init__(self, task_id: _Optional[str] = ..., page: _Optional[int] = ...) -> None: ...

class GetBrokerReportResponse(_message.Message):
    __slots__ = ["broker_report", "itemsCount", "page", "pagesCount"]
    BROKER_REPORT_FIELD_NUMBER: _ClassVar[int]
    ITEMSCOUNT_FIELD_NUMBER: _ClassVar[int]
    PAGESCOUNT_FIELD_NUMBER: _ClassVar[int]
    PAGE_FIELD_NUMBER: _ClassVar[int]
    broker_report: _containers.RepeatedCompositeFieldContainer[BrokerReport]
    itemsCount: int
    page: int
    pagesCount: int
    def __init__(self, broker_report: _Optional[_Iterable[_Union[BrokerReport, _Mapping]]] = ..., itemsCount: _Optional[int] = ..., pagesCount: _Optional[int] = ..., page: _Optional[int] = ...) -> None: ...

class GetDividendsForeignIssuerReportRequest(_message.Message):
    __slots__ = ["page", "task_id"]
    PAGE_FIELD_NUMBER: _ClassVar[int]
    TASK_ID_FIELD_NUMBER: _ClassVar[int]
    page: int
    task_id: str
    def __init__(self, task_id: _Optional[str] = ..., page: _Optional[int] = ...) -> None: ...

class GetDividendsForeignIssuerReportResponse(_message.Message):
    __slots__ = ["dividends_foreign_issuer_report", "itemsCount", "page", "pagesCount"]
    DIVIDENDS_FOREIGN_ISSUER_REPORT_FIELD_NUMBER: _ClassVar[int]
    ITEMSCOUNT_FIELD_NUMBER: _ClassVar[int]
    PAGESCOUNT_FIELD_NUMBER: _ClassVar[int]
    PAGE_FIELD_NUMBER: _ClassVar[int]
    dividends_foreign_issuer_report: _containers.RepeatedCompositeFieldContainer[DividendsForeignIssuerReport]
    itemsCount: int
    page: int
    pagesCount: int
    def __init__(self, dividends_foreign_issuer_report: _Optional[_Iterable[_Union[DividendsForeignIssuerReport, _Mapping]]] = ..., itemsCount: _Optional[int] = ..., pagesCount: _Optional[int] = ..., page: _Optional[int] = ...) -> None: ...

class GetDividendsForeignIssuerRequest(_message.Message):
    __slots__ = ["generate_div_foreign_issuer_report", "get_div_foreign_issuer_report"]
    GENERATE_DIV_FOREIGN_ISSUER_REPORT_FIELD_NUMBER: _ClassVar[int]
    GET_DIV_FOREIGN_ISSUER_REPORT_FIELD_NUMBER: _ClassVar[int]
    generate_div_foreign_issuer_report: GenerateDividendsForeignIssuerReportRequest
    get_div_foreign_issuer_report: GetDividendsForeignIssuerReportRequest
    def __init__(self, generate_div_foreign_issuer_report: _Optional[_Union[GenerateDividendsForeignIssuerReportRequest, _Mapping]] = ..., get_div_foreign_issuer_report: _Optional[_Union[GetDividendsForeignIssuerReportRequest, _Mapping]] = ...) -> None: ...

class GetDividendsForeignIssuerResponse(_message.Message):
    __slots__ = ["div_foreign_issuer_report", "generate_div_foreign_issuer_report_response"]
    DIV_FOREIGN_ISSUER_REPORT_FIELD_NUMBER: _ClassVar[int]
    GENERATE_DIV_FOREIGN_ISSUER_REPORT_RESPONSE_FIELD_NUMBER: _ClassVar[int]
    div_foreign_issuer_report: GetDividendsForeignIssuerReportResponse
    generate_div_foreign_issuer_report_response: GenerateDividendsForeignIssuerReportResponse
    def __init__(self, generate_div_foreign_issuer_report_response: _Optional[_Union[GenerateDividendsForeignIssuerReportResponse, _Mapping]] = ..., div_foreign_issuer_report: _Optional[_Union[GetDividendsForeignIssuerReportResponse, _Mapping]] = ...) -> None: ...

class GetOperationsByCursorRequest(_message.Message):
    __slots__ = ["account_id", "cursor", "instrument_id", "limit", "operation_types", "state", "to", "without_commissions", "without_overnights", "without_trades"]
    ACCOUNT_ID_FIELD_NUMBER: _ClassVar[int]
    CURSOR_FIELD_NUMBER: _ClassVar[int]
    FROM_FIELD_NUMBER: _ClassVar[int]
    INSTRUMENT_ID_FIELD_NUMBER: _ClassVar[int]
    LIMIT_FIELD_NUMBER: _ClassVar[int]
    OPERATION_TYPES_FIELD_NUMBER: _ClassVar[int]
    STATE_FIELD_NUMBER: _ClassVar[int]
    TO_FIELD_NUMBER: _ClassVar[int]
    WITHOUT_COMMISSIONS_FIELD_NUMBER: _ClassVar[int]
    WITHOUT_OVERNIGHTS_FIELD_NUMBER: _ClassVar[int]
    WITHOUT_TRADES_FIELD_NUMBER: _ClassVar[int]
    account_id: str
    cursor: str
    instrument_id: str
    limit: int
    operation_types: _containers.RepeatedScalarFieldContainer[OperationType]
    state: OperationState
    to: _timestamp_pb2.Timestamp
    without_commissions: bool
    without_overnights: bool
    without_trades: bool
    def __init__(self, account_id: _Optional[str] = ..., instrument_id: _Optional[str] = ..., to: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., cursor: _Optional[str] = ..., limit: _Optional[int] = ..., operation_types: _Optional[_Iterable[_Union[OperationType, str]]] = ..., state: _Optional[_Union[OperationState, str]] = ..., without_commissions: bool = ..., without_trades: bool = ..., without_overnights: bool = ..., **kwargs) -> None: ...

class GetOperationsByCursorResponse(_message.Message):
    __slots__ = ["has_next", "items", "next_cursor"]
    HAS_NEXT_FIELD_NUMBER: _ClassVar[int]
    ITEMS_FIELD_NUMBER: _ClassVar[int]
    NEXT_CURSOR_FIELD_NUMBER: _ClassVar[int]
    has_next: bool
    items: _containers.RepeatedCompositeFieldContainer[OperationItem]
    next_cursor: str
    def __init__(self, has_next: bool = ..., next_cursor: _Optional[str] = ..., items: _Optional[_Iterable[_Union[OperationItem, _Mapping]]] = ...) -> None: ...

class Operation(_message.Message):
    __slots__ = ["currency", "date", "figi", "id", "instrument_type", "operation_type", "parent_operation_id", "payment", "price", "quantity", "quantity_rest", "state", "trades", "type"]
    CURRENCY_FIELD_NUMBER: _ClassVar[int]
    DATE_FIELD_NUMBER: _ClassVar[int]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    ID_FIELD_NUMBER: _ClassVar[int]
    INSTRUMENT_TYPE_FIELD_NUMBER: _ClassVar[int]
    OPERATION_TYPE_FIELD_NUMBER: _ClassVar[int]
    PARENT_OPERATION_ID_FIELD_NUMBER: _ClassVar[int]
    PAYMENT_FIELD_NUMBER: _ClassVar[int]
    PRICE_FIELD_NUMBER: _ClassVar[int]
    QUANTITY_FIELD_NUMBER: _ClassVar[int]
    QUANTITY_REST_FIELD_NUMBER: _ClassVar[int]
    STATE_FIELD_NUMBER: _ClassVar[int]
    TRADES_FIELD_NUMBER: _ClassVar[int]
    TYPE_FIELD_NUMBER: _ClassVar[int]
    currency: str
    date: _timestamp_pb2.Timestamp
    figi: str
    id: str
    instrument_type: str
    operation_type: OperationType
    parent_operation_id: str
    payment: _common_pb2.MoneyValue
    price: _common_pb2.MoneyValue
    quantity: int
    quantity_rest: int
    state: OperationState
    trades: _containers.RepeatedCompositeFieldContainer[OperationTrade]
    type: str
    def __init__(self, id: _Optional[str] = ..., parent_operation_id: _Optional[str] = ..., currency: _Optional[str] = ..., payment: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., price: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., state: _Optional[_Union[OperationState, str]] = ..., quantity: _Optional[int] = ..., quantity_rest: _Optional[int] = ..., figi: _Optional[str] = ..., instrument_type: _Optional[str] = ..., date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., type: _Optional[str] = ..., operation_type: _Optional[_Union[OperationType, str]] = ..., trades: _Optional[_Iterable[_Union[OperationTrade, _Mapping]]] = ...) -> None: ...

class OperationItem(_message.Message):
    __slots__ = ["accrued_int", "broker_account_id", "cancel_date_time", "cancel_reason", "commission", "cursor", "date", "description", "figi", "id", "instrument_kind", "instrument_type", "instrument_uid", "name", "parent_operation_id", "payment", "price", "quantity", "quantity_done", "quantity_rest", "state", "trades_info", "type", "yield_relative"]
    ACCRUED_INT_FIELD_NUMBER: _ClassVar[int]
    BROKER_ACCOUNT_ID_FIELD_NUMBER: _ClassVar[int]
    CANCEL_DATE_TIME_FIELD_NUMBER: _ClassVar[int]
    CANCEL_REASON_FIELD_NUMBER: _ClassVar[int]
    COMMISSION_FIELD_NUMBER: _ClassVar[int]
    CURSOR_FIELD_NUMBER: _ClassVar[int]
    DATE_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    ID_FIELD_NUMBER: _ClassVar[int]
    INSTRUMENT_KIND_FIELD_NUMBER: _ClassVar[int]
    INSTRUMENT_TYPE_FIELD_NUMBER: _ClassVar[int]
    INSTRUMENT_UID_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    PARENT_OPERATION_ID_FIELD_NUMBER: _ClassVar[int]
    PAYMENT_FIELD_NUMBER: _ClassVar[int]
    PRICE_FIELD_NUMBER: _ClassVar[int]
    QUANTITY_DONE_FIELD_NUMBER: _ClassVar[int]
    QUANTITY_FIELD_NUMBER: _ClassVar[int]
    QUANTITY_REST_FIELD_NUMBER: _ClassVar[int]
    STATE_FIELD_NUMBER: _ClassVar[int]
    TRADES_INFO_FIELD_NUMBER: _ClassVar[int]
    TYPE_FIELD_NUMBER: _ClassVar[int]
    YIELD_FIELD_NUMBER: _ClassVar[int]
    YIELD_RELATIVE_FIELD_NUMBER: _ClassVar[int]
    accrued_int: _common_pb2.MoneyValue
    broker_account_id: str
    cancel_date_time: _timestamp_pb2.Timestamp
    cancel_reason: str
    commission: _common_pb2.MoneyValue
    cursor: str
    date: _timestamp_pb2.Timestamp
    description: str
    figi: str
    id: str
    instrument_kind: InstrumentType
    instrument_type: str
    instrument_uid: str
    name: str
    parent_operation_id: str
    payment: _common_pb2.MoneyValue
    price: _common_pb2.MoneyValue
    quantity: int
    quantity_done: int
    quantity_rest: int
    state: OperationState
    trades_info: OperationItemTrades
    type: OperationType
    yield_relative: _common_pb2.Quotation
    def __init__(self, cursor: _Optional[str] = ..., broker_account_id: _Optional[str] = ..., id: _Optional[str] = ..., parent_operation_id: _Optional[str] = ..., name: _Optional[str] = ..., date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., type: _Optional[_Union[OperationType, str]] = ..., description: _Optional[str] = ..., state: _Optional[_Union[OperationState, str]] = ..., instrument_uid: _Optional[str] = ..., figi: _Optional[str] = ..., instrument_type: _Optional[str] = ..., instrument_kind: _Optional[_Union[InstrumentType, str]] = ..., payment: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., price: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., commission: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., yield_relative: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., accrued_int: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., quantity: _Optional[int] = ..., quantity_rest: _Optional[int] = ..., quantity_done: _Optional[int] = ..., cancel_date_time: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., cancel_reason: _Optional[str] = ..., trades_info: _Optional[_Union[OperationItemTrades, _Mapping]] = ..., **kwargs) -> None: ...

class OperationItemTrade(_message.Message):
    __slots__ = ["date", "num", "price", "quantity", "yield_relative"]
    DATE_FIELD_NUMBER: _ClassVar[int]
    NUM_FIELD_NUMBER: _ClassVar[int]
    PRICE_FIELD_NUMBER: _ClassVar[int]
    QUANTITY_FIELD_NUMBER: _ClassVar[int]
    YIELD_FIELD_NUMBER: _ClassVar[int]
    YIELD_RELATIVE_FIELD_NUMBER: _ClassVar[int]
    date: _timestamp_pb2.Timestamp
    num: str
    price: _common_pb2.MoneyValue
    quantity: int
    yield_relative: _common_pb2.Quotation
    def __init__(self, num: _Optional[str] = ..., date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., quantity: _Optional[int] = ..., price: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., yield_relative: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., **kwargs) -> None: ...

class OperationItemTrades(_message.Message):
    __slots__ = ["trades", "trades_size"]
    TRADES_FIELD_NUMBER: _ClassVar[int]
    TRADES_SIZE_FIELD_NUMBER: _ClassVar[int]
    trades: _containers.RepeatedCompositeFieldContainer[OperationItemTrade]
    trades_size: int
    def __init__(self, trades_size: _Optional[int] = ..., trades: _Optional[_Iterable[_Union[OperationItemTrade, _Mapping]]] = ...) -> None: ...

class OperationTrade(_message.Message):
    __slots__ = ["date_time", "price", "quantity", "trade_id"]
    DATE_TIME_FIELD_NUMBER: _ClassVar[int]
    PRICE_FIELD_NUMBER: _ClassVar[int]
    QUANTITY_FIELD_NUMBER: _ClassVar[int]
    TRADE_ID_FIELD_NUMBER: _ClassVar[int]
    date_time: _timestamp_pb2.Timestamp
    price: _common_pb2.MoneyValue
    quantity: int
    trade_id: str
    def __init__(self, trade_id: _Optional[str] = ..., date_time: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., quantity: _Optional[int] = ..., price: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ...) -> None: ...

class OperationsRequest(_message.Message):
    __slots__ = ["account_id", "figi", "state", "to"]
    ACCOUNT_ID_FIELD_NUMBER: _ClassVar[int]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    FROM_FIELD_NUMBER: _ClassVar[int]
    STATE_FIELD_NUMBER: _ClassVar[int]
    TO_FIELD_NUMBER: _ClassVar[int]
    account_id: str
    figi: str
    state: OperationState
    to: _timestamp_pb2.Timestamp
    def __init__(self, account_id: _Optional[str] = ..., to: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., state: _Optional[_Union[OperationState, str]] = ..., figi: _Optional[str] = ..., **kwargs) -> None: ...

class OperationsResponse(_message.Message):
    __slots__ = ["operations"]
    OPERATIONS_FIELD_NUMBER: _ClassVar[int]
    operations: _containers.RepeatedCompositeFieldContainer[Operation]
    def __init__(self, operations: _Optional[_Iterable[_Union[Operation, _Mapping]]] = ...) -> None: ...

class PortfolioPosition(_message.Message):
    __slots__ = ["average_position_price", "average_position_price_fifo", "average_position_price_pt", "blocked", "current_nkd", "current_price", "expected_yield", "figi", "instrument_type", "quantity", "quantity_lots"]
    AVERAGE_POSITION_PRICE_FIELD_NUMBER: _ClassVar[int]
    AVERAGE_POSITION_PRICE_FIFO_FIELD_NUMBER: _ClassVar[int]
    AVERAGE_POSITION_PRICE_PT_FIELD_NUMBER: _ClassVar[int]
    BLOCKED_FIELD_NUMBER: _ClassVar[int]
    CURRENT_NKD_FIELD_NUMBER: _ClassVar[int]
    CURRENT_PRICE_FIELD_NUMBER: _ClassVar[int]
    EXPECTED_YIELD_FIELD_NUMBER: _ClassVar[int]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    INSTRUMENT_TYPE_FIELD_NUMBER: _ClassVar[int]
    QUANTITY_FIELD_NUMBER: _ClassVar[int]
    QUANTITY_LOTS_FIELD_NUMBER: _ClassVar[int]
    average_position_price: _common_pb2.MoneyValue
    average_position_price_fifo: _common_pb2.MoneyValue
    average_position_price_pt: _common_pb2.Quotation
    blocked: bool
    current_nkd: _common_pb2.MoneyValue
    current_price: _common_pb2.MoneyValue
    expected_yield: _common_pb2.Quotation
    figi: str
    instrument_type: str
    quantity: _common_pb2.Quotation
    quantity_lots: _common_pb2.Quotation
    def __init__(self, figi: _Optional[str] = ..., instrument_type: _Optional[str] = ..., quantity: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., average_position_price: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., expected_yield: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., current_nkd: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., average_position_price_pt: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., current_price: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., average_position_price_fifo: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., quantity_lots: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., blocked: bool = ...) -> None: ...

class PortfolioRequest(_message.Message):
    __slots__ = ["account_id"]
    ACCOUNT_ID_FIELD_NUMBER: _ClassVar[int]
    account_id: str
    def __init__(self, account_id: _Optional[str] = ...) -> None: ...

class PortfolioResponse(_message.Message):
    __slots__ = ["expected_yield", "positions", "total_amount_bonds", "total_amount_currencies", "total_amount_etf", "total_amount_futures", "total_amount_shares"]
    EXPECTED_YIELD_FIELD_NUMBER: _ClassVar[int]
    POSITIONS_FIELD_NUMBER: _ClassVar[int]
    TOTAL_AMOUNT_BONDS_FIELD_NUMBER: _ClassVar[int]
    TOTAL_AMOUNT_CURRENCIES_FIELD_NUMBER: _ClassVar[int]
    TOTAL_AMOUNT_ETF_FIELD_NUMBER: _ClassVar[int]
    TOTAL_AMOUNT_FUTURES_FIELD_NUMBER: _ClassVar[int]
    TOTAL_AMOUNT_SHARES_FIELD_NUMBER: _ClassVar[int]
    expected_yield: _common_pb2.Quotation
    positions: _containers.RepeatedCompositeFieldContainer[PortfolioPosition]
    total_amount_bonds: _common_pb2.MoneyValue
    total_amount_currencies: _common_pb2.MoneyValue
    total_amount_etf: _common_pb2.MoneyValue
    total_amount_futures: _common_pb2.MoneyValue
    total_amount_shares: _common_pb2.MoneyValue
    def __init__(self, total_amount_shares: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., total_amount_bonds: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., total_amount_etf: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., total_amount_currencies: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., total_amount_futures: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., expected_yield: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., positions: _Optional[_Iterable[_Union[PortfolioPosition, _Mapping]]] = ...) -> None: ...

class PortfolioStreamRequest(_message.Message):
    __slots__ = ["accounts"]
    ACCOUNTS_FIELD_NUMBER: _ClassVar[int]
    accounts: _containers.RepeatedScalarFieldContainer[str]
    def __init__(self, accounts: _Optional[_Iterable[str]] = ...) -> None: ...

class PortfolioStreamResponse(_message.Message):
    __slots__ = ["ping", "portfolio", "subscriptions"]
    PING_FIELD_NUMBER: _ClassVar[int]
    PORTFOLIO_FIELD_NUMBER: _ClassVar[int]
    SUBSCRIPTIONS_FIELD_NUMBER: _ClassVar[int]
    ping: _common_pb2.Ping
    portfolio: PortfolioResponse
    subscriptions: PortfolioSubscriptionResult
    def __init__(self, subscriptions: _Optional[_Union[PortfolioSubscriptionResult, _Mapping]] = ..., portfolio: _Optional[_Union[PortfolioResponse, _Mapping]] = ..., ping: _Optional[_Union[_common_pb2.Ping, _Mapping]] = ...) -> None: ...

class PortfolioSubscriptionResult(_message.Message):
    __slots__ = ["accounts"]
    ACCOUNTS_FIELD_NUMBER: _ClassVar[int]
    accounts: _containers.RepeatedCompositeFieldContainer[AccountSubscriptionStatus]
    def __init__(self, accounts: _Optional[_Iterable[_Union[AccountSubscriptionStatus, _Mapping]]] = ...) -> None: ...

class PositionData(_message.Message):
    __slots__ = ["account_id", "date", "futures", "money", "options", "securities"]
    ACCOUNT_ID_FIELD_NUMBER: _ClassVar[int]
    DATE_FIELD_NUMBER: _ClassVar[int]
    FUTURES_FIELD_NUMBER: _ClassVar[int]
    MONEY_FIELD_NUMBER: _ClassVar[int]
    OPTIONS_FIELD_NUMBER: _ClassVar[int]
    SECURITIES_FIELD_NUMBER: _ClassVar[int]
    account_id: str
    date: _timestamp_pb2.Timestamp
    futures: _containers.RepeatedCompositeFieldContainer[PositionsFutures]
    money: _containers.RepeatedCompositeFieldContainer[PositionsMoney]
    options: _containers.RepeatedCompositeFieldContainer[PositionsOptions]
    securities: _containers.RepeatedCompositeFieldContainer[PositionsSecurities]
    def __init__(self, account_id: _Optional[str] = ..., money: _Optional[_Iterable[_Union[PositionsMoney, _Mapping]]] = ..., securities: _Optional[_Iterable[_Union[PositionsSecurities, _Mapping]]] = ..., futures: _Optional[_Iterable[_Union[PositionsFutures, _Mapping]]] = ..., options: _Optional[_Iterable[_Union[PositionsOptions, _Mapping]]] = ..., date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ...) -> None: ...

class PositionsFutures(_message.Message):
    __slots__ = ["balance", "blocked", "figi", "instrument_uid", "position_uid"]
    BALANCE_FIELD_NUMBER: _ClassVar[int]
    BLOCKED_FIELD_NUMBER: _ClassVar[int]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    INSTRUMENT_UID_FIELD_NUMBER: _ClassVar[int]
    POSITION_UID_FIELD_NUMBER: _ClassVar[int]
    balance: int
    blocked: int
    figi: str
    instrument_uid: str
    position_uid: str
    def __init__(self, figi: _Optional[str] = ..., blocked: _Optional[int] = ..., balance: _Optional[int] = ..., position_uid: _Optional[str] = ..., instrument_uid: _Optional[str] = ...) -> None: ...

class PositionsMoney(_message.Message):
    __slots__ = ["available_value", "blocked_value"]
    AVAILABLE_VALUE_FIELD_NUMBER: _ClassVar[int]
    BLOCKED_VALUE_FIELD_NUMBER: _ClassVar[int]
    available_value: _common_pb2.MoneyValue
    blocked_value: _common_pb2.MoneyValue
    def __init__(self, available_value: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., blocked_value: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ...) -> None: ...

class PositionsOptions(_message.Message):
    __slots__ = ["balance", "blocked", "instrument_uid", "position_uid"]
    BALANCE_FIELD_NUMBER: _ClassVar[int]
    BLOCKED_FIELD_NUMBER: _ClassVar[int]
    INSTRUMENT_UID_FIELD_NUMBER: _ClassVar[int]
    POSITION_UID_FIELD_NUMBER: _ClassVar[int]
    balance: int
    blocked: int
    instrument_uid: str
    position_uid: str
    def __init__(self, position_uid: _Optional[str] = ..., instrument_uid: _Optional[str] = ..., blocked: _Optional[int] = ..., balance: _Optional[int] = ...) -> None: ...

class PositionsRequest(_message.Message):
    __slots__ = ["account_id"]
    ACCOUNT_ID_FIELD_NUMBER: _ClassVar[int]
    account_id: str
    def __init__(self, account_id: _Optional[str] = ...) -> None: ...

class PositionsResponse(_message.Message):
    __slots__ = ["blocked", "futures", "limits_loading_in_progress", "money", "options", "securities"]
    BLOCKED_FIELD_NUMBER: _ClassVar[int]
    FUTURES_FIELD_NUMBER: _ClassVar[int]
    LIMITS_LOADING_IN_PROGRESS_FIELD_NUMBER: _ClassVar[int]
    MONEY_FIELD_NUMBER: _ClassVar[int]
    OPTIONS_FIELD_NUMBER: _ClassVar[int]
    SECURITIES_FIELD_NUMBER: _ClassVar[int]
    blocked: _containers.RepeatedCompositeFieldContainer[_common_pb2.MoneyValue]
    futures: _containers.RepeatedCompositeFieldContainer[PositionsFutures]
    limits_loading_in_progress: bool
    money: _containers.RepeatedCompositeFieldContainer[_common_pb2.MoneyValue]
    options: _containers.RepeatedCompositeFieldContainer[PositionsOptions]
    securities: _containers.RepeatedCompositeFieldContainer[PositionsSecurities]
    def __init__(self, money: _Optional[_Iterable[_Union[_common_pb2.MoneyValue, _Mapping]]] = ..., blocked: _Optional[_Iterable[_Union[_common_pb2.MoneyValue, _Mapping]]] = ..., securities: _Optional[_Iterable[_Union[PositionsSecurities, _Mapping]]] = ..., limits_loading_in_progress: bool = ..., futures: _Optional[_Iterable[_Union[PositionsFutures, _Mapping]]] = ..., options: _Optional[_Iterable[_Union[PositionsOptions, _Mapping]]] = ...) -> None: ...

class PositionsSecurities(_message.Message):
    __slots__ = ["balance", "blocked", "exchange_blocked", "figi", "instrument_type", "instrument_uid", "position_uid"]
    BALANCE_FIELD_NUMBER: _ClassVar[int]
    BLOCKED_FIELD_NUMBER: _ClassVar[int]
    EXCHANGE_BLOCKED_FIELD_NUMBER: _ClassVar[int]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    INSTRUMENT_TYPE_FIELD_NUMBER: _ClassVar[int]
    INSTRUMENT_UID_FIELD_NUMBER: _ClassVar[int]
    POSITION_UID_FIELD_NUMBER: _ClassVar[int]
    balance: int
    blocked: int
    exchange_blocked: bool
    figi: str
    instrument_type: str
    instrument_uid: str
    position_uid: str
    def __init__(self, figi: _Optional[str] = ..., blocked: _Optional[int] = ..., balance: _Optional[int] = ..., position_uid: _Optional[str] = ..., instrument_uid: _Optional[str] = ..., exchange_blocked: bool = ..., instrument_type: _Optional[str] = ...) -> None: ...

class PositionsStreamRequest(_message.Message):
    __slots__ = ["accounts"]
    ACCOUNTS_FIELD_NUMBER: _ClassVar[int]
    accounts: _containers.RepeatedScalarFieldContainer[str]
    def __init__(self, accounts: _Optional[_Iterable[str]] = ...) -> None: ...

class PositionsStreamResponse(_message.Message):
    __slots__ = ["ping", "position", "subscriptions"]
    PING_FIELD_NUMBER: _ClassVar[int]
    POSITION_FIELD_NUMBER: _ClassVar[int]
    SUBSCRIPTIONS_FIELD_NUMBER: _ClassVar[int]
    ping: _common_pb2.Ping
    position: PositionData
    subscriptions: PositionsSubscriptionResult
    def __init__(self, subscriptions: _Optional[_Union[PositionsSubscriptionResult, _Mapping]] = ..., position: _Optional[_Union[PositionData, _Mapping]] = ..., ping: _Optional[_Union[_common_pb2.Ping, _Mapping]] = ...) -> None: ...

class PositionsSubscriptionResult(_message.Message):
    __slots__ = ["accounts"]
    ACCOUNTS_FIELD_NUMBER: _ClassVar[int]
    accounts: _containers.RepeatedCompositeFieldContainer[PositionsSubscriptionStatus]
    def __init__(self, accounts: _Optional[_Iterable[_Union[PositionsSubscriptionStatus, _Mapping]]] = ...) -> None: ...

class PositionsSubscriptionStatus(_message.Message):
    __slots__ = ["account_id", "subscription_status"]
    ACCOUNT_ID_FIELD_NUMBER: _ClassVar[int]
    SUBSCRIPTION_STATUS_FIELD_NUMBER: _ClassVar[int]
    account_id: str
    subscription_status: PositionsAccountSubscriptionStatus
    def __init__(self, account_id: _Optional[str] = ..., subscription_status: _Optional[_Union[PositionsAccountSubscriptionStatus, str]] = ...) -> None: ...

class WithdrawLimitsRequest(_message.Message):
    __slots__ = ["account_id"]
    ACCOUNT_ID_FIELD_NUMBER: _ClassVar[int]
    account_id: str
    def __init__(self, account_id: _Optional[str] = ...) -> None: ...

class WithdrawLimitsResponse(_message.Message):
    __slots__ = ["blocked", "blocked_guarantee", "money"]
    BLOCKED_FIELD_NUMBER: _ClassVar[int]
    BLOCKED_GUARANTEE_FIELD_NUMBER: _ClassVar[int]
    MONEY_FIELD_NUMBER: _ClassVar[int]
    blocked: _containers.RepeatedCompositeFieldContainer[_common_pb2.MoneyValue]
    blocked_guarantee: _containers.RepeatedCompositeFieldContainer[_common_pb2.MoneyValue]
    money: _containers.RepeatedCompositeFieldContainer[_common_pb2.MoneyValue]
    def __init__(self, money: _Optional[_Iterable[_Union[_common_pb2.MoneyValue, _Mapping]]] = ..., blocked: _Optional[_Iterable[_Union[_common_pb2.MoneyValue, _Mapping]]] = ..., blocked_guarantee: _Optional[_Iterable[_Union[_common_pb2.MoneyValue, _Mapping]]] = ...) -> None: ...

class OperationState(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []

class OperationType(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []

class PortfolioSubscriptionStatus(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []

class InstrumentType(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []

class PositionsAccountSubscriptionStatus(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
