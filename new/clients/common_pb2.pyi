from google.protobuf import timestamp_pb2 as _timestamp_pb2
from google.protobuf.internal import enum_type_wrapper as _enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Mapping as _Mapping, Optional as _Optional, Union as _Union

DESCRIPTOR: _descriptor.FileDescriptor
SECURITY_TRADING_STATUS_BREAK_IN_TRADING: SecurityTradingStatus
SECURITY_TRADING_STATUS_CLOSING_AUCTION: SecurityTradingStatus
SECURITY_TRADING_STATUS_CLOSING_PERIOD: SecurityTradingStatus
SECURITY_TRADING_STATUS_DARK_POOL_AUCTION: SecurityTradingStatus
SECURITY_TRADING_STATUS_DEALER_BREAK_IN_TRADING: SecurityTradingStatus
SECURITY_TRADING_STATUS_DEALER_NORMAL_TRADING: SecurityTradingStatus
SECURITY_TRADING_STATUS_DEALER_NOT_AVAILABLE_FOR_TRADING: SecurityTradingStatus
SECURITY_TRADING_STATUS_DISCRETE_AUCTION: SecurityTradingStatus
SECURITY_TRADING_STATUS_NORMAL_TRADING: SecurityTradingStatus
SECURITY_TRADING_STATUS_NOT_AVAILABLE_FOR_TRADING: SecurityTradingStatus
SECURITY_TRADING_STATUS_OPENING_AUCTION_PERIOD: SecurityTradingStatus
SECURITY_TRADING_STATUS_OPENING_PERIOD: SecurityTradingStatus
SECURITY_TRADING_STATUS_SESSION_ASSIGNED: SecurityTradingStatus
SECURITY_TRADING_STATUS_SESSION_CLOSE: SecurityTradingStatus
SECURITY_TRADING_STATUS_SESSION_OPEN: SecurityTradingStatus
SECURITY_TRADING_STATUS_TRADING_AT_CLOSING_AUCTION_PRICE: SecurityTradingStatus
SECURITY_TRADING_STATUS_UNSPECIFIED: SecurityTradingStatus

class MoneyValue(_message.Message):
    __slots__ = ["currency", "nano", "units"]
    CURRENCY_FIELD_NUMBER: _ClassVar[int]
    NANO_FIELD_NUMBER: _ClassVar[int]
    UNITS_FIELD_NUMBER: _ClassVar[int]
    currency: str
    nano: int
    units: int
    def __init__(self, currency: _Optional[str] = ..., units: _Optional[int] = ..., nano: _Optional[int] = ...) -> None: ...

class Ping(_message.Message):
    __slots__ = ["time"]
    TIME_FIELD_NUMBER: _ClassVar[int]
    time: _timestamp_pb2.Timestamp
    def __init__(self, time: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ...) -> None: ...

class Quotation(_message.Message):
    __slots__ = ["nano", "units"]
    NANO_FIELD_NUMBER: _ClassVar[int]
    UNITS_FIELD_NUMBER: _ClassVar[int]
    nano: int
    units: int
    def __init__(self, units: _Optional[int] = ..., nano: _Optional[int] = ...) -> None: ...

class SecurityTradingStatus(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
