from google.protobuf import timestamp_pb2 as _timestamp_pb2
import common_pb2 as _common_pb2
from google.protobuf.internal import containers as _containers
from google.protobuf.internal import enum_type_wrapper as _enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Iterable as _Iterable, Mapping as _Mapping, Optional as _Optional, Union as _Union

ASSET_TYPE_COMMODITY: AssetType
ASSET_TYPE_CURRENCY: AssetType
ASSET_TYPE_INDEX: AssetType
ASSET_TYPE_SECURITY: AssetType
ASSET_TYPE_UNSPECIFIED: AssetType
COUPON_TYPE_CONSTANT: CouponType
COUPON_TYPE_DISCOUNT: CouponType
COUPON_TYPE_FIX: CouponType
COUPON_TYPE_FLOATING: CouponType
COUPON_TYPE_MORTGAGE: CouponType
COUPON_TYPE_OTHER: CouponType
COUPON_TYPE_UNSPECIFIED: CouponType
COUPON_TYPE_VARIABLE: CouponType
DESCRIPTOR: _descriptor.FileDescriptor
EDIT_FAVORITES_ACTION_TYPE_ADD: EditFavoritesActionType
EDIT_FAVORITES_ACTION_TYPE_DEL: EditFavoritesActionType
EDIT_FAVORITES_ACTION_TYPE_UNSPECIFIED: EditFavoritesActionType
INSTRUMENT_ID_TYPE_FIGI: InstrumentIdType
INSTRUMENT_ID_TYPE_POSITION_UID: InstrumentIdType
INSTRUMENT_ID_TYPE_TICKER: InstrumentIdType
INSTRUMENT_ID_TYPE_UID: InstrumentIdType
INSTRUMENT_ID_UNSPECIFIED: InstrumentIdType
INSTRUMENT_STATUS_ALL: InstrumentStatus
INSTRUMENT_STATUS_BASE: InstrumentStatus
INSTRUMENT_STATUS_UNSPECIFIED: InstrumentStatus
OPTION_DIRECTION_CALL: OptionDirection
OPTION_DIRECTION_PUT: OptionDirection
OPTION_DIRECTION_UNSPECIFIED: OptionDirection
OPTION_EXECUTION_TYPE_CASH_SETTLEMENT: OptionSettlementType
OPTION_EXECUTION_TYPE_PHYSICAL_DELIVERY: OptionSettlementType
OPTION_EXECUTION_TYPE_UNSPECIFIED: OptionSettlementType
OPTION_PAYMENT_TYPE_MARGINAL: OptionPaymentType
OPTION_PAYMENT_TYPE_PREMIUM: OptionPaymentType
OPTION_PAYMENT_TYPE_UNSPECIFIED: OptionPaymentType
OPTION_STYLE_AMERICAN: OptionStyle
OPTION_STYLE_EUROPEAN: OptionStyle
OPTION_STYLE_UNSPECIFIED: OptionStyle
REAL_EXCHANGE_MOEX: RealExchange
REAL_EXCHANGE_OTC: RealExchange
REAL_EXCHANGE_RTS: RealExchange
REAL_EXCHANGE_UNSPECIFIED: RealExchange
SHARE_TYPE_ADR: ShareType
SHARE_TYPE_CLOSED_END_FUND: ShareType
SHARE_TYPE_COMMON: ShareType
SHARE_TYPE_GDR: ShareType
SHARE_TYPE_MLP: ShareType
SHARE_TYPE_NY_REG_SHRS: ShareType
SHARE_TYPE_PREFERRED: ShareType
SHARE_TYPE_REIT: ShareType
SHARE_TYPE_UNSPECIFIED: ShareType
SP_TYPE_DELIVERABLE: StructuredProductType
SP_TYPE_NON_DELIVERABLE: StructuredProductType
SP_TYPE_UNSPECIFIED: StructuredProductType

class AccruedInterest(_message.Message):
    __slots__ = ["date", "nominal", "value", "value_percent"]
    DATE_FIELD_NUMBER: _ClassVar[int]
    NOMINAL_FIELD_NUMBER: _ClassVar[int]
    VALUE_FIELD_NUMBER: _ClassVar[int]
    VALUE_PERCENT_FIELD_NUMBER: _ClassVar[int]
    date: _timestamp_pb2.Timestamp
    nominal: _common_pb2.Quotation
    value: _common_pb2.Quotation
    value_percent: _common_pb2.Quotation
    def __init__(self, date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., value: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., value_percent: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., nominal: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ...) -> None: ...

class Asset(_message.Message):
    __slots__ = ["instruments", "name", "type", "uid"]
    INSTRUMENTS_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    TYPE_FIELD_NUMBER: _ClassVar[int]
    UID_FIELD_NUMBER: _ClassVar[int]
    instruments: _containers.RepeatedCompositeFieldContainer[AssetInstrument]
    name: str
    type: AssetType
    uid: str
    def __init__(self, uid: _Optional[str] = ..., type: _Optional[_Union[AssetType, str]] = ..., name: _Optional[str] = ..., instruments: _Optional[_Iterable[_Union[AssetInstrument, _Mapping]]] = ...) -> None: ...

class AssetBond(_message.Message):
    __slots__ = ["amortization_flag", "borrow_name", "collateral_flag", "coupon_quantity_per_year", "current_nominal", "floating_coupon_flag", "indexed_nominal_flag", "interest_kind", "issue_kind", "issue_size", "issue_size_plan", "maturity_date", "nominal", "nominal_currency", "perpetual_flag", "placement_date", "placement_price", "return_condition", "state_reg_date", "subordinated_flag", "tax_free_flag"]
    AMORTIZATION_FLAG_FIELD_NUMBER: _ClassVar[int]
    BORROW_NAME_FIELD_NUMBER: _ClassVar[int]
    COLLATERAL_FLAG_FIELD_NUMBER: _ClassVar[int]
    COUPON_QUANTITY_PER_YEAR_FIELD_NUMBER: _ClassVar[int]
    CURRENT_NOMINAL_FIELD_NUMBER: _ClassVar[int]
    FLOATING_COUPON_FLAG_FIELD_NUMBER: _ClassVar[int]
    INDEXED_NOMINAL_FLAG_FIELD_NUMBER: _ClassVar[int]
    INTEREST_KIND_FIELD_NUMBER: _ClassVar[int]
    ISSUE_KIND_FIELD_NUMBER: _ClassVar[int]
    ISSUE_SIZE_FIELD_NUMBER: _ClassVar[int]
    ISSUE_SIZE_PLAN_FIELD_NUMBER: _ClassVar[int]
    MATURITY_DATE_FIELD_NUMBER: _ClassVar[int]
    NOMINAL_CURRENCY_FIELD_NUMBER: _ClassVar[int]
    NOMINAL_FIELD_NUMBER: _ClassVar[int]
    PERPETUAL_FLAG_FIELD_NUMBER: _ClassVar[int]
    PLACEMENT_DATE_FIELD_NUMBER: _ClassVar[int]
    PLACEMENT_PRICE_FIELD_NUMBER: _ClassVar[int]
    RETURN_CONDITION_FIELD_NUMBER: _ClassVar[int]
    STATE_REG_DATE_FIELD_NUMBER: _ClassVar[int]
    SUBORDINATED_FLAG_FIELD_NUMBER: _ClassVar[int]
    TAX_FREE_FLAG_FIELD_NUMBER: _ClassVar[int]
    amortization_flag: bool
    borrow_name: str
    collateral_flag: bool
    coupon_quantity_per_year: int
    current_nominal: _common_pb2.Quotation
    floating_coupon_flag: bool
    indexed_nominal_flag: bool
    interest_kind: str
    issue_kind: str
    issue_size: _common_pb2.Quotation
    issue_size_plan: _common_pb2.Quotation
    maturity_date: _timestamp_pb2.Timestamp
    nominal: _common_pb2.Quotation
    nominal_currency: str
    perpetual_flag: bool
    placement_date: _timestamp_pb2.Timestamp
    placement_price: _common_pb2.Quotation
    return_condition: str
    state_reg_date: _timestamp_pb2.Timestamp
    subordinated_flag: bool
    tax_free_flag: bool
    def __init__(self, current_nominal: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., borrow_name: _Optional[str] = ..., issue_size: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., nominal: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., nominal_currency: _Optional[str] = ..., issue_kind: _Optional[str] = ..., interest_kind: _Optional[str] = ..., coupon_quantity_per_year: _Optional[int] = ..., indexed_nominal_flag: bool = ..., subordinated_flag: bool = ..., collateral_flag: bool = ..., tax_free_flag: bool = ..., amortization_flag: bool = ..., floating_coupon_flag: bool = ..., perpetual_flag: bool = ..., maturity_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., return_condition: _Optional[str] = ..., state_reg_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., placement_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., placement_price: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., issue_size_plan: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ...) -> None: ...

class AssetClearingCertificate(_message.Message):
    __slots__ = ["nominal", "nominal_currency"]
    NOMINAL_CURRENCY_FIELD_NUMBER: _ClassVar[int]
    NOMINAL_FIELD_NUMBER: _ClassVar[int]
    nominal: _common_pb2.Quotation
    nominal_currency: str
    def __init__(self, nominal: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., nominal_currency: _Optional[str] = ...) -> None: ...

class AssetCurrency(_message.Message):
    __slots__ = ["base_currency"]
    BASE_CURRENCY_FIELD_NUMBER: _ClassVar[int]
    base_currency: str
    def __init__(self, base_currency: _Optional[str] = ...) -> None: ...

class AssetEtf(_message.Message):
    __slots__ = ["buy_premium", "description", "div_yield_flag", "expense_commission", "fixed_commission", "focus_type", "hurdle_rate", "inav_code", "index_recovery_period", "issue_kind", "leveraged_flag", "management_type", "nominal", "nominal_currency", "num_share", "payment_type", "performance_fee", "primary_index", "primary_index_company", "primary_index_description", "primary_index_tracking_error", "rebalancing_dates", "rebalancing_flag", "rebalancing_freq", "rebalancing_plan", "released_date", "sell_discount", "tax_rate", "total_expense", "ucits_flag", "watermark_flag"]
    BUY_PREMIUM_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    DIV_YIELD_FLAG_FIELD_NUMBER: _ClassVar[int]
    EXPENSE_COMMISSION_FIELD_NUMBER: _ClassVar[int]
    FIXED_COMMISSION_FIELD_NUMBER: _ClassVar[int]
    FOCUS_TYPE_FIELD_NUMBER: _ClassVar[int]
    HURDLE_RATE_FIELD_NUMBER: _ClassVar[int]
    INAV_CODE_FIELD_NUMBER: _ClassVar[int]
    INDEX_RECOVERY_PERIOD_FIELD_NUMBER: _ClassVar[int]
    ISSUE_KIND_FIELD_NUMBER: _ClassVar[int]
    LEVERAGED_FLAG_FIELD_NUMBER: _ClassVar[int]
    MANAGEMENT_TYPE_FIELD_NUMBER: _ClassVar[int]
    NOMINAL_CURRENCY_FIELD_NUMBER: _ClassVar[int]
    NOMINAL_FIELD_NUMBER: _ClassVar[int]
    NUM_SHARE_FIELD_NUMBER: _ClassVar[int]
    PAYMENT_TYPE_FIELD_NUMBER: _ClassVar[int]
    PERFORMANCE_FEE_FIELD_NUMBER: _ClassVar[int]
    PRIMARY_INDEX_COMPANY_FIELD_NUMBER: _ClassVar[int]
    PRIMARY_INDEX_DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    PRIMARY_INDEX_FIELD_NUMBER: _ClassVar[int]
    PRIMARY_INDEX_TRACKING_ERROR_FIELD_NUMBER: _ClassVar[int]
    REBALANCING_DATES_FIELD_NUMBER: _ClassVar[int]
    REBALANCING_FLAG_FIELD_NUMBER: _ClassVar[int]
    REBALANCING_FREQ_FIELD_NUMBER: _ClassVar[int]
    REBALANCING_PLAN_FIELD_NUMBER: _ClassVar[int]
    RELEASED_DATE_FIELD_NUMBER: _ClassVar[int]
    SELL_DISCOUNT_FIELD_NUMBER: _ClassVar[int]
    TAX_RATE_FIELD_NUMBER: _ClassVar[int]
    TOTAL_EXPENSE_FIELD_NUMBER: _ClassVar[int]
    UCITS_FLAG_FIELD_NUMBER: _ClassVar[int]
    WATERMARK_FLAG_FIELD_NUMBER: _ClassVar[int]
    buy_premium: _common_pb2.Quotation
    description: str
    div_yield_flag: bool
    expense_commission: _common_pb2.Quotation
    fixed_commission: _common_pb2.Quotation
    focus_type: str
    hurdle_rate: _common_pb2.Quotation
    inav_code: str
    index_recovery_period: _common_pb2.Quotation
    issue_kind: str
    leveraged_flag: bool
    management_type: str
    nominal: _common_pb2.Quotation
    nominal_currency: str
    num_share: _common_pb2.Quotation
    payment_type: str
    performance_fee: _common_pb2.Quotation
    primary_index: str
    primary_index_company: str
    primary_index_description: str
    primary_index_tracking_error: _common_pb2.Quotation
    rebalancing_dates: _containers.RepeatedCompositeFieldContainer[_timestamp_pb2.Timestamp]
    rebalancing_flag: bool
    rebalancing_freq: str
    rebalancing_plan: str
    released_date: _timestamp_pb2.Timestamp
    sell_discount: _common_pb2.Quotation
    tax_rate: str
    total_expense: _common_pb2.Quotation
    ucits_flag: bool
    watermark_flag: bool
    def __init__(self, total_expense: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., hurdle_rate: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., performance_fee: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., fixed_commission: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., payment_type: _Optional[str] = ..., watermark_flag: bool = ..., buy_premium: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., sell_discount: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., rebalancing_flag: bool = ..., rebalancing_freq: _Optional[str] = ..., management_type: _Optional[str] = ..., primary_index: _Optional[str] = ..., focus_type: _Optional[str] = ..., leveraged_flag: bool = ..., num_share: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., ucits_flag: bool = ..., released_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., description: _Optional[str] = ..., primary_index_description: _Optional[str] = ..., primary_index_company: _Optional[str] = ..., index_recovery_period: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., inav_code: _Optional[str] = ..., div_yield_flag: bool = ..., expense_commission: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., primary_index_tracking_error: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., rebalancing_plan: _Optional[str] = ..., tax_rate: _Optional[str] = ..., rebalancing_dates: _Optional[_Iterable[_Union[_timestamp_pb2.Timestamp, _Mapping]]] = ..., issue_kind: _Optional[str] = ..., nominal: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., nominal_currency: _Optional[str] = ...) -> None: ...

class AssetFull(_message.Message):
    __slots__ = ["br_code", "br_code_name", "brand", "cfi", "code_nsd", "currency", "deleted_at", "description", "gos_reg_code", "instruments", "name", "name_brief", "required_tests", "security", "status", "type", "uid", "updated_at"]
    BRAND_FIELD_NUMBER: _ClassVar[int]
    BR_CODE_FIELD_NUMBER: _ClassVar[int]
    BR_CODE_NAME_FIELD_NUMBER: _ClassVar[int]
    CFI_FIELD_NUMBER: _ClassVar[int]
    CODE_NSD_FIELD_NUMBER: _ClassVar[int]
    CURRENCY_FIELD_NUMBER: _ClassVar[int]
    DELETED_AT_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    GOS_REG_CODE_FIELD_NUMBER: _ClassVar[int]
    INSTRUMENTS_FIELD_NUMBER: _ClassVar[int]
    NAME_BRIEF_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    REQUIRED_TESTS_FIELD_NUMBER: _ClassVar[int]
    SECURITY_FIELD_NUMBER: _ClassVar[int]
    STATUS_FIELD_NUMBER: _ClassVar[int]
    TYPE_FIELD_NUMBER: _ClassVar[int]
    UID_FIELD_NUMBER: _ClassVar[int]
    UPDATED_AT_FIELD_NUMBER: _ClassVar[int]
    br_code: str
    br_code_name: str
    brand: Brand
    cfi: str
    code_nsd: str
    currency: AssetCurrency
    deleted_at: _timestamp_pb2.Timestamp
    description: str
    gos_reg_code: str
    instruments: _containers.RepeatedCompositeFieldContainer[AssetInstrument]
    name: str
    name_brief: str
    required_tests: _containers.RepeatedScalarFieldContainer[str]
    security: AssetSecurity
    status: str
    type: AssetType
    uid: str
    updated_at: _timestamp_pb2.Timestamp
    def __init__(self, uid: _Optional[str] = ..., type: _Optional[_Union[AssetType, str]] = ..., name: _Optional[str] = ..., name_brief: _Optional[str] = ..., description: _Optional[str] = ..., deleted_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., required_tests: _Optional[_Iterable[str]] = ..., currency: _Optional[_Union[AssetCurrency, _Mapping]] = ..., security: _Optional[_Union[AssetSecurity, _Mapping]] = ..., gos_reg_code: _Optional[str] = ..., cfi: _Optional[str] = ..., code_nsd: _Optional[str] = ..., status: _Optional[str] = ..., brand: _Optional[_Union[Brand, _Mapping]] = ..., updated_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., br_code: _Optional[str] = ..., br_code_name: _Optional[str] = ..., instruments: _Optional[_Iterable[_Union[AssetInstrument, _Mapping]]] = ...) -> None: ...

class AssetInstrument(_message.Message):
    __slots__ = ["class_code", "figi", "instrument_type", "links", "ticker", "uid"]
    CLASS_CODE_FIELD_NUMBER: _ClassVar[int]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    INSTRUMENT_TYPE_FIELD_NUMBER: _ClassVar[int]
    LINKS_FIELD_NUMBER: _ClassVar[int]
    TICKER_FIELD_NUMBER: _ClassVar[int]
    UID_FIELD_NUMBER: _ClassVar[int]
    class_code: str
    figi: str
    instrument_type: str
    links: _containers.RepeatedCompositeFieldContainer[InstrumentLink]
    ticker: str
    uid: str
    def __init__(self, uid: _Optional[str] = ..., figi: _Optional[str] = ..., instrument_type: _Optional[str] = ..., ticker: _Optional[str] = ..., class_code: _Optional[str] = ..., links: _Optional[_Iterable[_Union[InstrumentLink, _Mapping]]] = ...) -> None: ...

class AssetRequest(_message.Message):
    __slots__ = ["id"]
    ID_FIELD_NUMBER: _ClassVar[int]
    id: str
    def __init__(self, id: _Optional[str] = ...) -> None: ...

class AssetResponse(_message.Message):
    __slots__ = ["asset"]
    ASSET_FIELD_NUMBER: _ClassVar[int]
    asset: AssetFull
    def __init__(self, asset: _Optional[_Union[AssetFull, _Mapping]] = ...) -> None: ...

class AssetSecurity(_message.Message):
    __slots__ = ["bond", "clearing_certificate", "etf", "isin", "share", "sp", "type"]
    BOND_FIELD_NUMBER: _ClassVar[int]
    CLEARING_CERTIFICATE_FIELD_NUMBER: _ClassVar[int]
    ETF_FIELD_NUMBER: _ClassVar[int]
    ISIN_FIELD_NUMBER: _ClassVar[int]
    SHARE_FIELD_NUMBER: _ClassVar[int]
    SP_FIELD_NUMBER: _ClassVar[int]
    TYPE_FIELD_NUMBER: _ClassVar[int]
    bond: AssetBond
    clearing_certificate: AssetClearingCertificate
    etf: AssetEtf
    isin: str
    share: AssetShare
    sp: AssetStructuredProduct
    type: str
    def __init__(self, isin: _Optional[str] = ..., type: _Optional[str] = ..., share: _Optional[_Union[AssetShare, _Mapping]] = ..., bond: _Optional[_Union[AssetBond, _Mapping]] = ..., sp: _Optional[_Union[AssetStructuredProduct, _Mapping]] = ..., etf: _Optional[_Union[AssetEtf, _Mapping]] = ..., clearing_certificate: _Optional[_Union[AssetClearingCertificate, _Mapping]] = ...) -> None: ...

class AssetShare(_message.Message):
    __slots__ = ["div_yield_flag", "dividend_rate", "ipo_date", "issue_kind", "issue_size", "issue_size_plan", "nominal", "nominal_currency", "placement_date", "preferred_share_type", "primary_index", "registry_date", "repres_isin", "total_float", "type"]
    DIVIDEND_RATE_FIELD_NUMBER: _ClassVar[int]
    DIV_YIELD_FLAG_FIELD_NUMBER: _ClassVar[int]
    IPO_DATE_FIELD_NUMBER: _ClassVar[int]
    ISSUE_KIND_FIELD_NUMBER: _ClassVar[int]
    ISSUE_SIZE_FIELD_NUMBER: _ClassVar[int]
    ISSUE_SIZE_PLAN_FIELD_NUMBER: _ClassVar[int]
    NOMINAL_CURRENCY_FIELD_NUMBER: _ClassVar[int]
    NOMINAL_FIELD_NUMBER: _ClassVar[int]
    PLACEMENT_DATE_FIELD_NUMBER: _ClassVar[int]
    PREFERRED_SHARE_TYPE_FIELD_NUMBER: _ClassVar[int]
    PRIMARY_INDEX_FIELD_NUMBER: _ClassVar[int]
    REGISTRY_DATE_FIELD_NUMBER: _ClassVar[int]
    REPRES_ISIN_FIELD_NUMBER: _ClassVar[int]
    TOTAL_FLOAT_FIELD_NUMBER: _ClassVar[int]
    TYPE_FIELD_NUMBER: _ClassVar[int]
    div_yield_flag: bool
    dividend_rate: _common_pb2.Quotation
    ipo_date: _timestamp_pb2.Timestamp
    issue_kind: str
    issue_size: _common_pb2.Quotation
    issue_size_plan: _common_pb2.Quotation
    nominal: _common_pb2.Quotation
    nominal_currency: str
    placement_date: _timestamp_pb2.Timestamp
    preferred_share_type: str
    primary_index: str
    registry_date: _timestamp_pb2.Timestamp
    repres_isin: str
    total_float: _common_pb2.Quotation
    type: ShareType
    def __init__(self, type: _Optional[_Union[ShareType, str]] = ..., issue_size: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., nominal: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., nominal_currency: _Optional[str] = ..., primary_index: _Optional[str] = ..., dividend_rate: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., preferred_share_type: _Optional[str] = ..., ipo_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., registry_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., div_yield_flag: bool = ..., issue_kind: _Optional[str] = ..., placement_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., repres_isin: _Optional[str] = ..., issue_size_plan: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., total_float: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ...) -> None: ...

class AssetStructuredProduct(_message.Message):
    __slots__ = ["asset_type", "basic_asset", "borrow_name", "issue_kind", "issue_size", "issue_size_plan", "logic_portfolio", "maturity_date", "nominal", "nominal_currency", "placement_date", "safety_barrier", "type"]
    ASSET_TYPE_FIELD_NUMBER: _ClassVar[int]
    BASIC_ASSET_FIELD_NUMBER: _ClassVar[int]
    BORROW_NAME_FIELD_NUMBER: _ClassVar[int]
    ISSUE_KIND_FIELD_NUMBER: _ClassVar[int]
    ISSUE_SIZE_FIELD_NUMBER: _ClassVar[int]
    ISSUE_SIZE_PLAN_FIELD_NUMBER: _ClassVar[int]
    LOGIC_PORTFOLIO_FIELD_NUMBER: _ClassVar[int]
    MATURITY_DATE_FIELD_NUMBER: _ClassVar[int]
    NOMINAL_CURRENCY_FIELD_NUMBER: _ClassVar[int]
    NOMINAL_FIELD_NUMBER: _ClassVar[int]
    PLACEMENT_DATE_FIELD_NUMBER: _ClassVar[int]
    SAFETY_BARRIER_FIELD_NUMBER: _ClassVar[int]
    TYPE_FIELD_NUMBER: _ClassVar[int]
    asset_type: AssetType
    basic_asset: str
    borrow_name: str
    issue_kind: str
    issue_size: _common_pb2.Quotation
    issue_size_plan: _common_pb2.Quotation
    logic_portfolio: str
    maturity_date: _timestamp_pb2.Timestamp
    nominal: _common_pb2.Quotation
    nominal_currency: str
    placement_date: _timestamp_pb2.Timestamp
    safety_barrier: _common_pb2.Quotation
    type: StructuredProductType
    def __init__(self, borrow_name: _Optional[str] = ..., nominal: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., nominal_currency: _Optional[str] = ..., type: _Optional[_Union[StructuredProductType, str]] = ..., logic_portfolio: _Optional[str] = ..., asset_type: _Optional[_Union[AssetType, str]] = ..., basic_asset: _Optional[str] = ..., safety_barrier: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., maturity_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., issue_size_plan: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., issue_size: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., placement_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., issue_kind: _Optional[str] = ...) -> None: ...

class AssetsRequest(_message.Message):
    __slots__ = []
    def __init__(self) -> None: ...

class AssetsResponse(_message.Message):
    __slots__ = ["assets"]
    ASSETS_FIELD_NUMBER: _ClassVar[int]
    assets: _containers.RepeatedCompositeFieldContainer[Asset]
    def __init__(self, assets: _Optional[_Iterable[_Union[Asset, _Mapping]]] = ...) -> None: ...

class Bond(_message.Message):
    __slots__ = ["aci_value", "amortization_flag", "api_trade_available_flag", "buy_available_flag", "class_code", "country_of_risk", "country_of_risk_name", "coupon_quantity_per_year", "currency", "dlong", "dlong_min", "dshort", "dshort_min", "exchange", "figi", "first_1day_candle_date", "first_1min_candle_date", "floating_coupon_flag", "for_iis_flag", "isin", "issue_kind", "issue_size", "issue_size_plan", "klong", "kshort", "lot", "maturity_date", "min_price_increment", "name", "nominal", "otc_flag", "perpetual_flag", "placement_date", "placement_price", "position_uid", "real_exchange", "sector", "sell_available_flag", "short_enabled_flag", "state_reg_date", "ticker", "trading_status", "uid"]
    ACI_VALUE_FIELD_NUMBER: _ClassVar[int]
    AMORTIZATION_FLAG_FIELD_NUMBER: _ClassVar[int]
    API_TRADE_AVAILABLE_FLAG_FIELD_NUMBER: _ClassVar[int]
    BUY_AVAILABLE_FLAG_FIELD_NUMBER: _ClassVar[int]
    CLASS_CODE_FIELD_NUMBER: _ClassVar[int]
    COUNTRY_OF_RISK_FIELD_NUMBER: _ClassVar[int]
    COUNTRY_OF_RISK_NAME_FIELD_NUMBER: _ClassVar[int]
    COUPON_QUANTITY_PER_YEAR_FIELD_NUMBER: _ClassVar[int]
    CURRENCY_FIELD_NUMBER: _ClassVar[int]
    DLONG_FIELD_NUMBER: _ClassVar[int]
    DLONG_MIN_FIELD_NUMBER: _ClassVar[int]
    DSHORT_FIELD_NUMBER: _ClassVar[int]
    DSHORT_MIN_FIELD_NUMBER: _ClassVar[int]
    EXCHANGE_FIELD_NUMBER: _ClassVar[int]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    FIRST_1DAY_CANDLE_DATE_FIELD_NUMBER: _ClassVar[int]
    FIRST_1MIN_CANDLE_DATE_FIELD_NUMBER: _ClassVar[int]
    FLOATING_COUPON_FLAG_FIELD_NUMBER: _ClassVar[int]
    FOR_IIS_FLAG_FIELD_NUMBER: _ClassVar[int]
    ISIN_FIELD_NUMBER: _ClassVar[int]
    ISSUE_KIND_FIELD_NUMBER: _ClassVar[int]
    ISSUE_SIZE_FIELD_NUMBER: _ClassVar[int]
    ISSUE_SIZE_PLAN_FIELD_NUMBER: _ClassVar[int]
    KLONG_FIELD_NUMBER: _ClassVar[int]
    KSHORT_FIELD_NUMBER: _ClassVar[int]
    LOT_FIELD_NUMBER: _ClassVar[int]
    MATURITY_DATE_FIELD_NUMBER: _ClassVar[int]
    MIN_PRICE_INCREMENT_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    NOMINAL_FIELD_NUMBER: _ClassVar[int]
    OTC_FLAG_FIELD_NUMBER: _ClassVar[int]
    PERPETUAL_FLAG_FIELD_NUMBER: _ClassVar[int]
    PLACEMENT_DATE_FIELD_NUMBER: _ClassVar[int]
    PLACEMENT_PRICE_FIELD_NUMBER: _ClassVar[int]
    POSITION_UID_FIELD_NUMBER: _ClassVar[int]
    REAL_EXCHANGE_FIELD_NUMBER: _ClassVar[int]
    SECTOR_FIELD_NUMBER: _ClassVar[int]
    SELL_AVAILABLE_FLAG_FIELD_NUMBER: _ClassVar[int]
    SHORT_ENABLED_FLAG_FIELD_NUMBER: _ClassVar[int]
    STATE_REG_DATE_FIELD_NUMBER: _ClassVar[int]
    TICKER_FIELD_NUMBER: _ClassVar[int]
    TRADING_STATUS_FIELD_NUMBER: _ClassVar[int]
    UID_FIELD_NUMBER: _ClassVar[int]
    aci_value: _common_pb2.MoneyValue
    amortization_flag: bool
    api_trade_available_flag: bool
    buy_available_flag: bool
    class_code: str
    country_of_risk: str
    country_of_risk_name: str
    coupon_quantity_per_year: int
    currency: str
    dlong: _common_pb2.Quotation
    dlong_min: _common_pb2.Quotation
    dshort: _common_pb2.Quotation
    dshort_min: _common_pb2.Quotation
    exchange: str
    figi: str
    first_1day_candle_date: _timestamp_pb2.Timestamp
    first_1min_candle_date: _timestamp_pb2.Timestamp
    floating_coupon_flag: bool
    for_iis_flag: bool
    isin: str
    issue_kind: str
    issue_size: int
    issue_size_plan: int
    klong: _common_pb2.Quotation
    kshort: _common_pb2.Quotation
    lot: int
    maturity_date: _timestamp_pb2.Timestamp
    min_price_increment: _common_pb2.Quotation
    name: str
    nominal: _common_pb2.MoneyValue
    otc_flag: bool
    perpetual_flag: bool
    placement_date: _timestamp_pb2.Timestamp
    placement_price: _common_pb2.MoneyValue
    position_uid: str
    real_exchange: RealExchange
    sector: str
    sell_available_flag: bool
    short_enabled_flag: bool
    state_reg_date: _timestamp_pb2.Timestamp
    ticker: str
    trading_status: _common_pb2.SecurityTradingStatus
    uid: str
    def __init__(self, figi: _Optional[str] = ..., ticker: _Optional[str] = ..., class_code: _Optional[str] = ..., isin: _Optional[str] = ..., lot: _Optional[int] = ..., currency: _Optional[str] = ..., klong: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., kshort: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., dlong: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., dshort: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., dlong_min: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., dshort_min: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., short_enabled_flag: bool = ..., name: _Optional[str] = ..., exchange: _Optional[str] = ..., coupon_quantity_per_year: _Optional[int] = ..., maturity_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., nominal: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., state_reg_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., placement_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., placement_price: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., aci_value: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., country_of_risk: _Optional[str] = ..., country_of_risk_name: _Optional[str] = ..., sector: _Optional[str] = ..., issue_kind: _Optional[str] = ..., issue_size: _Optional[int] = ..., issue_size_plan: _Optional[int] = ..., trading_status: _Optional[_Union[_common_pb2.SecurityTradingStatus, str]] = ..., otc_flag: bool = ..., buy_available_flag: bool = ..., sell_available_flag: bool = ..., floating_coupon_flag: bool = ..., perpetual_flag: bool = ..., amortization_flag: bool = ..., min_price_increment: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., api_trade_available_flag: bool = ..., uid: _Optional[str] = ..., real_exchange: _Optional[_Union[RealExchange, str]] = ..., position_uid: _Optional[str] = ..., for_iis_flag: bool = ..., first_1min_candle_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., first_1day_candle_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ...) -> None: ...

class BondResponse(_message.Message):
    __slots__ = ["instrument"]
    INSTRUMENT_FIELD_NUMBER: _ClassVar[int]
    instrument: Bond
    def __init__(self, instrument: _Optional[_Union[Bond, _Mapping]] = ...) -> None: ...

class BondsResponse(_message.Message):
    __slots__ = ["instruments"]
    INSTRUMENTS_FIELD_NUMBER: _ClassVar[int]
    instruments: _containers.RepeatedCompositeFieldContainer[Bond]
    def __init__(self, instruments: _Optional[_Iterable[_Union[Bond, _Mapping]]] = ...) -> None: ...

class Brand(_message.Message):
    __slots__ = ["company", "country_of_risk", "country_of_risk_name", "description", "info", "name", "sector", "uid"]
    COMPANY_FIELD_NUMBER: _ClassVar[int]
    COUNTRY_OF_RISK_FIELD_NUMBER: _ClassVar[int]
    COUNTRY_OF_RISK_NAME_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    INFO_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    SECTOR_FIELD_NUMBER: _ClassVar[int]
    UID_FIELD_NUMBER: _ClassVar[int]
    company: str
    country_of_risk: str
    country_of_risk_name: str
    description: str
    info: str
    name: str
    sector: str
    uid: str
    def __init__(self, uid: _Optional[str] = ..., name: _Optional[str] = ..., description: _Optional[str] = ..., info: _Optional[str] = ..., company: _Optional[str] = ..., sector: _Optional[str] = ..., country_of_risk: _Optional[str] = ..., country_of_risk_name: _Optional[str] = ...) -> None: ...

class CountryResponse(_message.Message):
    __slots__ = ["alfa_three", "alfa_two", "name", "name_brief"]
    ALFA_THREE_FIELD_NUMBER: _ClassVar[int]
    ALFA_TWO_FIELD_NUMBER: _ClassVar[int]
    NAME_BRIEF_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    alfa_three: str
    alfa_two: str
    name: str
    name_brief: str
    def __init__(self, alfa_two: _Optional[str] = ..., alfa_three: _Optional[str] = ..., name: _Optional[str] = ..., name_brief: _Optional[str] = ...) -> None: ...

class Coupon(_message.Message):
    __slots__ = ["coupon_date", "coupon_end_date", "coupon_number", "coupon_period", "coupon_start_date", "coupon_type", "figi", "fix_date", "pay_one_bond"]
    COUPON_DATE_FIELD_NUMBER: _ClassVar[int]
    COUPON_END_DATE_FIELD_NUMBER: _ClassVar[int]
    COUPON_NUMBER_FIELD_NUMBER: _ClassVar[int]
    COUPON_PERIOD_FIELD_NUMBER: _ClassVar[int]
    COUPON_START_DATE_FIELD_NUMBER: _ClassVar[int]
    COUPON_TYPE_FIELD_NUMBER: _ClassVar[int]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    FIX_DATE_FIELD_NUMBER: _ClassVar[int]
    PAY_ONE_BOND_FIELD_NUMBER: _ClassVar[int]
    coupon_date: _timestamp_pb2.Timestamp
    coupon_end_date: _timestamp_pb2.Timestamp
    coupon_number: int
    coupon_period: int
    coupon_start_date: _timestamp_pb2.Timestamp
    coupon_type: CouponType
    figi: str
    fix_date: _timestamp_pb2.Timestamp
    pay_one_bond: _common_pb2.MoneyValue
    def __init__(self, figi: _Optional[str] = ..., coupon_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., coupon_number: _Optional[int] = ..., fix_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., pay_one_bond: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., coupon_type: _Optional[_Union[CouponType, str]] = ..., coupon_start_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., coupon_end_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., coupon_period: _Optional[int] = ...) -> None: ...

class CurrenciesResponse(_message.Message):
    __slots__ = ["instruments"]
    INSTRUMENTS_FIELD_NUMBER: _ClassVar[int]
    instruments: _containers.RepeatedCompositeFieldContainer[Currency]
    def __init__(self, instruments: _Optional[_Iterable[_Union[Currency, _Mapping]]] = ...) -> None: ...

class Currency(_message.Message):
    __slots__ = ["api_trade_available_flag", "buy_available_flag", "class_code", "country_of_risk", "country_of_risk_name", "currency", "dlong", "dlong_min", "dshort", "dshort_min", "exchange", "figi", "first_1day_candle_date", "first_1min_candle_date", "for_iis_flag", "isin", "iso_currency_name", "klong", "kshort", "lot", "min_price_increment", "name", "nominal", "otc_flag", "position_uid", "real_exchange", "sell_available_flag", "short_enabled_flag", "ticker", "trading_status", "uid"]
    API_TRADE_AVAILABLE_FLAG_FIELD_NUMBER: _ClassVar[int]
    BUY_AVAILABLE_FLAG_FIELD_NUMBER: _ClassVar[int]
    CLASS_CODE_FIELD_NUMBER: _ClassVar[int]
    COUNTRY_OF_RISK_FIELD_NUMBER: _ClassVar[int]
    COUNTRY_OF_RISK_NAME_FIELD_NUMBER: _ClassVar[int]
    CURRENCY_FIELD_NUMBER: _ClassVar[int]
    DLONG_FIELD_NUMBER: _ClassVar[int]
    DLONG_MIN_FIELD_NUMBER: _ClassVar[int]
    DSHORT_FIELD_NUMBER: _ClassVar[int]
    DSHORT_MIN_FIELD_NUMBER: _ClassVar[int]
    EXCHANGE_FIELD_NUMBER: _ClassVar[int]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    FIRST_1DAY_CANDLE_DATE_FIELD_NUMBER: _ClassVar[int]
    FIRST_1MIN_CANDLE_DATE_FIELD_NUMBER: _ClassVar[int]
    FOR_IIS_FLAG_FIELD_NUMBER: _ClassVar[int]
    ISIN_FIELD_NUMBER: _ClassVar[int]
    ISO_CURRENCY_NAME_FIELD_NUMBER: _ClassVar[int]
    KLONG_FIELD_NUMBER: _ClassVar[int]
    KSHORT_FIELD_NUMBER: _ClassVar[int]
    LOT_FIELD_NUMBER: _ClassVar[int]
    MIN_PRICE_INCREMENT_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    NOMINAL_FIELD_NUMBER: _ClassVar[int]
    OTC_FLAG_FIELD_NUMBER: _ClassVar[int]
    POSITION_UID_FIELD_NUMBER: _ClassVar[int]
    REAL_EXCHANGE_FIELD_NUMBER: _ClassVar[int]
    SELL_AVAILABLE_FLAG_FIELD_NUMBER: _ClassVar[int]
    SHORT_ENABLED_FLAG_FIELD_NUMBER: _ClassVar[int]
    TICKER_FIELD_NUMBER: _ClassVar[int]
    TRADING_STATUS_FIELD_NUMBER: _ClassVar[int]
    UID_FIELD_NUMBER: _ClassVar[int]
    api_trade_available_flag: bool
    buy_available_flag: bool
    class_code: str
    country_of_risk: str
    country_of_risk_name: str
    currency: str
    dlong: _common_pb2.Quotation
    dlong_min: _common_pb2.Quotation
    dshort: _common_pb2.Quotation
    dshort_min: _common_pb2.Quotation
    exchange: str
    figi: str
    first_1day_candle_date: _timestamp_pb2.Timestamp
    first_1min_candle_date: _timestamp_pb2.Timestamp
    for_iis_flag: bool
    isin: str
    iso_currency_name: str
    klong: _common_pb2.Quotation
    kshort: _common_pb2.Quotation
    lot: int
    min_price_increment: _common_pb2.Quotation
    name: str
    nominal: _common_pb2.MoneyValue
    otc_flag: bool
    position_uid: str
    real_exchange: RealExchange
    sell_available_flag: bool
    short_enabled_flag: bool
    ticker: str
    trading_status: _common_pb2.SecurityTradingStatus
    uid: str
    def __init__(self, figi: _Optional[str] = ..., ticker: _Optional[str] = ..., class_code: _Optional[str] = ..., isin: _Optional[str] = ..., lot: _Optional[int] = ..., currency: _Optional[str] = ..., klong: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., kshort: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., dlong: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., dshort: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., dlong_min: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., dshort_min: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., short_enabled_flag: bool = ..., name: _Optional[str] = ..., exchange: _Optional[str] = ..., nominal: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., country_of_risk: _Optional[str] = ..., country_of_risk_name: _Optional[str] = ..., trading_status: _Optional[_Union[_common_pb2.SecurityTradingStatus, str]] = ..., otc_flag: bool = ..., buy_available_flag: bool = ..., sell_available_flag: bool = ..., iso_currency_name: _Optional[str] = ..., min_price_increment: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., api_trade_available_flag: bool = ..., uid: _Optional[str] = ..., real_exchange: _Optional[_Union[RealExchange, str]] = ..., position_uid: _Optional[str] = ..., for_iis_flag: bool = ..., first_1min_candle_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., first_1day_candle_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ...) -> None: ...

class CurrencyResponse(_message.Message):
    __slots__ = ["instrument"]
    INSTRUMENT_FIELD_NUMBER: _ClassVar[int]
    instrument: Currency
    def __init__(self, instrument: _Optional[_Union[Currency, _Mapping]] = ...) -> None: ...

class Dividend(_message.Message):
    __slots__ = ["close_price", "created_at", "declared_date", "dividend_net", "dividend_type", "last_buy_date", "payment_date", "record_date", "regularity", "yield_value"]
    CLOSE_PRICE_FIELD_NUMBER: _ClassVar[int]
    CREATED_AT_FIELD_NUMBER: _ClassVar[int]
    DECLARED_DATE_FIELD_NUMBER: _ClassVar[int]
    DIVIDEND_NET_FIELD_NUMBER: _ClassVar[int]
    DIVIDEND_TYPE_FIELD_NUMBER: _ClassVar[int]
    LAST_BUY_DATE_FIELD_NUMBER: _ClassVar[int]
    PAYMENT_DATE_FIELD_NUMBER: _ClassVar[int]
    RECORD_DATE_FIELD_NUMBER: _ClassVar[int]
    REGULARITY_FIELD_NUMBER: _ClassVar[int]
    YIELD_VALUE_FIELD_NUMBER: _ClassVar[int]
    close_price: _common_pb2.MoneyValue
    created_at: _timestamp_pb2.Timestamp
    declared_date: _timestamp_pb2.Timestamp
    dividend_net: _common_pb2.MoneyValue
    dividend_type: str
    last_buy_date: _timestamp_pb2.Timestamp
    payment_date: _timestamp_pb2.Timestamp
    record_date: _timestamp_pb2.Timestamp
    regularity: str
    yield_value: _common_pb2.Quotation
    def __init__(self, dividend_net: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., payment_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., declared_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., last_buy_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., dividend_type: _Optional[str] = ..., record_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., regularity: _Optional[str] = ..., close_price: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., yield_value: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., created_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ...) -> None: ...

class EditFavoritesRequest(_message.Message):
    __slots__ = ["action_type", "instruments"]
    ACTION_TYPE_FIELD_NUMBER: _ClassVar[int]
    INSTRUMENTS_FIELD_NUMBER: _ClassVar[int]
    action_type: EditFavoritesActionType
    instruments: _containers.RepeatedCompositeFieldContainer[EditFavoritesRequestInstrument]
    def __init__(self, instruments: _Optional[_Iterable[_Union[EditFavoritesRequestInstrument, _Mapping]]] = ..., action_type: _Optional[_Union[EditFavoritesActionType, str]] = ...) -> None: ...

class EditFavoritesRequestInstrument(_message.Message):
    __slots__ = ["figi"]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    figi: str
    def __init__(self, figi: _Optional[str] = ...) -> None: ...

class EditFavoritesResponse(_message.Message):
    __slots__ = ["favorite_instruments"]
    FAVORITE_INSTRUMENTS_FIELD_NUMBER: _ClassVar[int]
    favorite_instruments: _containers.RepeatedCompositeFieldContainer[FavoriteInstrument]
    def __init__(self, favorite_instruments: _Optional[_Iterable[_Union[FavoriteInstrument, _Mapping]]] = ...) -> None: ...

class Etf(_message.Message):
    __slots__ = ["api_trade_available_flag", "buy_available_flag", "class_code", "country_of_risk", "country_of_risk_name", "currency", "dlong", "dlong_min", "dshort", "dshort_min", "exchange", "figi", "first_1day_candle_date", "first_1min_candle_date", "fixed_commission", "focus_type", "for_iis_flag", "isin", "klong", "kshort", "lot", "min_price_increment", "name", "num_shares", "otc_flag", "position_uid", "real_exchange", "rebalancing_freq", "released_date", "sector", "sell_available_flag", "short_enabled_flag", "ticker", "trading_status", "uid"]
    API_TRADE_AVAILABLE_FLAG_FIELD_NUMBER: _ClassVar[int]
    BUY_AVAILABLE_FLAG_FIELD_NUMBER: _ClassVar[int]
    CLASS_CODE_FIELD_NUMBER: _ClassVar[int]
    COUNTRY_OF_RISK_FIELD_NUMBER: _ClassVar[int]
    COUNTRY_OF_RISK_NAME_FIELD_NUMBER: _ClassVar[int]
    CURRENCY_FIELD_NUMBER: _ClassVar[int]
    DLONG_FIELD_NUMBER: _ClassVar[int]
    DLONG_MIN_FIELD_NUMBER: _ClassVar[int]
    DSHORT_FIELD_NUMBER: _ClassVar[int]
    DSHORT_MIN_FIELD_NUMBER: _ClassVar[int]
    EXCHANGE_FIELD_NUMBER: _ClassVar[int]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    FIRST_1DAY_CANDLE_DATE_FIELD_NUMBER: _ClassVar[int]
    FIRST_1MIN_CANDLE_DATE_FIELD_NUMBER: _ClassVar[int]
    FIXED_COMMISSION_FIELD_NUMBER: _ClassVar[int]
    FOCUS_TYPE_FIELD_NUMBER: _ClassVar[int]
    FOR_IIS_FLAG_FIELD_NUMBER: _ClassVar[int]
    ISIN_FIELD_NUMBER: _ClassVar[int]
    KLONG_FIELD_NUMBER: _ClassVar[int]
    KSHORT_FIELD_NUMBER: _ClassVar[int]
    LOT_FIELD_NUMBER: _ClassVar[int]
    MIN_PRICE_INCREMENT_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    NUM_SHARES_FIELD_NUMBER: _ClassVar[int]
    OTC_FLAG_FIELD_NUMBER: _ClassVar[int]
    POSITION_UID_FIELD_NUMBER: _ClassVar[int]
    REAL_EXCHANGE_FIELD_NUMBER: _ClassVar[int]
    REBALANCING_FREQ_FIELD_NUMBER: _ClassVar[int]
    RELEASED_DATE_FIELD_NUMBER: _ClassVar[int]
    SECTOR_FIELD_NUMBER: _ClassVar[int]
    SELL_AVAILABLE_FLAG_FIELD_NUMBER: _ClassVar[int]
    SHORT_ENABLED_FLAG_FIELD_NUMBER: _ClassVar[int]
    TICKER_FIELD_NUMBER: _ClassVar[int]
    TRADING_STATUS_FIELD_NUMBER: _ClassVar[int]
    UID_FIELD_NUMBER: _ClassVar[int]
    api_trade_available_flag: bool
    buy_available_flag: bool
    class_code: str
    country_of_risk: str
    country_of_risk_name: str
    currency: str
    dlong: _common_pb2.Quotation
    dlong_min: _common_pb2.Quotation
    dshort: _common_pb2.Quotation
    dshort_min: _common_pb2.Quotation
    exchange: str
    figi: str
    first_1day_candle_date: _timestamp_pb2.Timestamp
    first_1min_candle_date: _timestamp_pb2.Timestamp
    fixed_commission: _common_pb2.Quotation
    focus_type: str
    for_iis_flag: bool
    isin: str
    klong: _common_pb2.Quotation
    kshort: _common_pb2.Quotation
    lot: int
    min_price_increment: _common_pb2.Quotation
    name: str
    num_shares: _common_pb2.Quotation
    otc_flag: bool
    position_uid: str
    real_exchange: RealExchange
    rebalancing_freq: str
    released_date: _timestamp_pb2.Timestamp
    sector: str
    sell_available_flag: bool
    short_enabled_flag: bool
    ticker: str
    trading_status: _common_pb2.SecurityTradingStatus
    uid: str
    def __init__(self, figi: _Optional[str] = ..., ticker: _Optional[str] = ..., class_code: _Optional[str] = ..., isin: _Optional[str] = ..., lot: _Optional[int] = ..., currency: _Optional[str] = ..., klong: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., kshort: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., dlong: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., dshort: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., dlong_min: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., dshort_min: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., short_enabled_flag: bool = ..., name: _Optional[str] = ..., exchange: _Optional[str] = ..., fixed_commission: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., focus_type: _Optional[str] = ..., released_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., num_shares: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., country_of_risk: _Optional[str] = ..., country_of_risk_name: _Optional[str] = ..., sector: _Optional[str] = ..., rebalancing_freq: _Optional[str] = ..., trading_status: _Optional[_Union[_common_pb2.SecurityTradingStatus, str]] = ..., otc_flag: bool = ..., buy_available_flag: bool = ..., sell_available_flag: bool = ..., min_price_increment: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., api_trade_available_flag: bool = ..., uid: _Optional[str] = ..., real_exchange: _Optional[_Union[RealExchange, str]] = ..., position_uid: _Optional[str] = ..., for_iis_flag: bool = ..., first_1min_candle_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., first_1day_candle_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ...) -> None: ...

class EtfResponse(_message.Message):
    __slots__ = ["instrument"]
    INSTRUMENT_FIELD_NUMBER: _ClassVar[int]
    instrument: Etf
    def __init__(self, instrument: _Optional[_Union[Etf, _Mapping]] = ...) -> None: ...

class EtfsResponse(_message.Message):
    __slots__ = ["instruments"]
    INSTRUMENTS_FIELD_NUMBER: _ClassVar[int]
    instruments: _containers.RepeatedCompositeFieldContainer[Etf]
    def __init__(self, instruments: _Optional[_Iterable[_Union[Etf, _Mapping]]] = ...) -> None: ...

class FavoriteInstrument(_message.Message):
    __slots__ = ["api_trade_available_flag", "class_code", "figi", "instrument_type", "isin", "otc_flag", "ticker"]
    API_TRADE_AVAILABLE_FLAG_FIELD_NUMBER: _ClassVar[int]
    CLASS_CODE_FIELD_NUMBER: _ClassVar[int]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    INSTRUMENT_TYPE_FIELD_NUMBER: _ClassVar[int]
    ISIN_FIELD_NUMBER: _ClassVar[int]
    OTC_FLAG_FIELD_NUMBER: _ClassVar[int]
    TICKER_FIELD_NUMBER: _ClassVar[int]
    api_trade_available_flag: bool
    class_code: str
    figi: str
    instrument_type: str
    isin: str
    otc_flag: bool
    ticker: str
    def __init__(self, figi: _Optional[str] = ..., ticker: _Optional[str] = ..., class_code: _Optional[str] = ..., isin: _Optional[str] = ..., instrument_type: _Optional[str] = ..., otc_flag: bool = ..., api_trade_available_flag: bool = ...) -> None: ...

class FindInstrumentRequest(_message.Message):
    __slots__ = ["query"]
    QUERY_FIELD_NUMBER: _ClassVar[int]
    query: str
    def __init__(self, query: _Optional[str] = ...) -> None: ...

class FindInstrumentResponse(_message.Message):
    __slots__ = ["instruments"]
    INSTRUMENTS_FIELD_NUMBER: _ClassVar[int]
    instruments: _containers.RepeatedCompositeFieldContainer[InstrumentShort]
    def __init__(self, instruments: _Optional[_Iterable[_Union[InstrumentShort, _Mapping]]] = ...) -> None: ...

class Future(_message.Message):
    __slots__ = ["api_trade_available_flag", "asset_type", "basic_asset", "basic_asset_position_uid", "basic_asset_size", "buy_available_flag", "class_code", "country_of_risk", "country_of_risk_name", "currency", "dlong", "dlong_min", "dshort", "dshort_min", "exchange", "expiration_date", "figi", "first_1day_candle_date", "first_1min_candle_date", "first_trade_date", "for_iis_flag", "futures_type", "klong", "kshort", "last_trade_date", "lot", "min_price_increment", "name", "otc_flag", "position_uid", "real_exchange", "sector", "sell_available_flag", "short_enabled_flag", "ticker", "trading_status", "uid"]
    API_TRADE_AVAILABLE_FLAG_FIELD_NUMBER: _ClassVar[int]
    ASSET_TYPE_FIELD_NUMBER: _ClassVar[int]
    BASIC_ASSET_FIELD_NUMBER: _ClassVar[int]
    BASIC_ASSET_POSITION_UID_FIELD_NUMBER: _ClassVar[int]
    BASIC_ASSET_SIZE_FIELD_NUMBER: _ClassVar[int]
    BUY_AVAILABLE_FLAG_FIELD_NUMBER: _ClassVar[int]
    CLASS_CODE_FIELD_NUMBER: _ClassVar[int]
    COUNTRY_OF_RISK_FIELD_NUMBER: _ClassVar[int]
    COUNTRY_OF_RISK_NAME_FIELD_NUMBER: _ClassVar[int]
    CURRENCY_FIELD_NUMBER: _ClassVar[int]
    DLONG_FIELD_NUMBER: _ClassVar[int]
    DLONG_MIN_FIELD_NUMBER: _ClassVar[int]
    DSHORT_FIELD_NUMBER: _ClassVar[int]
    DSHORT_MIN_FIELD_NUMBER: _ClassVar[int]
    EXCHANGE_FIELD_NUMBER: _ClassVar[int]
    EXPIRATION_DATE_FIELD_NUMBER: _ClassVar[int]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    FIRST_1DAY_CANDLE_DATE_FIELD_NUMBER: _ClassVar[int]
    FIRST_1MIN_CANDLE_DATE_FIELD_NUMBER: _ClassVar[int]
    FIRST_TRADE_DATE_FIELD_NUMBER: _ClassVar[int]
    FOR_IIS_FLAG_FIELD_NUMBER: _ClassVar[int]
    FUTURES_TYPE_FIELD_NUMBER: _ClassVar[int]
    KLONG_FIELD_NUMBER: _ClassVar[int]
    KSHORT_FIELD_NUMBER: _ClassVar[int]
    LAST_TRADE_DATE_FIELD_NUMBER: _ClassVar[int]
    LOT_FIELD_NUMBER: _ClassVar[int]
    MIN_PRICE_INCREMENT_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    OTC_FLAG_FIELD_NUMBER: _ClassVar[int]
    POSITION_UID_FIELD_NUMBER: _ClassVar[int]
    REAL_EXCHANGE_FIELD_NUMBER: _ClassVar[int]
    SECTOR_FIELD_NUMBER: _ClassVar[int]
    SELL_AVAILABLE_FLAG_FIELD_NUMBER: _ClassVar[int]
    SHORT_ENABLED_FLAG_FIELD_NUMBER: _ClassVar[int]
    TICKER_FIELD_NUMBER: _ClassVar[int]
    TRADING_STATUS_FIELD_NUMBER: _ClassVar[int]
    UID_FIELD_NUMBER: _ClassVar[int]
    api_trade_available_flag: bool
    asset_type: str
    basic_asset: str
    basic_asset_position_uid: str
    basic_asset_size: _common_pb2.Quotation
    buy_available_flag: bool
    class_code: str
    country_of_risk: str
    country_of_risk_name: str
    currency: str
    dlong: _common_pb2.Quotation
    dlong_min: _common_pb2.Quotation
    dshort: _common_pb2.Quotation
    dshort_min: _common_pb2.Quotation
    exchange: str
    expiration_date: _timestamp_pb2.Timestamp
    figi: str
    first_1day_candle_date: _timestamp_pb2.Timestamp
    first_1min_candle_date: _timestamp_pb2.Timestamp
    first_trade_date: _timestamp_pb2.Timestamp
    for_iis_flag: bool
    futures_type: str
    klong: _common_pb2.Quotation
    kshort: _common_pb2.Quotation
    last_trade_date: _timestamp_pb2.Timestamp
    lot: int
    min_price_increment: _common_pb2.Quotation
    name: str
    otc_flag: bool
    position_uid: str
    real_exchange: RealExchange
    sector: str
    sell_available_flag: bool
    short_enabled_flag: bool
    ticker: str
    trading_status: _common_pb2.SecurityTradingStatus
    uid: str
    def __init__(self, figi: _Optional[str] = ..., ticker: _Optional[str] = ..., class_code: _Optional[str] = ..., lot: _Optional[int] = ..., currency: _Optional[str] = ..., klong: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., kshort: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., dlong: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., dshort: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., dlong_min: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., dshort_min: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., short_enabled_flag: bool = ..., name: _Optional[str] = ..., exchange: _Optional[str] = ..., first_trade_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., last_trade_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., futures_type: _Optional[str] = ..., asset_type: _Optional[str] = ..., basic_asset: _Optional[str] = ..., basic_asset_size: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., country_of_risk: _Optional[str] = ..., country_of_risk_name: _Optional[str] = ..., sector: _Optional[str] = ..., expiration_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., trading_status: _Optional[_Union[_common_pb2.SecurityTradingStatus, str]] = ..., otc_flag: bool = ..., buy_available_flag: bool = ..., sell_available_flag: bool = ..., min_price_increment: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., api_trade_available_flag: bool = ..., uid: _Optional[str] = ..., real_exchange: _Optional[_Union[RealExchange, str]] = ..., position_uid: _Optional[str] = ..., basic_asset_position_uid: _Optional[str] = ..., for_iis_flag: bool = ..., first_1min_candle_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., first_1day_candle_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ...) -> None: ...

class FutureResponse(_message.Message):
    __slots__ = ["instrument"]
    INSTRUMENT_FIELD_NUMBER: _ClassVar[int]
    instrument: Future
    def __init__(self, instrument: _Optional[_Union[Future, _Mapping]] = ...) -> None: ...

class FuturesResponse(_message.Message):
    __slots__ = ["instruments"]
    INSTRUMENTS_FIELD_NUMBER: _ClassVar[int]
    instruments: _containers.RepeatedCompositeFieldContainer[Future]
    def __init__(self, instruments: _Optional[_Iterable[_Union[Future, _Mapping]]] = ...) -> None: ...

class GetAccruedInterestsRequest(_message.Message):
    __slots__ = ["figi", "to"]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    FROM_FIELD_NUMBER: _ClassVar[int]
    TO_FIELD_NUMBER: _ClassVar[int]
    figi: str
    to: _timestamp_pb2.Timestamp
    def __init__(self, figi: _Optional[str] = ..., to: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., **kwargs) -> None: ...

class GetAccruedInterestsResponse(_message.Message):
    __slots__ = ["accrued_interests"]
    ACCRUED_INTERESTS_FIELD_NUMBER: _ClassVar[int]
    accrued_interests: _containers.RepeatedCompositeFieldContainer[AccruedInterest]
    def __init__(self, accrued_interests: _Optional[_Iterable[_Union[AccruedInterest, _Mapping]]] = ...) -> None: ...

class GetBondCouponsRequest(_message.Message):
    __slots__ = ["figi", "to"]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    FROM_FIELD_NUMBER: _ClassVar[int]
    TO_FIELD_NUMBER: _ClassVar[int]
    figi: str
    to: _timestamp_pb2.Timestamp
    def __init__(self, figi: _Optional[str] = ..., to: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., **kwargs) -> None: ...

class GetBondCouponsResponse(_message.Message):
    __slots__ = ["events"]
    EVENTS_FIELD_NUMBER: _ClassVar[int]
    events: _containers.RepeatedCompositeFieldContainer[Coupon]
    def __init__(self, events: _Optional[_Iterable[_Union[Coupon, _Mapping]]] = ...) -> None: ...

class GetBrandRequest(_message.Message):
    __slots__ = ["id"]
    ID_FIELD_NUMBER: _ClassVar[int]
    id: str
    def __init__(self, id: _Optional[str] = ...) -> None: ...

class GetBrandsRequest(_message.Message):
    __slots__ = []
    def __init__(self) -> None: ...

class GetBrandsResponse(_message.Message):
    __slots__ = ["brands"]
    BRANDS_FIELD_NUMBER: _ClassVar[int]
    brands: _containers.RepeatedCompositeFieldContainer[Brand]
    def __init__(self, brands: _Optional[_Iterable[_Union[Brand, _Mapping]]] = ...) -> None: ...

class GetCountriesRequest(_message.Message):
    __slots__ = []
    def __init__(self) -> None: ...

class GetCountriesResponse(_message.Message):
    __slots__ = ["countries"]
    COUNTRIES_FIELD_NUMBER: _ClassVar[int]
    countries: _containers.RepeatedCompositeFieldContainer[CountryResponse]
    def __init__(self, countries: _Optional[_Iterable[_Union[CountryResponse, _Mapping]]] = ...) -> None: ...

class GetDividendsRequest(_message.Message):
    __slots__ = ["figi", "to"]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    FROM_FIELD_NUMBER: _ClassVar[int]
    TO_FIELD_NUMBER: _ClassVar[int]
    figi: str
    to: _timestamp_pb2.Timestamp
    def __init__(self, figi: _Optional[str] = ..., to: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., **kwargs) -> None: ...

class GetDividendsResponse(_message.Message):
    __slots__ = ["dividends"]
    DIVIDENDS_FIELD_NUMBER: _ClassVar[int]
    dividends: _containers.RepeatedCompositeFieldContainer[Dividend]
    def __init__(self, dividends: _Optional[_Iterable[_Union[Dividend, _Mapping]]] = ...) -> None: ...

class GetFavoritesRequest(_message.Message):
    __slots__ = []
    def __init__(self) -> None: ...

class GetFavoritesResponse(_message.Message):
    __slots__ = ["favorite_instruments"]
    FAVORITE_INSTRUMENTS_FIELD_NUMBER: _ClassVar[int]
    favorite_instruments: _containers.RepeatedCompositeFieldContainer[FavoriteInstrument]
    def __init__(self, favorite_instruments: _Optional[_Iterable[_Union[FavoriteInstrument, _Mapping]]] = ...) -> None: ...

class GetFuturesMarginRequest(_message.Message):
    __slots__ = ["figi"]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    figi: str
    def __init__(self, figi: _Optional[str] = ...) -> None: ...

class GetFuturesMarginResponse(_message.Message):
    __slots__ = ["initial_margin_on_buy", "initial_margin_on_sell", "min_price_increment", "min_price_increment_amount"]
    INITIAL_MARGIN_ON_BUY_FIELD_NUMBER: _ClassVar[int]
    INITIAL_MARGIN_ON_SELL_FIELD_NUMBER: _ClassVar[int]
    MIN_PRICE_INCREMENT_AMOUNT_FIELD_NUMBER: _ClassVar[int]
    MIN_PRICE_INCREMENT_FIELD_NUMBER: _ClassVar[int]
    initial_margin_on_buy: _common_pb2.MoneyValue
    initial_margin_on_sell: _common_pb2.MoneyValue
    min_price_increment: _common_pb2.Quotation
    min_price_increment_amount: _common_pb2.Quotation
    def __init__(self, initial_margin_on_buy: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., initial_margin_on_sell: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., min_price_increment: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., min_price_increment_amount: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ...) -> None: ...

class Instrument(_message.Message):
    __slots__ = ["api_trade_available_flag", "buy_available_flag", "class_code", "country_of_risk", "country_of_risk_name", "currency", "dlong", "dlong_min", "dshort", "dshort_min", "exchange", "figi", "first_1day_candle_date", "first_1min_candle_date", "for_iis_flag", "instrument_type", "isin", "klong", "kshort", "lot", "min_price_increment", "name", "otc_flag", "position_uid", "real_exchange", "sell_available_flag", "short_enabled_flag", "ticker", "trading_status", "uid"]
    API_TRADE_AVAILABLE_FLAG_FIELD_NUMBER: _ClassVar[int]
    BUY_AVAILABLE_FLAG_FIELD_NUMBER: _ClassVar[int]
    CLASS_CODE_FIELD_NUMBER: _ClassVar[int]
    COUNTRY_OF_RISK_FIELD_NUMBER: _ClassVar[int]
    COUNTRY_OF_RISK_NAME_FIELD_NUMBER: _ClassVar[int]
    CURRENCY_FIELD_NUMBER: _ClassVar[int]
    DLONG_FIELD_NUMBER: _ClassVar[int]
    DLONG_MIN_FIELD_NUMBER: _ClassVar[int]
    DSHORT_FIELD_NUMBER: _ClassVar[int]
    DSHORT_MIN_FIELD_NUMBER: _ClassVar[int]
    EXCHANGE_FIELD_NUMBER: _ClassVar[int]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    FIRST_1DAY_CANDLE_DATE_FIELD_NUMBER: _ClassVar[int]
    FIRST_1MIN_CANDLE_DATE_FIELD_NUMBER: _ClassVar[int]
    FOR_IIS_FLAG_FIELD_NUMBER: _ClassVar[int]
    INSTRUMENT_TYPE_FIELD_NUMBER: _ClassVar[int]
    ISIN_FIELD_NUMBER: _ClassVar[int]
    KLONG_FIELD_NUMBER: _ClassVar[int]
    KSHORT_FIELD_NUMBER: _ClassVar[int]
    LOT_FIELD_NUMBER: _ClassVar[int]
    MIN_PRICE_INCREMENT_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    OTC_FLAG_FIELD_NUMBER: _ClassVar[int]
    POSITION_UID_FIELD_NUMBER: _ClassVar[int]
    REAL_EXCHANGE_FIELD_NUMBER: _ClassVar[int]
    SELL_AVAILABLE_FLAG_FIELD_NUMBER: _ClassVar[int]
    SHORT_ENABLED_FLAG_FIELD_NUMBER: _ClassVar[int]
    TICKER_FIELD_NUMBER: _ClassVar[int]
    TRADING_STATUS_FIELD_NUMBER: _ClassVar[int]
    UID_FIELD_NUMBER: _ClassVar[int]
    api_trade_available_flag: bool
    buy_available_flag: bool
    class_code: str
    country_of_risk: str
    country_of_risk_name: str
    currency: str
    dlong: _common_pb2.Quotation
    dlong_min: _common_pb2.Quotation
    dshort: _common_pb2.Quotation
    dshort_min: _common_pb2.Quotation
    exchange: str
    figi: str
    first_1day_candle_date: _timestamp_pb2.Timestamp
    first_1min_candle_date: _timestamp_pb2.Timestamp
    for_iis_flag: bool
    instrument_type: str
    isin: str
    klong: _common_pb2.Quotation
    kshort: _common_pb2.Quotation
    lot: int
    min_price_increment: _common_pb2.Quotation
    name: str
    otc_flag: bool
    position_uid: str
    real_exchange: RealExchange
    sell_available_flag: bool
    short_enabled_flag: bool
    ticker: str
    trading_status: _common_pb2.SecurityTradingStatus
    uid: str
    def __init__(self, figi: _Optional[str] = ..., ticker: _Optional[str] = ..., class_code: _Optional[str] = ..., isin: _Optional[str] = ..., lot: _Optional[int] = ..., currency: _Optional[str] = ..., klong: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., kshort: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., dlong: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., dshort: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., dlong_min: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., dshort_min: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., short_enabled_flag: bool = ..., name: _Optional[str] = ..., exchange: _Optional[str] = ..., country_of_risk: _Optional[str] = ..., country_of_risk_name: _Optional[str] = ..., instrument_type: _Optional[str] = ..., trading_status: _Optional[_Union[_common_pb2.SecurityTradingStatus, str]] = ..., otc_flag: bool = ..., buy_available_flag: bool = ..., sell_available_flag: bool = ..., min_price_increment: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., api_trade_available_flag: bool = ..., uid: _Optional[str] = ..., real_exchange: _Optional[_Union[RealExchange, str]] = ..., position_uid: _Optional[str] = ..., for_iis_flag: bool = ..., first_1min_candle_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., first_1day_candle_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ...) -> None: ...

class InstrumentLink(_message.Message):
    __slots__ = ["instrument_uid", "type"]
    INSTRUMENT_UID_FIELD_NUMBER: _ClassVar[int]
    TYPE_FIELD_NUMBER: _ClassVar[int]
    instrument_uid: str
    type: str
    def __init__(self, type: _Optional[str] = ..., instrument_uid: _Optional[str] = ...) -> None: ...

class InstrumentRequest(_message.Message):
    __slots__ = ["class_code", "id", "id_type"]
    CLASS_CODE_FIELD_NUMBER: _ClassVar[int]
    ID_FIELD_NUMBER: _ClassVar[int]
    ID_TYPE_FIELD_NUMBER: _ClassVar[int]
    class_code: str
    id: str
    id_type: InstrumentIdType
    def __init__(self, id_type: _Optional[_Union[InstrumentIdType, str]] = ..., class_code: _Optional[str] = ..., id: _Optional[str] = ...) -> None: ...

class InstrumentResponse(_message.Message):
    __slots__ = ["instrument"]
    INSTRUMENT_FIELD_NUMBER: _ClassVar[int]
    instrument: Instrument
    def __init__(self, instrument: _Optional[_Union[Instrument, _Mapping]] = ...) -> None: ...

class InstrumentShort(_message.Message):
    __slots__ = ["api_trade_available_flag", "class_code", "figi", "first_1day_candle_date", "first_1min_candle_date", "for_iis_flag", "instrument_type", "isin", "name", "position_uid", "ticker", "uid"]
    API_TRADE_AVAILABLE_FLAG_FIELD_NUMBER: _ClassVar[int]
    CLASS_CODE_FIELD_NUMBER: _ClassVar[int]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    FIRST_1DAY_CANDLE_DATE_FIELD_NUMBER: _ClassVar[int]
    FIRST_1MIN_CANDLE_DATE_FIELD_NUMBER: _ClassVar[int]
    FOR_IIS_FLAG_FIELD_NUMBER: _ClassVar[int]
    INSTRUMENT_TYPE_FIELD_NUMBER: _ClassVar[int]
    ISIN_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    POSITION_UID_FIELD_NUMBER: _ClassVar[int]
    TICKER_FIELD_NUMBER: _ClassVar[int]
    UID_FIELD_NUMBER: _ClassVar[int]
    api_trade_available_flag: bool
    class_code: str
    figi: str
    first_1day_candle_date: _timestamp_pb2.Timestamp
    first_1min_candle_date: _timestamp_pb2.Timestamp
    for_iis_flag: bool
    instrument_type: str
    isin: str
    name: str
    position_uid: str
    ticker: str
    uid: str
    def __init__(self, isin: _Optional[str] = ..., figi: _Optional[str] = ..., ticker: _Optional[str] = ..., class_code: _Optional[str] = ..., instrument_type: _Optional[str] = ..., name: _Optional[str] = ..., uid: _Optional[str] = ..., position_uid: _Optional[str] = ..., api_trade_available_flag: bool = ..., for_iis_flag: bool = ..., first_1min_candle_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., first_1day_candle_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ...) -> None: ...

class InstrumentsRequest(_message.Message):
    __slots__ = ["instrument_status"]
    INSTRUMENT_STATUS_FIELD_NUMBER: _ClassVar[int]
    instrument_status: InstrumentStatus
    def __init__(self, instrument_status: _Optional[_Union[InstrumentStatus, str]] = ...) -> None: ...

class Option(_message.Message):
    __slots__ = ["asset_type", "basic_asset", "basic_asset_position_uid", "basic_asset_size", "buy_available_flag", "class_code", "country_of_risk", "country_of_risk_name", "currency", "direction", "dlong", "dlong_min", "dshort", "dshort_min", "exchange", "expiration_date", "first_1day_candle_date", "first_1min_candle_date", "first_trade_date", "for_iis_flag", "klong", "kshort", "last_trade_date", "lot", "min_price_increment", "name", "otc_flag", "payment_type", "position_uid", "real_exchange", "sector", "sell_available_flag", "settlement_currency", "settlement_type", "short_enabled_flag", "strike_price", "style", "ticker", "trading_status", "uid"]
    ASSET_TYPE_FIELD_NUMBER: _ClassVar[int]
    BASIC_ASSET_FIELD_NUMBER: _ClassVar[int]
    BASIC_ASSET_POSITION_UID_FIELD_NUMBER: _ClassVar[int]
    BASIC_ASSET_SIZE_FIELD_NUMBER: _ClassVar[int]
    BUY_AVAILABLE_FLAG_FIELD_NUMBER: _ClassVar[int]
    CLASS_CODE_FIELD_NUMBER: _ClassVar[int]
    COUNTRY_OF_RISK_FIELD_NUMBER: _ClassVar[int]
    COUNTRY_OF_RISK_NAME_FIELD_NUMBER: _ClassVar[int]
    CURRENCY_FIELD_NUMBER: _ClassVar[int]
    DIRECTION_FIELD_NUMBER: _ClassVar[int]
    DLONG_FIELD_NUMBER: _ClassVar[int]
    DLONG_MIN_FIELD_NUMBER: _ClassVar[int]
    DSHORT_FIELD_NUMBER: _ClassVar[int]
    DSHORT_MIN_FIELD_NUMBER: _ClassVar[int]
    EXCHANGE_FIELD_NUMBER: _ClassVar[int]
    EXPIRATION_DATE_FIELD_NUMBER: _ClassVar[int]
    FIRST_1DAY_CANDLE_DATE_FIELD_NUMBER: _ClassVar[int]
    FIRST_1MIN_CANDLE_DATE_FIELD_NUMBER: _ClassVar[int]
    FIRST_TRADE_DATE_FIELD_NUMBER: _ClassVar[int]
    FOR_IIS_FLAG_FIELD_NUMBER: _ClassVar[int]
    KLONG_FIELD_NUMBER: _ClassVar[int]
    KSHORT_FIELD_NUMBER: _ClassVar[int]
    LAST_TRADE_DATE_FIELD_NUMBER: _ClassVar[int]
    LOT_FIELD_NUMBER: _ClassVar[int]
    MIN_PRICE_INCREMENT_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    OTC_FLAG_FIELD_NUMBER: _ClassVar[int]
    PAYMENT_TYPE_FIELD_NUMBER: _ClassVar[int]
    POSITION_UID_FIELD_NUMBER: _ClassVar[int]
    REAL_EXCHANGE_FIELD_NUMBER: _ClassVar[int]
    SECTOR_FIELD_NUMBER: _ClassVar[int]
    SELL_AVAILABLE_FLAG_FIELD_NUMBER: _ClassVar[int]
    SETTLEMENT_CURRENCY_FIELD_NUMBER: _ClassVar[int]
    SETTLEMENT_TYPE_FIELD_NUMBER: _ClassVar[int]
    SHORT_ENABLED_FLAG_FIELD_NUMBER: _ClassVar[int]
    STRIKE_PRICE_FIELD_NUMBER: _ClassVar[int]
    STYLE_FIELD_NUMBER: _ClassVar[int]
    TICKER_FIELD_NUMBER: _ClassVar[int]
    TRADING_STATUS_FIELD_NUMBER: _ClassVar[int]
    UID_FIELD_NUMBER: _ClassVar[int]
    asset_type: str
    basic_asset: str
    basic_asset_position_uid: str
    basic_asset_size: _common_pb2.Quotation
    buy_available_flag: bool
    class_code: str
    country_of_risk: str
    country_of_risk_name: str
    currency: str
    direction: OptionDirection
    dlong: _common_pb2.Quotation
    dlong_min: _common_pb2.Quotation
    dshort: _common_pb2.Quotation
    dshort_min: _common_pb2.Quotation
    exchange: str
    expiration_date: _timestamp_pb2.Timestamp
    first_1day_candle_date: _timestamp_pb2.Timestamp
    first_1min_candle_date: _timestamp_pb2.Timestamp
    first_trade_date: _timestamp_pb2.Timestamp
    for_iis_flag: bool
    klong: _common_pb2.Quotation
    kshort: _common_pb2.Quotation
    last_trade_date: _timestamp_pb2.Timestamp
    lot: int
    min_price_increment: _common_pb2.Quotation
    name: str
    otc_flag: bool
    payment_type: OptionPaymentType
    position_uid: str
    real_exchange: RealExchange
    sector: str
    sell_available_flag: bool
    settlement_currency: str
    settlement_type: OptionSettlementType
    short_enabled_flag: bool
    strike_price: _common_pb2.MoneyValue
    style: OptionStyle
    ticker: str
    trading_status: _common_pb2.SecurityTradingStatus
    uid: str
    def __init__(self, uid: _Optional[str] = ..., position_uid: _Optional[str] = ..., ticker: _Optional[str] = ..., class_code: _Optional[str] = ..., basic_asset_position_uid: _Optional[str] = ..., trading_status: _Optional[_Union[_common_pb2.SecurityTradingStatus, str]] = ..., real_exchange: _Optional[_Union[RealExchange, str]] = ..., direction: _Optional[_Union[OptionDirection, str]] = ..., payment_type: _Optional[_Union[OptionPaymentType, str]] = ..., style: _Optional[_Union[OptionStyle, str]] = ..., settlement_type: _Optional[_Union[OptionSettlementType, str]] = ..., name: _Optional[str] = ..., currency: _Optional[str] = ..., settlement_currency: _Optional[str] = ..., asset_type: _Optional[str] = ..., basic_asset: _Optional[str] = ..., exchange: _Optional[str] = ..., country_of_risk: _Optional[str] = ..., country_of_risk_name: _Optional[str] = ..., sector: _Optional[str] = ..., lot: _Optional[int] = ..., basic_asset_size: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., klong: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., kshort: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., dlong: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., dshort: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., dlong_min: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., dshort_min: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., min_price_increment: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., strike_price: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., expiration_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., first_trade_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., last_trade_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., first_1min_candle_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., first_1day_candle_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., short_enabled_flag: bool = ..., for_iis_flag: bool = ..., otc_flag: bool = ..., buy_available_flag: bool = ..., sell_available_flag: bool = ...) -> None: ...

class OptionResponse(_message.Message):
    __slots__ = ["instrument"]
    INSTRUMENT_FIELD_NUMBER: _ClassVar[int]
    instrument: Option
    def __init__(self, instrument: _Optional[_Union[Option, _Mapping]] = ...) -> None: ...

class OptionsResponse(_message.Message):
    __slots__ = ["instruments"]
    INSTRUMENTS_FIELD_NUMBER: _ClassVar[int]
    instruments: _containers.RepeatedCompositeFieldContainer[Option]
    def __init__(self, instruments: _Optional[_Iterable[_Union[Option, _Mapping]]] = ...) -> None: ...

class Share(_message.Message):
    __slots__ = ["api_trade_available_flag", "buy_available_flag", "class_code", "country_of_risk", "country_of_risk_name", "currency", "div_yield_flag", "dlong", "dlong_min", "dshort", "dshort_min", "exchange", "figi", "first_1day_candle_date", "first_1min_candle_date", "for_iis_flag", "ipo_date", "isin", "issue_size", "issue_size_plan", "klong", "kshort", "lot", "min_price_increment", "name", "nominal", "otc_flag", "position_uid", "real_exchange", "sector", "sell_available_flag", "share_type", "short_enabled_flag", "ticker", "trading_status", "uid"]
    API_TRADE_AVAILABLE_FLAG_FIELD_NUMBER: _ClassVar[int]
    BUY_AVAILABLE_FLAG_FIELD_NUMBER: _ClassVar[int]
    CLASS_CODE_FIELD_NUMBER: _ClassVar[int]
    COUNTRY_OF_RISK_FIELD_NUMBER: _ClassVar[int]
    COUNTRY_OF_RISK_NAME_FIELD_NUMBER: _ClassVar[int]
    CURRENCY_FIELD_NUMBER: _ClassVar[int]
    DIV_YIELD_FLAG_FIELD_NUMBER: _ClassVar[int]
    DLONG_FIELD_NUMBER: _ClassVar[int]
    DLONG_MIN_FIELD_NUMBER: _ClassVar[int]
    DSHORT_FIELD_NUMBER: _ClassVar[int]
    DSHORT_MIN_FIELD_NUMBER: _ClassVar[int]
    EXCHANGE_FIELD_NUMBER: _ClassVar[int]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    FIRST_1DAY_CANDLE_DATE_FIELD_NUMBER: _ClassVar[int]
    FIRST_1MIN_CANDLE_DATE_FIELD_NUMBER: _ClassVar[int]
    FOR_IIS_FLAG_FIELD_NUMBER: _ClassVar[int]
    IPO_DATE_FIELD_NUMBER: _ClassVar[int]
    ISIN_FIELD_NUMBER: _ClassVar[int]
    ISSUE_SIZE_FIELD_NUMBER: _ClassVar[int]
    ISSUE_SIZE_PLAN_FIELD_NUMBER: _ClassVar[int]
    KLONG_FIELD_NUMBER: _ClassVar[int]
    KSHORT_FIELD_NUMBER: _ClassVar[int]
    LOT_FIELD_NUMBER: _ClassVar[int]
    MIN_PRICE_INCREMENT_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    NOMINAL_FIELD_NUMBER: _ClassVar[int]
    OTC_FLAG_FIELD_NUMBER: _ClassVar[int]
    POSITION_UID_FIELD_NUMBER: _ClassVar[int]
    REAL_EXCHANGE_FIELD_NUMBER: _ClassVar[int]
    SECTOR_FIELD_NUMBER: _ClassVar[int]
    SELL_AVAILABLE_FLAG_FIELD_NUMBER: _ClassVar[int]
    SHARE_TYPE_FIELD_NUMBER: _ClassVar[int]
    SHORT_ENABLED_FLAG_FIELD_NUMBER: _ClassVar[int]
    TICKER_FIELD_NUMBER: _ClassVar[int]
    TRADING_STATUS_FIELD_NUMBER: _ClassVar[int]
    UID_FIELD_NUMBER: _ClassVar[int]
    api_trade_available_flag: bool
    buy_available_flag: bool
    class_code: str
    country_of_risk: str
    country_of_risk_name: str
    currency: str
    div_yield_flag: bool
    dlong: _common_pb2.Quotation
    dlong_min: _common_pb2.Quotation
    dshort: _common_pb2.Quotation
    dshort_min: _common_pb2.Quotation
    exchange: str
    figi: str
    first_1day_candle_date: _timestamp_pb2.Timestamp
    first_1min_candle_date: _timestamp_pb2.Timestamp
    for_iis_flag: bool
    ipo_date: _timestamp_pb2.Timestamp
    isin: str
    issue_size: int
    issue_size_plan: int
    klong: _common_pb2.Quotation
    kshort: _common_pb2.Quotation
    lot: int
    min_price_increment: _common_pb2.Quotation
    name: str
    nominal: _common_pb2.MoneyValue
    otc_flag: bool
    position_uid: str
    real_exchange: RealExchange
    sector: str
    sell_available_flag: bool
    share_type: ShareType
    short_enabled_flag: bool
    ticker: str
    trading_status: _common_pb2.SecurityTradingStatus
    uid: str
    def __init__(self, figi: _Optional[str] = ..., ticker: _Optional[str] = ..., class_code: _Optional[str] = ..., isin: _Optional[str] = ..., lot: _Optional[int] = ..., currency: _Optional[str] = ..., klong: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., kshort: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., dlong: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., dshort: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., dlong_min: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., dshort_min: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., short_enabled_flag: bool = ..., name: _Optional[str] = ..., exchange: _Optional[str] = ..., ipo_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., issue_size: _Optional[int] = ..., country_of_risk: _Optional[str] = ..., country_of_risk_name: _Optional[str] = ..., sector: _Optional[str] = ..., issue_size_plan: _Optional[int] = ..., nominal: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., trading_status: _Optional[_Union[_common_pb2.SecurityTradingStatus, str]] = ..., otc_flag: bool = ..., buy_available_flag: bool = ..., sell_available_flag: bool = ..., div_yield_flag: bool = ..., share_type: _Optional[_Union[ShareType, str]] = ..., min_price_increment: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., api_trade_available_flag: bool = ..., uid: _Optional[str] = ..., real_exchange: _Optional[_Union[RealExchange, str]] = ..., position_uid: _Optional[str] = ..., for_iis_flag: bool = ..., first_1min_candle_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., first_1day_candle_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ...) -> None: ...

class ShareResponse(_message.Message):
    __slots__ = ["instrument"]
    INSTRUMENT_FIELD_NUMBER: _ClassVar[int]
    instrument: Share
    def __init__(self, instrument: _Optional[_Union[Share, _Mapping]] = ...) -> None: ...

class SharesResponse(_message.Message):
    __slots__ = ["instruments"]
    INSTRUMENTS_FIELD_NUMBER: _ClassVar[int]
    instruments: _containers.RepeatedCompositeFieldContainer[Share]
    def __init__(self, instruments: _Optional[_Iterable[_Union[Share, _Mapping]]] = ...) -> None: ...

class TradingDay(_message.Message):
    __slots__ = ["clearing_end_time", "clearing_start_time", "closing_auction_end_time", "date", "end_time", "evening_end_time", "evening_opening_auction_start_time", "evening_start_time", "is_trading_day", "opening_auction_start_time", "premarket_end_time", "premarket_start_time", "start_time"]
    CLEARING_END_TIME_FIELD_NUMBER: _ClassVar[int]
    CLEARING_START_TIME_FIELD_NUMBER: _ClassVar[int]
    CLOSING_AUCTION_END_TIME_FIELD_NUMBER: _ClassVar[int]
    DATE_FIELD_NUMBER: _ClassVar[int]
    END_TIME_FIELD_NUMBER: _ClassVar[int]
    EVENING_END_TIME_FIELD_NUMBER: _ClassVar[int]
    EVENING_OPENING_AUCTION_START_TIME_FIELD_NUMBER: _ClassVar[int]
    EVENING_START_TIME_FIELD_NUMBER: _ClassVar[int]
    IS_TRADING_DAY_FIELD_NUMBER: _ClassVar[int]
    OPENING_AUCTION_START_TIME_FIELD_NUMBER: _ClassVar[int]
    PREMARKET_END_TIME_FIELD_NUMBER: _ClassVar[int]
    PREMARKET_START_TIME_FIELD_NUMBER: _ClassVar[int]
    START_TIME_FIELD_NUMBER: _ClassVar[int]
    clearing_end_time: _timestamp_pb2.Timestamp
    clearing_start_time: _timestamp_pb2.Timestamp
    closing_auction_end_time: _timestamp_pb2.Timestamp
    date: _timestamp_pb2.Timestamp
    end_time: _timestamp_pb2.Timestamp
    evening_end_time: _timestamp_pb2.Timestamp
    evening_opening_auction_start_time: _timestamp_pb2.Timestamp
    evening_start_time: _timestamp_pb2.Timestamp
    is_trading_day: bool
    opening_auction_start_time: _timestamp_pb2.Timestamp
    premarket_end_time: _timestamp_pb2.Timestamp
    premarket_start_time: _timestamp_pb2.Timestamp
    start_time: _timestamp_pb2.Timestamp
    def __init__(self, date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., is_trading_day: bool = ..., start_time: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., end_time: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., opening_auction_start_time: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., closing_auction_end_time: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., evening_opening_auction_start_time: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., evening_start_time: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., evening_end_time: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., clearing_start_time: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., clearing_end_time: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., premarket_start_time: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., premarket_end_time: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ...) -> None: ...

class TradingSchedule(_message.Message):
    __slots__ = ["days", "exchange"]
    DAYS_FIELD_NUMBER: _ClassVar[int]
    EXCHANGE_FIELD_NUMBER: _ClassVar[int]
    days: _containers.RepeatedCompositeFieldContainer[TradingDay]
    exchange: str
    def __init__(self, exchange: _Optional[str] = ..., days: _Optional[_Iterable[_Union[TradingDay, _Mapping]]] = ...) -> None: ...

class TradingSchedulesRequest(_message.Message):
    __slots__ = ["exchange", "to"]
    EXCHANGE_FIELD_NUMBER: _ClassVar[int]
    FROM_FIELD_NUMBER: _ClassVar[int]
    TO_FIELD_NUMBER: _ClassVar[int]
    exchange: str
    to: _timestamp_pb2.Timestamp
    def __init__(self, exchange: _Optional[str] = ..., to: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., **kwargs) -> None: ...

class TradingSchedulesResponse(_message.Message):
    __slots__ = ["exchanges"]
    EXCHANGES_FIELD_NUMBER: _ClassVar[int]
    exchanges: _containers.RepeatedCompositeFieldContainer[TradingSchedule]
    def __init__(self, exchanges: _Optional[_Iterable[_Union[TradingSchedule, _Mapping]]] = ...) -> None: ...

class CouponType(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []

class OptionDirection(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []

class OptionPaymentType(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []

class OptionStyle(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []

class OptionSettlementType(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []

class InstrumentIdType(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []

class InstrumentStatus(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []

class ShareType(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []

class AssetType(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []

class StructuredProductType(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []

class EditFavoritesActionType(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []

class RealExchange(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
