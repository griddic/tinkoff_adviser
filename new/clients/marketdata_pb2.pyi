from google.protobuf import timestamp_pb2 as _timestamp_pb2
import common_pb2 as _common_pb2
from google.protobuf.internal import containers as _containers
from google.protobuf.internal import enum_type_wrapper as _enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Iterable as _Iterable, Mapping as _Mapping, Optional as _Optional, Union as _Union

CANDLE_INTERVAL_15_MIN: CandleInterval
CANDLE_INTERVAL_1_MIN: CandleInterval
CANDLE_INTERVAL_5_MIN: CandleInterval
CANDLE_INTERVAL_DAY: CandleInterval
CANDLE_INTERVAL_HOUR: CandleInterval
CANDLE_INTERVAL_UNSPECIFIED: CandleInterval
DESCRIPTOR: _descriptor.FileDescriptor
SUBSCRIPTION_ACTION_SUBSCRIBE: SubscriptionAction
SUBSCRIPTION_ACTION_UNSPECIFIED: SubscriptionAction
SUBSCRIPTION_ACTION_UNSUBSCRIBE: SubscriptionAction
SUBSCRIPTION_INTERVAL_FIVE_MINUTES: SubscriptionInterval
SUBSCRIPTION_INTERVAL_ONE_MINUTE: SubscriptionInterval
SUBSCRIPTION_INTERVAL_UNSPECIFIED: SubscriptionInterval
SUBSCRIPTION_STATUS_DEPTH_IS_INVALID: SubscriptionStatus
SUBSCRIPTION_STATUS_INSTRUMENT_NOT_FOUND: SubscriptionStatus
SUBSCRIPTION_STATUS_INTERNAL_ERROR: SubscriptionStatus
SUBSCRIPTION_STATUS_INTERVAL_IS_INVALID: SubscriptionStatus
SUBSCRIPTION_STATUS_LIMIT_IS_EXCEEDED: SubscriptionStatus
SUBSCRIPTION_STATUS_SUBSCRIPTION_ACTION_IS_INVALID: SubscriptionStatus
SUBSCRIPTION_STATUS_SUCCESS: SubscriptionStatus
SUBSCRIPTION_STATUS_TOO_MANY_REQUESTS: SubscriptionStatus
SUBSCRIPTION_STATUS_UNSPECIFIED: SubscriptionStatus
TRADE_DIRECTION_BUY: TradeDirection
TRADE_DIRECTION_SELL: TradeDirection
TRADE_DIRECTION_UNSPECIFIED: TradeDirection

class Candle(_message.Message):
    __slots__ = ["close", "figi", "high", "interval", "last_trade_ts", "low", "open", "time", "volume"]
    CLOSE_FIELD_NUMBER: _ClassVar[int]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    HIGH_FIELD_NUMBER: _ClassVar[int]
    INTERVAL_FIELD_NUMBER: _ClassVar[int]
    LAST_TRADE_TS_FIELD_NUMBER: _ClassVar[int]
    LOW_FIELD_NUMBER: _ClassVar[int]
    OPEN_FIELD_NUMBER: _ClassVar[int]
    TIME_FIELD_NUMBER: _ClassVar[int]
    VOLUME_FIELD_NUMBER: _ClassVar[int]
    close: _common_pb2.Quotation
    figi: str
    high: _common_pb2.Quotation
    interval: SubscriptionInterval
    last_trade_ts: _timestamp_pb2.Timestamp
    low: _common_pb2.Quotation
    open: _common_pb2.Quotation
    time: _timestamp_pb2.Timestamp
    volume: int
    def __init__(self, figi: _Optional[str] = ..., interval: _Optional[_Union[SubscriptionInterval, str]] = ..., open: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., high: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., low: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., close: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., volume: _Optional[int] = ..., time: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., last_trade_ts: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ...) -> None: ...

class CandleInstrument(_message.Message):
    __slots__ = ["figi", "interval"]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    INTERVAL_FIELD_NUMBER: _ClassVar[int]
    figi: str
    interval: SubscriptionInterval
    def __init__(self, figi: _Optional[str] = ..., interval: _Optional[_Union[SubscriptionInterval, str]] = ...) -> None: ...

class CandleSubscription(_message.Message):
    __slots__ = ["figi", "interval", "subscription_status"]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    INTERVAL_FIELD_NUMBER: _ClassVar[int]
    SUBSCRIPTION_STATUS_FIELD_NUMBER: _ClassVar[int]
    figi: str
    interval: SubscriptionInterval
    subscription_status: SubscriptionStatus
    def __init__(self, figi: _Optional[str] = ..., interval: _Optional[_Union[SubscriptionInterval, str]] = ..., subscription_status: _Optional[_Union[SubscriptionStatus, str]] = ...) -> None: ...

class GetCandlesRequest(_message.Message):
    __slots__ = ["figi", "interval", "to"]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    FROM_FIELD_NUMBER: _ClassVar[int]
    INTERVAL_FIELD_NUMBER: _ClassVar[int]
    TO_FIELD_NUMBER: _ClassVar[int]
    figi: str
    interval: CandleInterval
    to: _timestamp_pb2.Timestamp
    def __init__(self, figi: _Optional[str] = ..., to: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., interval: _Optional[_Union[CandleInterval, str]] = ..., **kwargs) -> None: ...

class GetCandlesResponse(_message.Message):
    __slots__ = ["candles"]
    CANDLES_FIELD_NUMBER: _ClassVar[int]
    candles: _containers.RepeatedCompositeFieldContainer[HistoricCandle]
    def __init__(self, candles: _Optional[_Iterable[_Union[HistoricCandle, _Mapping]]] = ...) -> None: ...

class GetClosePricesRequest(_message.Message):
    __slots__ = ["instruments"]
    INSTRUMENTS_FIELD_NUMBER: _ClassVar[int]
    instruments: _containers.RepeatedCompositeFieldContainer[InstrumentClosePriceRequest]
    def __init__(self, instruments: _Optional[_Iterable[_Union[InstrumentClosePriceRequest, _Mapping]]] = ...) -> None: ...

class GetClosePricesResponse(_message.Message):
    __slots__ = ["close_prices"]
    CLOSE_PRICES_FIELD_NUMBER: _ClassVar[int]
    close_prices: _containers.RepeatedCompositeFieldContainer[InstrumentClosePriceResponse]
    def __init__(self, close_prices: _Optional[_Iterable[_Union[InstrumentClosePriceResponse, _Mapping]]] = ...) -> None: ...

class GetLastPricesRequest(_message.Message):
    __slots__ = ["figi"]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    figi: _containers.RepeatedScalarFieldContainer[str]
    def __init__(self, figi: _Optional[_Iterable[str]] = ...) -> None: ...

class GetLastPricesResponse(_message.Message):
    __slots__ = ["last_prices"]
    LAST_PRICES_FIELD_NUMBER: _ClassVar[int]
    last_prices: _containers.RepeatedCompositeFieldContainer[LastPrice]
    def __init__(self, last_prices: _Optional[_Iterable[_Union[LastPrice, _Mapping]]] = ...) -> None: ...

class GetLastTradesRequest(_message.Message):
    __slots__ = ["figi", "to"]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    FROM_FIELD_NUMBER: _ClassVar[int]
    TO_FIELD_NUMBER: _ClassVar[int]
    figi: str
    to: _timestamp_pb2.Timestamp
    def __init__(self, figi: _Optional[str] = ..., to: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., **kwargs) -> None: ...

class GetLastTradesResponse(_message.Message):
    __slots__ = ["trades"]
    TRADES_FIELD_NUMBER: _ClassVar[int]
    trades: _containers.RepeatedCompositeFieldContainer[Trade]
    def __init__(self, trades: _Optional[_Iterable[_Union[Trade, _Mapping]]] = ...) -> None: ...

class GetMySubscriptions(_message.Message):
    __slots__ = []
    def __init__(self) -> None: ...

class GetOrderBookRequest(_message.Message):
    __slots__ = ["depth", "figi"]
    DEPTH_FIELD_NUMBER: _ClassVar[int]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    depth: int
    figi: str
    def __init__(self, figi: _Optional[str] = ..., depth: _Optional[int] = ...) -> None: ...

class GetOrderBookResponse(_message.Message):
    __slots__ = ["asks", "bids", "close_price", "close_price_ts", "depth", "figi", "last_price", "last_price_ts", "limit_down", "limit_up", "orderbook_ts"]
    ASKS_FIELD_NUMBER: _ClassVar[int]
    BIDS_FIELD_NUMBER: _ClassVar[int]
    CLOSE_PRICE_FIELD_NUMBER: _ClassVar[int]
    CLOSE_PRICE_TS_FIELD_NUMBER: _ClassVar[int]
    DEPTH_FIELD_NUMBER: _ClassVar[int]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    LAST_PRICE_FIELD_NUMBER: _ClassVar[int]
    LAST_PRICE_TS_FIELD_NUMBER: _ClassVar[int]
    LIMIT_DOWN_FIELD_NUMBER: _ClassVar[int]
    LIMIT_UP_FIELD_NUMBER: _ClassVar[int]
    ORDERBOOK_TS_FIELD_NUMBER: _ClassVar[int]
    asks: _containers.RepeatedCompositeFieldContainer[Order]
    bids: _containers.RepeatedCompositeFieldContainer[Order]
    close_price: _common_pb2.Quotation
    close_price_ts: _timestamp_pb2.Timestamp
    depth: int
    figi: str
    last_price: _common_pb2.Quotation
    last_price_ts: _timestamp_pb2.Timestamp
    limit_down: _common_pb2.Quotation
    limit_up: _common_pb2.Quotation
    orderbook_ts: _timestamp_pb2.Timestamp
    def __init__(self, figi: _Optional[str] = ..., depth: _Optional[int] = ..., bids: _Optional[_Iterable[_Union[Order, _Mapping]]] = ..., asks: _Optional[_Iterable[_Union[Order, _Mapping]]] = ..., last_price: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., close_price: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., limit_up: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., limit_down: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., last_price_ts: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., close_price_ts: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., orderbook_ts: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ...) -> None: ...

class GetTradingStatusRequest(_message.Message):
    __slots__ = ["figi"]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    figi: str
    def __init__(self, figi: _Optional[str] = ...) -> None: ...

class GetTradingStatusResponse(_message.Message):
    __slots__ = ["api_trade_available_flag", "figi", "limit_order_available_flag", "market_order_available_flag", "trading_status"]
    API_TRADE_AVAILABLE_FLAG_FIELD_NUMBER: _ClassVar[int]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    LIMIT_ORDER_AVAILABLE_FLAG_FIELD_NUMBER: _ClassVar[int]
    MARKET_ORDER_AVAILABLE_FLAG_FIELD_NUMBER: _ClassVar[int]
    TRADING_STATUS_FIELD_NUMBER: _ClassVar[int]
    api_trade_available_flag: bool
    figi: str
    limit_order_available_flag: bool
    market_order_available_flag: bool
    trading_status: _common_pb2.SecurityTradingStatus
    def __init__(self, figi: _Optional[str] = ..., trading_status: _Optional[_Union[_common_pb2.SecurityTradingStatus, str]] = ..., limit_order_available_flag: bool = ..., market_order_available_flag: bool = ..., api_trade_available_flag: bool = ...) -> None: ...

class HistoricCandle(_message.Message):
    __slots__ = ["close", "high", "is_complete", "low", "open", "time", "volume"]
    CLOSE_FIELD_NUMBER: _ClassVar[int]
    HIGH_FIELD_NUMBER: _ClassVar[int]
    IS_COMPLETE_FIELD_NUMBER: _ClassVar[int]
    LOW_FIELD_NUMBER: _ClassVar[int]
    OPEN_FIELD_NUMBER: _ClassVar[int]
    TIME_FIELD_NUMBER: _ClassVar[int]
    VOLUME_FIELD_NUMBER: _ClassVar[int]
    close: _common_pb2.Quotation
    high: _common_pb2.Quotation
    is_complete: bool
    low: _common_pb2.Quotation
    open: _common_pb2.Quotation
    time: _timestamp_pb2.Timestamp
    volume: int
    def __init__(self, open: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., high: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., low: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., close: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., volume: _Optional[int] = ..., time: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., is_complete: bool = ...) -> None: ...

class InfoInstrument(_message.Message):
    __slots__ = ["figi"]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    figi: str
    def __init__(self, figi: _Optional[str] = ...) -> None: ...

class InfoSubscription(_message.Message):
    __slots__ = ["figi", "subscription_status"]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    SUBSCRIPTION_STATUS_FIELD_NUMBER: _ClassVar[int]
    figi: str
    subscription_status: SubscriptionStatus
    def __init__(self, figi: _Optional[str] = ..., subscription_status: _Optional[_Union[SubscriptionStatus, str]] = ...) -> None: ...

class InstrumentClosePriceRequest(_message.Message):
    __slots__ = ["instrument_id"]
    INSTRUMENT_ID_FIELD_NUMBER: _ClassVar[int]
    instrument_id: str
    def __init__(self, instrument_id: _Optional[str] = ...) -> None: ...

class InstrumentClosePriceResponse(_message.Message):
    __slots__ = ["figi", "instrument_uid", "price", "time"]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    INSTRUMENT_UID_FIELD_NUMBER: _ClassVar[int]
    PRICE_FIELD_NUMBER: _ClassVar[int]
    TIME_FIELD_NUMBER: _ClassVar[int]
    figi: str
    instrument_uid: str
    price: _common_pb2.Quotation
    time: _timestamp_pb2.Timestamp
    def __init__(self, figi: _Optional[str] = ..., instrument_uid: _Optional[str] = ..., price: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., time: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ...) -> None: ...

class LastPrice(_message.Message):
    __slots__ = ["figi", "instrument_uid", "price", "time"]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    INSTRUMENT_UID_FIELD_NUMBER: _ClassVar[int]
    PRICE_FIELD_NUMBER: _ClassVar[int]
    TIME_FIELD_NUMBER: _ClassVar[int]
    figi: str
    instrument_uid: str
    price: _common_pb2.Quotation
    time: _timestamp_pb2.Timestamp
    def __init__(self, figi: _Optional[str] = ..., price: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., time: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., instrument_uid: _Optional[str] = ...) -> None: ...

class LastPriceInstrument(_message.Message):
    __slots__ = ["figi"]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    figi: str
    def __init__(self, figi: _Optional[str] = ...) -> None: ...

class LastPriceSubscription(_message.Message):
    __slots__ = ["figi", "subscription_status"]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    SUBSCRIPTION_STATUS_FIELD_NUMBER: _ClassVar[int]
    figi: str
    subscription_status: SubscriptionStatus
    def __init__(self, figi: _Optional[str] = ..., subscription_status: _Optional[_Union[SubscriptionStatus, str]] = ...) -> None: ...

class MarketDataRequest(_message.Message):
    __slots__ = ["get_my_subscriptions", "subscribe_candles_request", "subscribe_info_request", "subscribe_last_price_request", "subscribe_order_book_request", "subscribe_trades_request"]
    GET_MY_SUBSCRIPTIONS_FIELD_NUMBER: _ClassVar[int]
    SUBSCRIBE_CANDLES_REQUEST_FIELD_NUMBER: _ClassVar[int]
    SUBSCRIBE_INFO_REQUEST_FIELD_NUMBER: _ClassVar[int]
    SUBSCRIBE_LAST_PRICE_REQUEST_FIELD_NUMBER: _ClassVar[int]
    SUBSCRIBE_ORDER_BOOK_REQUEST_FIELD_NUMBER: _ClassVar[int]
    SUBSCRIBE_TRADES_REQUEST_FIELD_NUMBER: _ClassVar[int]
    get_my_subscriptions: GetMySubscriptions
    subscribe_candles_request: SubscribeCandlesRequest
    subscribe_info_request: SubscribeInfoRequest
    subscribe_last_price_request: SubscribeLastPriceRequest
    subscribe_order_book_request: SubscribeOrderBookRequest
    subscribe_trades_request: SubscribeTradesRequest
    def __init__(self, subscribe_candles_request: _Optional[_Union[SubscribeCandlesRequest, _Mapping]] = ..., subscribe_order_book_request: _Optional[_Union[SubscribeOrderBookRequest, _Mapping]] = ..., subscribe_trades_request: _Optional[_Union[SubscribeTradesRequest, _Mapping]] = ..., subscribe_info_request: _Optional[_Union[SubscribeInfoRequest, _Mapping]] = ..., subscribe_last_price_request: _Optional[_Union[SubscribeLastPriceRequest, _Mapping]] = ..., get_my_subscriptions: _Optional[_Union[GetMySubscriptions, _Mapping]] = ...) -> None: ...

class MarketDataResponse(_message.Message):
    __slots__ = ["candle", "last_price", "orderbook", "ping", "subscribe_candles_response", "subscribe_info_response", "subscribe_last_price_response", "subscribe_order_book_response", "subscribe_trades_response", "trade", "trading_status"]
    CANDLE_FIELD_NUMBER: _ClassVar[int]
    LAST_PRICE_FIELD_NUMBER: _ClassVar[int]
    ORDERBOOK_FIELD_NUMBER: _ClassVar[int]
    PING_FIELD_NUMBER: _ClassVar[int]
    SUBSCRIBE_CANDLES_RESPONSE_FIELD_NUMBER: _ClassVar[int]
    SUBSCRIBE_INFO_RESPONSE_FIELD_NUMBER: _ClassVar[int]
    SUBSCRIBE_LAST_PRICE_RESPONSE_FIELD_NUMBER: _ClassVar[int]
    SUBSCRIBE_ORDER_BOOK_RESPONSE_FIELD_NUMBER: _ClassVar[int]
    SUBSCRIBE_TRADES_RESPONSE_FIELD_NUMBER: _ClassVar[int]
    TRADE_FIELD_NUMBER: _ClassVar[int]
    TRADING_STATUS_FIELD_NUMBER: _ClassVar[int]
    candle: Candle
    last_price: LastPrice
    orderbook: OrderBook
    ping: _common_pb2.Ping
    subscribe_candles_response: SubscribeCandlesResponse
    subscribe_info_response: SubscribeInfoResponse
    subscribe_last_price_response: SubscribeLastPriceResponse
    subscribe_order_book_response: SubscribeOrderBookResponse
    subscribe_trades_response: SubscribeTradesResponse
    trade: Trade
    trading_status: TradingStatus
    def __init__(self, subscribe_candles_response: _Optional[_Union[SubscribeCandlesResponse, _Mapping]] = ..., subscribe_order_book_response: _Optional[_Union[SubscribeOrderBookResponse, _Mapping]] = ..., subscribe_trades_response: _Optional[_Union[SubscribeTradesResponse, _Mapping]] = ..., subscribe_info_response: _Optional[_Union[SubscribeInfoResponse, _Mapping]] = ..., candle: _Optional[_Union[Candle, _Mapping]] = ..., trade: _Optional[_Union[Trade, _Mapping]] = ..., orderbook: _Optional[_Union[OrderBook, _Mapping]] = ..., trading_status: _Optional[_Union[TradingStatus, _Mapping]] = ..., ping: _Optional[_Union[_common_pb2.Ping, _Mapping]] = ..., subscribe_last_price_response: _Optional[_Union[SubscribeLastPriceResponse, _Mapping]] = ..., last_price: _Optional[_Union[LastPrice, _Mapping]] = ...) -> None: ...

class MarketDataServerSideStreamRequest(_message.Message):
    __slots__ = ["subscribe_candles_request", "subscribe_info_request", "subscribe_last_price_request", "subscribe_order_book_request", "subscribe_trades_request"]
    SUBSCRIBE_CANDLES_REQUEST_FIELD_NUMBER: _ClassVar[int]
    SUBSCRIBE_INFO_REQUEST_FIELD_NUMBER: _ClassVar[int]
    SUBSCRIBE_LAST_PRICE_REQUEST_FIELD_NUMBER: _ClassVar[int]
    SUBSCRIBE_ORDER_BOOK_REQUEST_FIELD_NUMBER: _ClassVar[int]
    SUBSCRIBE_TRADES_REQUEST_FIELD_NUMBER: _ClassVar[int]
    subscribe_candles_request: SubscribeCandlesRequest
    subscribe_info_request: SubscribeInfoRequest
    subscribe_last_price_request: SubscribeLastPriceRequest
    subscribe_order_book_request: SubscribeOrderBookRequest
    subscribe_trades_request: SubscribeTradesRequest
    def __init__(self, subscribe_candles_request: _Optional[_Union[SubscribeCandlesRequest, _Mapping]] = ..., subscribe_order_book_request: _Optional[_Union[SubscribeOrderBookRequest, _Mapping]] = ..., subscribe_trades_request: _Optional[_Union[SubscribeTradesRequest, _Mapping]] = ..., subscribe_info_request: _Optional[_Union[SubscribeInfoRequest, _Mapping]] = ..., subscribe_last_price_request: _Optional[_Union[SubscribeLastPriceRequest, _Mapping]] = ...) -> None: ...

class Order(_message.Message):
    __slots__ = ["price", "quantity"]
    PRICE_FIELD_NUMBER: _ClassVar[int]
    QUANTITY_FIELD_NUMBER: _ClassVar[int]
    price: _common_pb2.Quotation
    quantity: int
    def __init__(self, price: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., quantity: _Optional[int] = ...) -> None: ...

class OrderBook(_message.Message):
    __slots__ = ["asks", "bids", "depth", "figi", "is_consistent", "limit_down", "limit_up", "time"]
    ASKS_FIELD_NUMBER: _ClassVar[int]
    BIDS_FIELD_NUMBER: _ClassVar[int]
    DEPTH_FIELD_NUMBER: _ClassVar[int]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    IS_CONSISTENT_FIELD_NUMBER: _ClassVar[int]
    LIMIT_DOWN_FIELD_NUMBER: _ClassVar[int]
    LIMIT_UP_FIELD_NUMBER: _ClassVar[int]
    TIME_FIELD_NUMBER: _ClassVar[int]
    asks: _containers.RepeatedCompositeFieldContainer[Order]
    bids: _containers.RepeatedCompositeFieldContainer[Order]
    depth: int
    figi: str
    is_consistent: bool
    limit_down: _common_pb2.Quotation
    limit_up: _common_pb2.Quotation
    time: _timestamp_pb2.Timestamp
    def __init__(self, figi: _Optional[str] = ..., depth: _Optional[int] = ..., is_consistent: bool = ..., bids: _Optional[_Iterable[_Union[Order, _Mapping]]] = ..., asks: _Optional[_Iterable[_Union[Order, _Mapping]]] = ..., time: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., limit_up: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., limit_down: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ...) -> None: ...

class OrderBookInstrument(_message.Message):
    __slots__ = ["depth", "figi"]
    DEPTH_FIELD_NUMBER: _ClassVar[int]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    depth: int
    figi: str
    def __init__(self, figi: _Optional[str] = ..., depth: _Optional[int] = ...) -> None: ...

class OrderBookSubscription(_message.Message):
    __slots__ = ["depth", "figi", "subscription_status"]
    DEPTH_FIELD_NUMBER: _ClassVar[int]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    SUBSCRIPTION_STATUS_FIELD_NUMBER: _ClassVar[int]
    depth: int
    figi: str
    subscription_status: SubscriptionStatus
    def __init__(self, figi: _Optional[str] = ..., depth: _Optional[int] = ..., subscription_status: _Optional[_Union[SubscriptionStatus, str]] = ...) -> None: ...

class SubscribeCandlesRequest(_message.Message):
    __slots__ = ["instruments", "subscription_action", "waiting_close"]
    INSTRUMENTS_FIELD_NUMBER: _ClassVar[int]
    SUBSCRIPTION_ACTION_FIELD_NUMBER: _ClassVar[int]
    WAITING_CLOSE_FIELD_NUMBER: _ClassVar[int]
    instruments: _containers.RepeatedCompositeFieldContainer[CandleInstrument]
    subscription_action: SubscriptionAction
    waiting_close: bool
    def __init__(self, subscription_action: _Optional[_Union[SubscriptionAction, str]] = ..., instruments: _Optional[_Iterable[_Union[CandleInstrument, _Mapping]]] = ..., waiting_close: bool = ...) -> None: ...

class SubscribeCandlesResponse(_message.Message):
    __slots__ = ["candles_subscriptions", "tracking_id"]
    CANDLES_SUBSCRIPTIONS_FIELD_NUMBER: _ClassVar[int]
    TRACKING_ID_FIELD_NUMBER: _ClassVar[int]
    candles_subscriptions: _containers.RepeatedCompositeFieldContainer[CandleSubscription]
    tracking_id: str
    def __init__(self, tracking_id: _Optional[str] = ..., candles_subscriptions: _Optional[_Iterable[_Union[CandleSubscription, _Mapping]]] = ...) -> None: ...

class SubscribeInfoRequest(_message.Message):
    __slots__ = ["instruments", "subscription_action"]
    INSTRUMENTS_FIELD_NUMBER: _ClassVar[int]
    SUBSCRIPTION_ACTION_FIELD_NUMBER: _ClassVar[int]
    instruments: _containers.RepeatedCompositeFieldContainer[InfoInstrument]
    subscription_action: SubscriptionAction
    def __init__(self, subscription_action: _Optional[_Union[SubscriptionAction, str]] = ..., instruments: _Optional[_Iterable[_Union[InfoInstrument, _Mapping]]] = ...) -> None: ...

class SubscribeInfoResponse(_message.Message):
    __slots__ = ["info_subscriptions", "tracking_id"]
    INFO_SUBSCRIPTIONS_FIELD_NUMBER: _ClassVar[int]
    TRACKING_ID_FIELD_NUMBER: _ClassVar[int]
    info_subscriptions: _containers.RepeatedCompositeFieldContainer[InfoSubscription]
    tracking_id: str
    def __init__(self, tracking_id: _Optional[str] = ..., info_subscriptions: _Optional[_Iterable[_Union[InfoSubscription, _Mapping]]] = ...) -> None: ...

class SubscribeLastPriceRequest(_message.Message):
    __slots__ = ["instruments", "subscription_action"]
    INSTRUMENTS_FIELD_NUMBER: _ClassVar[int]
    SUBSCRIPTION_ACTION_FIELD_NUMBER: _ClassVar[int]
    instruments: _containers.RepeatedCompositeFieldContainer[LastPriceInstrument]
    subscription_action: SubscriptionAction
    def __init__(self, subscription_action: _Optional[_Union[SubscriptionAction, str]] = ..., instruments: _Optional[_Iterable[_Union[LastPriceInstrument, _Mapping]]] = ...) -> None: ...

class SubscribeLastPriceResponse(_message.Message):
    __slots__ = ["last_price_subscriptions", "tracking_id"]
    LAST_PRICE_SUBSCRIPTIONS_FIELD_NUMBER: _ClassVar[int]
    TRACKING_ID_FIELD_NUMBER: _ClassVar[int]
    last_price_subscriptions: _containers.RepeatedCompositeFieldContainer[LastPriceSubscription]
    tracking_id: str
    def __init__(self, tracking_id: _Optional[str] = ..., last_price_subscriptions: _Optional[_Iterable[_Union[LastPriceSubscription, _Mapping]]] = ...) -> None: ...

class SubscribeOrderBookRequest(_message.Message):
    __slots__ = ["instruments", "subscription_action"]
    INSTRUMENTS_FIELD_NUMBER: _ClassVar[int]
    SUBSCRIPTION_ACTION_FIELD_NUMBER: _ClassVar[int]
    instruments: _containers.RepeatedCompositeFieldContainer[OrderBookInstrument]
    subscription_action: SubscriptionAction
    def __init__(self, subscription_action: _Optional[_Union[SubscriptionAction, str]] = ..., instruments: _Optional[_Iterable[_Union[OrderBookInstrument, _Mapping]]] = ...) -> None: ...

class SubscribeOrderBookResponse(_message.Message):
    __slots__ = ["order_book_subscriptions", "tracking_id"]
    ORDER_BOOK_SUBSCRIPTIONS_FIELD_NUMBER: _ClassVar[int]
    TRACKING_ID_FIELD_NUMBER: _ClassVar[int]
    order_book_subscriptions: _containers.RepeatedCompositeFieldContainer[OrderBookSubscription]
    tracking_id: str
    def __init__(self, tracking_id: _Optional[str] = ..., order_book_subscriptions: _Optional[_Iterable[_Union[OrderBookSubscription, _Mapping]]] = ...) -> None: ...

class SubscribeTradesRequest(_message.Message):
    __slots__ = ["instruments", "subscription_action"]
    INSTRUMENTS_FIELD_NUMBER: _ClassVar[int]
    SUBSCRIPTION_ACTION_FIELD_NUMBER: _ClassVar[int]
    instruments: _containers.RepeatedCompositeFieldContainer[TradeInstrument]
    subscription_action: SubscriptionAction
    def __init__(self, subscription_action: _Optional[_Union[SubscriptionAction, str]] = ..., instruments: _Optional[_Iterable[_Union[TradeInstrument, _Mapping]]] = ...) -> None: ...

class SubscribeTradesResponse(_message.Message):
    __slots__ = ["tracking_id", "trade_subscriptions"]
    TRACKING_ID_FIELD_NUMBER: _ClassVar[int]
    TRADE_SUBSCRIPTIONS_FIELD_NUMBER: _ClassVar[int]
    tracking_id: str
    trade_subscriptions: _containers.RepeatedCompositeFieldContainer[TradeSubscription]
    def __init__(self, tracking_id: _Optional[str] = ..., trade_subscriptions: _Optional[_Iterable[_Union[TradeSubscription, _Mapping]]] = ...) -> None: ...

class Trade(_message.Message):
    __slots__ = ["direction", "figi", "price", "quantity", "time"]
    DIRECTION_FIELD_NUMBER: _ClassVar[int]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    PRICE_FIELD_NUMBER: _ClassVar[int]
    QUANTITY_FIELD_NUMBER: _ClassVar[int]
    TIME_FIELD_NUMBER: _ClassVar[int]
    direction: TradeDirection
    figi: str
    price: _common_pb2.Quotation
    quantity: int
    time: _timestamp_pb2.Timestamp
    def __init__(self, figi: _Optional[str] = ..., direction: _Optional[_Union[TradeDirection, str]] = ..., price: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., quantity: _Optional[int] = ..., time: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ...) -> None: ...

class TradeInstrument(_message.Message):
    __slots__ = ["figi"]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    figi: str
    def __init__(self, figi: _Optional[str] = ...) -> None: ...

class TradeSubscription(_message.Message):
    __slots__ = ["figi", "subscription_status"]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    SUBSCRIPTION_STATUS_FIELD_NUMBER: _ClassVar[int]
    figi: str
    subscription_status: SubscriptionStatus
    def __init__(self, figi: _Optional[str] = ..., subscription_status: _Optional[_Union[SubscriptionStatus, str]] = ...) -> None: ...

class TradingStatus(_message.Message):
    __slots__ = ["figi", "limit_order_available_flag", "market_order_available_flag", "time", "trading_status"]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    LIMIT_ORDER_AVAILABLE_FLAG_FIELD_NUMBER: _ClassVar[int]
    MARKET_ORDER_AVAILABLE_FLAG_FIELD_NUMBER: _ClassVar[int]
    TIME_FIELD_NUMBER: _ClassVar[int]
    TRADING_STATUS_FIELD_NUMBER: _ClassVar[int]
    figi: str
    limit_order_available_flag: bool
    market_order_available_flag: bool
    time: _timestamp_pb2.Timestamp
    trading_status: _common_pb2.SecurityTradingStatus
    def __init__(self, figi: _Optional[str] = ..., trading_status: _Optional[_Union[_common_pb2.SecurityTradingStatus, str]] = ..., time: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., limit_order_available_flag: bool = ..., market_order_available_flag: bool = ...) -> None: ...

class SubscriptionAction(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []

class SubscriptionInterval(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []

class SubscriptionStatus(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []

class TradeDirection(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []

class CandleInterval(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
