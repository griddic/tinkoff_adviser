from google.protobuf import timestamp_pb2 as _timestamp_pb2
import common_pb2 as _common_pb2
from google.protobuf.internal import containers as _containers
from google.protobuf.internal import enum_type_wrapper as _enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Iterable as _Iterable, Mapping as _Mapping, Optional as _Optional, Union as _Union

ACCOUNT_ACCESS_LEVEL_FULL_ACCESS: AccessLevel
ACCOUNT_ACCESS_LEVEL_NO_ACCESS: AccessLevel
ACCOUNT_ACCESS_LEVEL_READ_ONLY: AccessLevel
ACCOUNT_ACCESS_LEVEL_UNSPECIFIED: AccessLevel
ACCOUNT_STATUS_CLOSED: AccountStatus
ACCOUNT_STATUS_NEW: AccountStatus
ACCOUNT_STATUS_OPEN: AccountStatus
ACCOUNT_STATUS_UNSPECIFIED: AccountStatus
ACCOUNT_TYPE_INVEST_BOX: AccountType
ACCOUNT_TYPE_TINKOFF: AccountType
ACCOUNT_TYPE_TINKOFF_IIS: AccountType
ACCOUNT_TYPE_UNSPECIFIED: AccountType
DESCRIPTOR: _descriptor.FileDescriptor

class Account(_message.Message):
    __slots__ = ["access_level", "closed_date", "id", "name", "opened_date", "status", "type"]
    ACCESS_LEVEL_FIELD_NUMBER: _ClassVar[int]
    CLOSED_DATE_FIELD_NUMBER: _ClassVar[int]
    ID_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    OPENED_DATE_FIELD_NUMBER: _ClassVar[int]
    STATUS_FIELD_NUMBER: _ClassVar[int]
    TYPE_FIELD_NUMBER: _ClassVar[int]
    access_level: AccessLevel
    closed_date: _timestamp_pb2.Timestamp
    id: str
    name: str
    opened_date: _timestamp_pb2.Timestamp
    status: AccountStatus
    type: AccountType
    def __init__(self, id: _Optional[str] = ..., type: _Optional[_Union[AccountType, str]] = ..., name: _Optional[str] = ..., status: _Optional[_Union[AccountStatus, str]] = ..., opened_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., closed_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., access_level: _Optional[_Union[AccessLevel, str]] = ...) -> None: ...

class GetAccountsRequest(_message.Message):
    __slots__ = []
    def __init__(self) -> None: ...

class GetAccountsResponse(_message.Message):
    __slots__ = ["accounts"]
    ACCOUNTS_FIELD_NUMBER: _ClassVar[int]
    accounts: _containers.RepeatedCompositeFieldContainer[Account]
    def __init__(self, accounts: _Optional[_Iterable[_Union[Account, _Mapping]]] = ...) -> None: ...

class GetInfoRequest(_message.Message):
    __slots__ = []
    def __init__(self) -> None: ...

class GetInfoResponse(_message.Message):
    __slots__ = ["prem_status", "qual_status", "qualified_for_work_with", "tariff"]
    PREM_STATUS_FIELD_NUMBER: _ClassVar[int]
    QUALIFIED_FOR_WORK_WITH_FIELD_NUMBER: _ClassVar[int]
    QUAL_STATUS_FIELD_NUMBER: _ClassVar[int]
    TARIFF_FIELD_NUMBER: _ClassVar[int]
    prem_status: bool
    qual_status: bool
    qualified_for_work_with: _containers.RepeatedScalarFieldContainer[str]
    tariff: str
    def __init__(self, prem_status: bool = ..., qual_status: bool = ..., qualified_for_work_with: _Optional[_Iterable[str]] = ..., tariff: _Optional[str] = ...) -> None: ...

class GetMarginAttributesRequest(_message.Message):
    __slots__ = ["account_id"]
    ACCOUNT_ID_FIELD_NUMBER: _ClassVar[int]
    account_id: str
    def __init__(self, account_id: _Optional[str] = ...) -> None: ...

class GetMarginAttributesResponse(_message.Message):
    __slots__ = ["amount_of_missing_funds", "funds_sufficiency_level", "liquid_portfolio", "minimal_margin", "starting_margin"]
    AMOUNT_OF_MISSING_FUNDS_FIELD_NUMBER: _ClassVar[int]
    FUNDS_SUFFICIENCY_LEVEL_FIELD_NUMBER: _ClassVar[int]
    LIQUID_PORTFOLIO_FIELD_NUMBER: _ClassVar[int]
    MINIMAL_MARGIN_FIELD_NUMBER: _ClassVar[int]
    STARTING_MARGIN_FIELD_NUMBER: _ClassVar[int]
    amount_of_missing_funds: _common_pb2.MoneyValue
    funds_sufficiency_level: _common_pb2.Quotation
    liquid_portfolio: _common_pb2.MoneyValue
    minimal_margin: _common_pb2.MoneyValue
    starting_margin: _common_pb2.MoneyValue
    def __init__(self, liquid_portfolio: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., starting_margin: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., minimal_margin: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., funds_sufficiency_level: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., amount_of_missing_funds: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ...) -> None: ...

class GetUserTariffRequest(_message.Message):
    __slots__ = []
    def __init__(self) -> None: ...

class GetUserTariffResponse(_message.Message):
    __slots__ = ["stream_limits", "unary_limits"]
    STREAM_LIMITS_FIELD_NUMBER: _ClassVar[int]
    UNARY_LIMITS_FIELD_NUMBER: _ClassVar[int]
    stream_limits: _containers.RepeatedCompositeFieldContainer[StreamLimit]
    unary_limits: _containers.RepeatedCompositeFieldContainer[UnaryLimit]
    def __init__(self, unary_limits: _Optional[_Iterable[_Union[UnaryLimit, _Mapping]]] = ..., stream_limits: _Optional[_Iterable[_Union[StreamLimit, _Mapping]]] = ...) -> None: ...

class StreamLimit(_message.Message):
    __slots__ = ["limit", "streams"]
    LIMIT_FIELD_NUMBER: _ClassVar[int]
    STREAMS_FIELD_NUMBER: _ClassVar[int]
    limit: int
    streams: _containers.RepeatedScalarFieldContainer[str]
    def __init__(self, limit: _Optional[int] = ..., streams: _Optional[_Iterable[str]] = ...) -> None: ...

class UnaryLimit(_message.Message):
    __slots__ = ["limit_per_minute", "methods"]
    LIMIT_PER_MINUTE_FIELD_NUMBER: _ClassVar[int]
    METHODS_FIELD_NUMBER: _ClassVar[int]
    limit_per_minute: int
    methods: _containers.RepeatedScalarFieldContainer[str]
    def __init__(self, limit_per_minute: _Optional[int] = ..., methods: _Optional[_Iterable[str]] = ...) -> None: ...

class AccountType(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []

class AccountStatus(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []

class AccessLevel(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
