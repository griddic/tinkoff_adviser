import common_pb2 as _common_pb2
from google.protobuf import timestamp_pb2 as _timestamp_pb2
from google.protobuf.internal import containers as _containers
from google.protobuf.internal import enum_type_wrapper as _enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Iterable as _Iterable, Mapping as _Mapping, Optional as _Optional, Union as _Union

DESCRIPTOR: _descriptor.FileDescriptor
EXECUTION_REPORT_STATUS_CANCELLED: OrderExecutionReportStatus
EXECUTION_REPORT_STATUS_FILL: OrderExecutionReportStatus
EXECUTION_REPORT_STATUS_NEW: OrderExecutionReportStatus
EXECUTION_REPORT_STATUS_PARTIALLYFILL: OrderExecutionReportStatus
EXECUTION_REPORT_STATUS_REJECTED: OrderExecutionReportStatus
EXECUTION_REPORT_STATUS_UNSPECIFIED: OrderExecutionReportStatus
ORDER_DIRECTION_BUY: OrderDirection
ORDER_DIRECTION_SELL: OrderDirection
ORDER_DIRECTION_UNSPECIFIED: OrderDirection
ORDER_TYPE_LIMIT: OrderType
ORDER_TYPE_MARKET: OrderType
ORDER_TYPE_UNSPECIFIED: OrderType
PRICE_TYPE_CURRENCY: PriceType
PRICE_TYPE_POINT: PriceType
PRICE_TYPE_UNSPECIFIED: PriceType

class CancelOrderRequest(_message.Message):
    __slots__ = ["account_id", "order_id"]
    ACCOUNT_ID_FIELD_NUMBER: _ClassVar[int]
    ORDER_ID_FIELD_NUMBER: _ClassVar[int]
    account_id: str
    order_id: str
    def __init__(self, account_id: _Optional[str] = ..., order_id: _Optional[str] = ...) -> None: ...

class CancelOrderResponse(_message.Message):
    __slots__ = ["time"]
    TIME_FIELD_NUMBER: _ClassVar[int]
    time: _timestamp_pb2.Timestamp
    def __init__(self, time: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ...) -> None: ...

class GetOrderStateRequest(_message.Message):
    __slots__ = ["account_id", "order_id"]
    ACCOUNT_ID_FIELD_NUMBER: _ClassVar[int]
    ORDER_ID_FIELD_NUMBER: _ClassVar[int]
    account_id: str
    order_id: str
    def __init__(self, account_id: _Optional[str] = ..., order_id: _Optional[str] = ...) -> None: ...

class GetOrdersRequest(_message.Message):
    __slots__ = ["account_id"]
    ACCOUNT_ID_FIELD_NUMBER: _ClassVar[int]
    account_id: str
    def __init__(self, account_id: _Optional[str] = ...) -> None: ...

class GetOrdersResponse(_message.Message):
    __slots__ = ["orders"]
    ORDERS_FIELD_NUMBER: _ClassVar[int]
    orders: _containers.RepeatedCompositeFieldContainer[OrderState]
    def __init__(self, orders: _Optional[_Iterable[_Union[OrderState, _Mapping]]] = ...) -> None: ...

class OrderStage(_message.Message):
    __slots__ = ["price", "quantity", "trade_id"]
    PRICE_FIELD_NUMBER: _ClassVar[int]
    QUANTITY_FIELD_NUMBER: _ClassVar[int]
    TRADE_ID_FIELD_NUMBER: _ClassVar[int]
    price: _common_pb2.MoneyValue
    quantity: int
    trade_id: str
    def __init__(self, price: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., quantity: _Optional[int] = ..., trade_id: _Optional[str] = ...) -> None: ...

class OrderState(_message.Message):
    __slots__ = ["average_position_price", "currency", "direction", "executed_commission", "executed_order_price", "execution_report_status", "figi", "initial_commission", "initial_order_price", "initial_security_price", "lots_executed", "lots_requested", "order_date", "order_id", "order_type", "service_commission", "stages", "total_order_amount"]
    AVERAGE_POSITION_PRICE_FIELD_NUMBER: _ClassVar[int]
    CURRENCY_FIELD_NUMBER: _ClassVar[int]
    DIRECTION_FIELD_NUMBER: _ClassVar[int]
    EXECUTED_COMMISSION_FIELD_NUMBER: _ClassVar[int]
    EXECUTED_ORDER_PRICE_FIELD_NUMBER: _ClassVar[int]
    EXECUTION_REPORT_STATUS_FIELD_NUMBER: _ClassVar[int]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    INITIAL_COMMISSION_FIELD_NUMBER: _ClassVar[int]
    INITIAL_ORDER_PRICE_FIELD_NUMBER: _ClassVar[int]
    INITIAL_SECURITY_PRICE_FIELD_NUMBER: _ClassVar[int]
    LOTS_EXECUTED_FIELD_NUMBER: _ClassVar[int]
    LOTS_REQUESTED_FIELD_NUMBER: _ClassVar[int]
    ORDER_DATE_FIELD_NUMBER: _ClassVar[int]
    ORDER_ID_FIELD_NUMBER: _ClassVar[int]
    ORDER_TYPE_FIELD_NUMBER: _ClassVar[int]
    SERVICE_COMMISSION_FIELD_NUMBER: _ClassVar[int]
    STAGES_FIELD_NUMBER: _ClassVar[int]
    TOTAL_ORDER_AMOUNT_FIELD_NUMBER: _ClassVar[int]
    average_position_price: _common_pb2.MoneyValue
    currency: str
    direction: OrderDirection
    executed_commission: _common_pb2.MoneyValue
    executed_order_price: _common_pb2.MoneyValue
    execution_report_status: OrderExecutionReportStatus
    figi: str
    initial_commission: _common_pb2.MoneyValue
    initial_order_price: _common_pb2.MoneyValue
    initial_security_price: _common_pb2.MoneyValue
    lots_executed: int
    lots_requested: int
    order_date: _timestamp_pb2.Timestamp
    order_id: str
    order_type: OrderType
    service_commission: _common_pb2.MoneyValue
    stages: _containers.RepeatedCompositeFieldContainer[OrderStage]
    total_order_amount: _common_pb2.MoneyValue
    def __init__(self, order_id: _Optional[str] = ..., execution_report_status: _Optional[_Union[OrderExecutionReportStatus, str]] = ..., lots_requested: _Optional[int] = ..., lots_executed: _Optional[int] = ..., initial_order_price: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., executed_order_price: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., total_order_amount: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., average_position_price: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., initial_commission: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., executed_commission: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., figi: _Optional[str] = ..., direction: _Optional[_Union[OrderDirection, str]] = ..., initial_security_price: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., stages: _Optional[_Iterable[_Union[OrderStage, _Mapping]]] = ..., service_commission: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., currency: _Optional[str] = ..., order_type: _Optional[_Union[OrderType, str]] = ..., order_date: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ...) -> None: ...

class OrderTrade(_message.Message):
    __slots__ = ["date_time", "price", "quantity"]
    DATE_TIME_FIELD_NUMBER: _ClassVar[int]
    PRICE_FIELD_NUMBER: _ClassVar[int]
    QUANTITY_FIELD_NUMBER: _ClassVar[int]
    date_time: _timestamp_pb2.Timestamp
    price: _common_pb2.Quotation
    quantity: int
    def __init__(self, date_time: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., price: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., quantity: _Optional[int] = ...) -> None: ...

class OrderTrades(_message.Message):
    __slots__ = ["account_id", "created_at", "direction", "figi", "order_id", "trades"]
    ACCOUNT_ID_FIELD_NUMBER: _ClassVar[int]
    CREATED_AT_FIELD_NUMBER: _ClassVar[int]
    DIRECTION_FIELD_NUMBER: _ClassVar[int]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    ORDER_ID_FIELD_NUMBER: _ClassVar[int]
    TRADES_FIELD_NUMBER: _ClassVar[int]
    account_id: str
    created_at: _timestamp_pb2.Timestamp
    direction: OrderDirection
    figi: str
    order_id: str
    trades: _containers.RepeatedCompositeFieldContainer[OrderTrade]
    def __init__(self, order_id: _Optional[str] = ..., created_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., direction: _Optional[_Union[OrderDirection, str]] = ..., figi: _Optional[str] = ..., trades: _Optional[_Iterable[_Union[OrderTrade, _Mapping]]] = ..., account_id: _Optional[str] = ...) -> None: ...

class PostOrderRequest(_message.Message):
    __slots__ = ["account_id", "direction", "figi", "order_id", "order_type", "price", "quantity"]
    ACCOUNT_ID_FIELD_NUMBER: _ClassVar[int]
    DIRECTION_FIELD_NUMBER: _ClassVar[int]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    ORDER_ID_FIELD_NUMBER: _ClassVar[int]
    ORDER_TYPE_FIELD_NUMBER: _ClassVar[int]
    PRICE_FIELD_NUMBER: _ClassVar[int]
    QUANTITY_FIELD_NUMBER: _ClassVar[int]
    account_id: str
    direction: OrderDirection
    figi: str
    order_id: str
    order_type: OrderType
    price: _common_pb2.Quotation
    quantity: int
    def __init__(self, figi: _Optional[str] = ..., quantity: _Optional[int] = ..., price: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., direction: _Optional[_Union[OrderDirection, str]] = ..., account_id: _Optional[str] = ..., order_type: _Optional[_Union[OrderType, str]] = ..., order_id: _Optional[str] = ...) -> None: ...

class PostOrderResponse(_message.Message):
    __slots__ = ["aci_value", "direction", "executed_commission", "executed_order_price", "execution_report_status", "figi", "initial_commission", "initial_order_price", "initial_order_price_pt", "initial_security_price", "lots_executed", "lots_requested", "message", "order_id", "order_type", "total_order_amount"]
    ACI_VALUE_FIELD_NUMBER: _ClassVar[int]
    DIRECTION_FIELD_NUMBER: _ClassVar[int]
    EXECUTED_COMMISSION_FIELD_NUMBER: _ClassVar[int]
    EXECUTED_ORDER_PRICE_FIELD_NUMBER: _ClassVar[int]
    EXECUTION_REPORT_STATUS_FIELD_NUMBER: _ClassVar[int]
    FIGI_FIELD_NUMBER: _ClassVar[int]
    INITIAL_COMMISSION_FIELD_NUMBER: _ClassVar[int]
    INITIAL_ORDER_PRICE_FIELD_NUMBER: _ClassVar[int]
    INITIAL_ORDER_PRICE_PT_FIELD_NUMBER: _ClassVar[int]
    INITIAL_SECURITY_PRICE_FIELD_NUMBER: _ClassVar[int]
    LOTS_EXECUTED_FIELD_NUMBER: _ClassVar[int]
    LOTS_REQUESTED_FIELD_NUMBER: _ClassVar[int]
    MESSAGE_FIELD_NUMBER: _ClassVar[int]
    ORDER_ID_FIELD_NUMBER: _ClassVar[int]
    ORDER_TYPE_FIELD_NUMBER: _ClassVar[int]
    TOTAL_ORDER_AMOUNT_FIELD_NUMBER: _ClassVar[int]
    aci_value: _common_pb2.MoneyValue
    direction: OrderDirection
    executed_commission: _common_pb2.MoneyValue
    executed_order_price: _common_pb2.MoneyValue
    execution_report_status: OrderExecutionReportStatus
    figi: str
    initial_commission: _common_pb2.MoneyValue
    initial_order_price: _common_pb2.MoneyValue
    initial_order_price_pt: _common_pb2.Quotation
    initial_security_price: _common_pb2.MoneyValue
    lots_executed: int
    lots_requested: int
    message: str
    order_id: str
    order_type: OrderType
    total_order_amount: _common_pb2.MoneyValue
    def __init__(self, order_id: _Optional[str] = ..., execution_report_status: _Optional[_Union[OrderExecutionReportStatus, str]] = ..., lots_requested: _Optional[int] = ..., lots_executed: _Optional[int] = ..., initial_order_price: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., executed_order_price: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., total_order_amount: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., initial_commission: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., executed_commission: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., aci_value: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., figi: _Optional[str] = ..., direction: _Optional[_Union[OrderDirection, str]] = ..., initial_security_price: _Optional[_Union[_common_pb2.MoneyValue, _Mapping]] = ..., order_type: _Optional[_Union[OrderType, str]] = ..., message: _Optional[str] = ..., initial_order_price_pt: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ...) -> None: ...

class ReplaceOrderRequest(_message.Message):
    __slots__ = ["account_id", "idempotency_key", "order_id", "price", "price_type", "quantity"]
    ACCOUNT_ID_FIELD_NUMBER: _ClassVar[int]
    IDEMPOTENCY_KEY_FIELD_NUMBER: _ClassVar[int]
    ORDER_ID_FIELD_NUMBER: _ClassVar[int]
    PRICE_FIELD_NUMBER: _ClassVar[int]
    PRICE_TYPE_FIELD_NUMBER: _ClassVar[int]
    QUANTITY_FIELD_NUMBER: _ClassVar[int]
    account_id: str
    idempotency_key: str
    order_id: str
    price: _common_pb2.Quotation
    price_type: PriceType
    quantity: int
    def __init__(self, account_id: _Optional[str] = ..., order_id: _Optional[str] = ..., idempotency_key: _Optional[str] = ..., quantity: _Optional[int] = ..., price: _Optional[_Union[_common_pb2.Quotation, _Mapping]] = ..., price_type: _Optional[_Union[PriceType, str]] = ...) -> None: ...

class TradesStreamRequest(_message.Message):
    __slots__ = ["accounts"]
    ACCOUNTS_FIELD_NUMBER: _ClassVar[int]
    accounts: _containers.RepeatedScalarFieldContainer[str]
    def __init__(self, accounts: _Optional[_Iterable[str]] = ...) -> None: ...

class TradesStreamResponse(_message.Message):
    __slots__ = ["order_trades", "ping"]
    ORDER_TRADES_FIELD_NUMBER: _ClassVar[int]
    PING_FIELD_NUMBER: _ClassVar[int]
    order_trades: OrderTrades
    ping: _common_pb2.Ping
    def __init__(self, order_trades: _Optional[_Union[OrderTrades, _Mapping]] = ..., ping: _Optional[_Union[_common_pb2.Ping, _Mapping]] = ...) -> None: ...

class OrderDirection(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []

class OrderType(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []

class OrderExecutionReportStatus(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []

class PriceType(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
