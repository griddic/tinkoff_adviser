import os
import re
from pathlib import Path

if __name__ == '__main__':
    clients = Path('clients')
    for file_name in os.listdir(clients.name):
        if not (file_name.endswith('_pb2_grpc.py') or file_name.endswith('_pb2.py')):
            continue
        file_path = clients / file_name
        content = file_path.read_text()
        content = re.sub(r'^import (\w+_pb2 as \w+__pb2)$', r'import clients.\1', content, flags=re.M)
        file_path.write_text(content)