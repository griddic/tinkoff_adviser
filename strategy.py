import logging
from datetime import datetime
from functools import lru_cache
from typing import Dict, Union, List

from money import Money, RUB
from settings import CONFIG

LOGGER_NAME = "strategy"
LOGGER = logging.getLogger(LOGGER_NAME)

__all__ = ['Strategy']


class CurrencyWeights:
    def __init__(self, raw, total: Money, currency, hold_in_bonds):
        self._raw = raw
        self._total: Money = total
        self._currency = currency
        self._hold_in_bonds = hold_in_bonds

    @property
    def total(self) -> Money:
        return self._total.to(self._currency)

    @property
    def to_distribute(self):
        return self.total - self._hold_in_bonds

    @property
    def _bonds_w(self):
        return self._raw['bonds']

    @property
    def _shares_w(self):
        return self._raw['shares']

    @property
    def _total_w(self):
        return self._bonds_w + self._shares_w

    @property
    def bonds(self) -> Money:
        return (self.to_distribute * (self._bonds_w / self._total_w) + self._hold_in_bonds).to(self._currency)

    @property
    def shares(self) -> Money:
        return (self.to_distribute * (self._shares_w / self._total_w)).to(self._currency)


class Limits:
    def __init__(self, raw, overall_cost: Money):
        self._raw = raw
        self._overall_cost: Money = overall_cost.to('rub')

    @property
    def overall(self) -> Money:
        return self._overall_cost * self._raw['overall']

    @property
    def monthly(self) -> Money:
        return self._overall_cost * self._raw['monthly']


class Goal:
    format = '%Y-%m-%d'

    def __init__(self, name, raw):
        self._name = name
        self._raw = raw

    @property
    def name(self):
        return self._name

    @property
    def currency(self):
        return self._raw['price']['currency']

    @property
    def target(self):
        return Money(self._raw['price']['amount'], self.currency)

    @property
    def current_reserve(self):
        return self.target * self._maturity

    @property
    def _maturity(self):
        end = self._raw['time']['to']
        begin = self._raw['time']['from']
        now = datetime.now().date()
        maturity = (now - begin) / (end - begin)
        maturity = max(0.0, maturity)
        maturity = min(1.0, maturity)
        return maturity


class Strategy:
    curencies = ['usd', 'rub']

    def __init__(self, raw, overall_cost: Money):
        self._raw = raw
        self.overall_cost: Money = overall_cost

    @classmethod
    def load(cls, overall_cost):
        return cls(CONFIG.strategy, overall_cost)

    @property
    def total(self) -> Dict[str, CurrencyWeights]:
        for_distribution = self.overall_cost - sum(self.hold_for_goals.values())

        weights = {
            c: self._raw['weights'][c]['w']
            for c in self.curencies
        }
        total_w = sum(weights.values())
        distribute_without_goals = {
            c: for_distribution * (weights[c] / total_w)
            for c in self.curencies
        }
        return {
            cur: CurrencyWeights(self._raw['weights'][cur],
                                 distribute_without_goals[cur] + self.hold_for_goals[cur],
                                 cur,
                                 self.hold_for_goals[cur])
            for cur in Strategy.curencies
        }

    @property
    def borders(self) -> Dict[str, Dict[str, Union[int, float]]]:
        return {
            c: {ticker: Money(price, c) for ticker, price in self._raw['equities'][c].items()}
            for c in self.curencies
        }

    @property
    def limits(self) -> Limits:
        return Limits(self._raw['relative limits'], self.overall_cost)

    @property
    @lru_cache()
    def goals(self) -> List[Goal]:
        if goals_ := self._raw.get('goals'):
            return [Goal(name, raw) for name, raw in goals_.items()]
        return []

    @property
    @lru_cache()
    def hold_for_goals(self) -> Dict[str, Money]:
        hold = {c: Money(0, c) for c in self.curencies}
        for goal in self.goals:
            hold[goal.currency] += goal.current_reserve
        return hold

    @property
    def minimal_transaction(self):
        return max(0, min(0.5 * self.limits.monthly, 100_000 * RUB))
