import logging
import os
import shutil
from pathlib import Path
from typing import List

import click
import cryptocode
import numpy as np
import telebot
from dotenv import load_dotenv

import open_api
from adviser_code import configure_logging, POSITIONS_FILE_PATH, Adviser, get_suitable_bonds
from broker import Broker
from settings import CONFIG
from site_api import Bond

LOGGER_NAME = "adviser_cli"
LOGGER = logging.getLogger(LOGGER_NAME)


@click.group()
def advise():
    pass


@advise.command(name="analyze")
def analyze():
    with open_api.Client.broker() as client:
        Adviser(client).analyze_portfolio()


@advise.command(name="find_bonds")
def find_bonds():
    interest_bonds: List[Bond] = get_suitable_bonds('rub', acceptable_years_to_maturity=1)
    yields = [d.yield_ for d in interest_bonds]

    order = np.argsort(yields)[::-1]
    bonds_ordered = [interest_bonds[i] for i in order]
    for i in range(10):
        print(bonds_ordered[i], bonds_ordered[i].yield_)


@advise.command(name="place-orders")
def place_orders():
    Broker().place_orders_from_file(POSITIONS_FILE_PATH)


@advise.command(name="encrypt")
def encrypt():
    password = os.getenv("VAULT_PASSWORD")
    if not password:
        raise ValueError("VAULT_PASSWORD not set.")

    for file_path in filter(Path.is_file, CONFIG.decrypted_dir.iterdir()):
        decrypted = file_path.read_text()
        out_file = CONFIG.encrypted_dir / file_path.name
        CONFIG.encrypted_dir.mkdir(parents=True, exist_ok=True)
        with out_file.open('w') as outt:
            print(cryptocode.encrypt(decrypted, password), file=outt)


@advise.command(name="decrypt")
def decrypt():
    password = os.getenv("VAULT_PASSWORD")
    if not password:
        raise ValueError("VAULT_PASSWORD not set.")

    for file_name in filter(Path.is_file, CONFIG.encrypted_dir.iterdir()):
        print(f"Decrypting file {file_name} .")
        with open(CONFIG.encrypted_dir / file_name) as inn:
            decrypted = cryptocode.decrypt(inn.read(), password)
        CONFIG.decrypted_dir.mkdir(parents=True, exist_ok=True)
        out_file = CONFIG.decrypted_dir / file_name.name
        with open(out_file, "w") as outt:
            outt.write(decrypted)


def split_message_into_chunks(message: str, limit=3500) -> List[str]:
    chunks = []
    start = 0
    while start < len(message):
        if len(message) - start < limit:
            chunks.append(message[start:len(message)])
            break
        end = message.rfind('\n', start, start + limit)
        if end == -1:
            end = start + limit
        else:
            end += 1
        end = min(end, start + limit)
        end = min(end, len(message))
        assert start < end
        chunks.append(message[start:end])
        start = end
    return chunks


@advise.command(name="notify")
def notify_telegram():
    message = Path("logs/telegram.log").read_text()
    if not message:
        LOGGER.warning("Message to telegram is empty.")
        return
    token = CONFIG.telegram_token
    user_id = CONFIG.telegram_user
    bot = telebot.TeleBot(token, parse_mode='Markdown')
    for chunk in split_message_into_chunks(message):
        bot.send_message(user_id, "\n".join(["```", chunk, "```"]))


@advise.command(name="clean")
def clean():
    shutil.rmtree('logs')


if __name__ == "__main__":
    load_dotenv()
    configure_logging()
    advise()
