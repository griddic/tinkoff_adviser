import logging
from collections import namedtuple, defaultdict
from copy import deepcopy
from datetime import datetime, timedelta
from functools import cached_property, lru_cache
from logging.config import dictConfig
from pathlib import Path
from typing import List

import numpy as np
import yaml
from tabulate import tabulate

from beauty_report.beauty_reporter import PrettyReport
from broker import OrderToPlace
from temporary import DO_NOT_BY_CURRENCIES, NEVER_BOUGHT_SHARES
from money import RUB, USD, Money
from open_api import OpenApiError, Client, usd_rub_instrument
from settings import CONFIG
from site_api import SiteApi, Bond, Share, Instrument, Currency
from strategy import Strategy

USD << SiteApi.usd_rub_exchange() * RUB

LOGGER_NAME = "adviser_impl"
LOGGER = logging.getLogger(LOGGER_NAME)

POSITIONS_FILE_PATH = Path("samples") / "orders.yaml"
POSITIONS_FILE_PATH.parent.mkdir(parents=True, exist_ok=True)


class CompanyPaymentsSummary:
    def __init__(self):
        self.bought_last_month = 0
        self.actual_cost = 0
        self.cost_in_original_prices = 0

    def __repr__(self):
        return f'Summary({self.bought_last_month}, {self.actual_cost}, {self.cost_in_original_prices})'


class Adviser:
    def __init__(self, client: Client):
        self.client = client
        self.beauty_report = PrettyReport()
        self.orders: List[OrderToPlace] = []  # amount to invest decreases dynamicly, we need to fill orders on the fly

    @cached_property
    def strategy(self):
        return load_strategy_from_file(self.client.portfolio_cost_no_nkd)

    @cached_property
    def free_cash(self):
        _free_cash = self.client.free_cash()
        self.beauty_report.set_free_cash(_free_cash)
        LOGGER.info(f"free cash: {', '.join(map(str, _free_cash.values()))}.")
        return _free_cash

    @cached_property
    def to_invest(self):
        _to_invest = deepcopy(self.free_cash)
        if datetime.now().month in (1, 12):
            _to_invest["rub"] = max(0 * RUB, _to_invest["rub"] - CONFIG.hold_for_taxes * RUB)
            LOGGER.info(f"Hold some money ({CONFIG.hold_for_taxes * RUB}) for taxes.")
            LOGGER.info(f"to invest: {_to_invest}")
        return _to_invest

    @cached_property
    def _companies_payment_summary(self) -> dict[str, CompanyPaymentsSummary]:
        summaries = defaultdict(CompanyPaymentsSummary)
        for position in self.client.equities_positions:
            summary = summaries[position.company_name]
            summary.bought_last_month += self.client.bought_last_month(position.figi)
            summary.actual_cost += position.cost_no_nkd
            summary.cost_in_original_prices += position.payed

        for company in sorted(summaries.keys()):
            summary = summaries[company]
            LOGGER.debug(f"{company=} {summary=}")
            if summary.bought_last_month > self.strategy.limits.monthly:
                LOGGER.warning(f'{company=} overbought monthly: {summary.bought_last_month} > {self.strategy.limits.monthly}')
            if summary.actual_cost > self.strategy.limits.overall:
                LOGGER.warning(f'{company=} overbought (actual cost): {summary.actual_cost} > {self.strategy.limits.overall}')
            if summary.cost_in_original_prices > self.strategy.limits.overall:
                LOGGER.warning(
                    f'{company=} overbought (payed cost): {summary.cost_in_original_prices} > {self.strategy.limits.overall}')

        return summaries

    @lru_cache()
    def payment_summary(self, company_name):
        return self._companies_payment_summary[company_name]

    def analyze_portfolio(self):
        self.find_instruments_to_buy()
        self.find_shares_to_sell()

        figis = [o.figi for o in self.orders]
        assert len(set(figis)) == len(figis)
        OrderToPlace.to_yaml_file(self.orders, POSITIONS_FILE_PATH)

        for order in self.orders:
            LOGGER.info(f"Place {order.side} {order.ticker} [{order.show_name}] {order.volume}")
            self.beauty_report.positions.append(
                [
                    order.show_name,
                    order.side.value,
                    Money(order.volume, order._currency),
                ]
            )
        with open('logs/telegram.log', 'w') as out:
            out.write(self.beauty_report.telegram_report())

    def find_instruments_to_buy(self):
        # transfer rub <-> usd
        self.balance_currencies()

        for currency in ["usd", "rub"]:
            in_shares = sum(p.cost_no_nkd for p in
                            self.client.filter_positions(currency=currency,
                                                         instrument_type="Stock"))
            in_bonds = sum(p.cost_no_nkd for p in
                           self.client.filter_positions(currency=currency, instrument_type="Bond"))
            LOGGER.info(f'Analyzing {currency}.')
            to_shares, to_bonds = self.to_shares_to_bonds(self.to_invest[currency], in_shares, in_bonds,
                                                          self.strategy.total[currency].shares,
                                                          self.strategy.total[currency].bonds,
                                                          currency)
            LOGGER.info(f'Will buy shares on: {to_shares}')
            LOGGER.info(f'Will buy bonds on: {to_bonds}')
            self.beauty_report.set_buy_shares_bonds(currency, to_shares, to_bonds)
            # to_buy[f"{currency}.shares"] = {}
            # to_buy[f"{currency}.bonds"] = {}
            left = Money(0, currency)
            if to_shares > self.strategy.minimal_transaction:
                left_from_shares = self.distribute_between_shares(to_shares, currency)
                LOGGER.info(f'left from shares: {left_from_shares}')
                left += left_from_shares
            if to_bonds > self.strategy.minimal_transaction:
                left_from_bonds = self.distribute_between_bonds(to_bonds, currency)
                LOGGER.info(f'left from bonds: {left_from_bonds}')
                left += left_from_bonds
            LOGGER.info(f'left for {currency}: {left}')
            self.beauty_report.set_buy_left(currency, left)

    def balance_currencies(self):
        LOGGER.debug("Going to balance currencies.")
        free_usd = self.to_invest["usd"]
        free_rub = self.to_invest["rub"]
        usd_positions_sum = sum(p.cost_no_nkd for p in self.client.filter_positions("usd"))
        rub_positions_sum = sum(p.cost_no_nkd for p in self.client.filter_positions("rub"))

        total = usd_positions_sum + rub_positions_sum + free_usd + free_rub
        LOGGER.info(f"Currencies balance: USD: {usd_positions_sum / total:.3f}, RUB: {rub_positions_sum / total:.3f}")
        self.beauty_report.set_actual_currencies_balance(usd_positions_sum, rub_positions_sum)

        should_be_in_usd = self.strategy.total["usd"].total
        should_be_in_rub = self.strategy.total["rub"].total
        LOGGER.info(f"  Expected balance: USD: {should_be_in_usd / (should_be_in_usd + should_be_in_rub):.3f}, "
                    f"RUB: {should_be_in_rub / (should_be_in_usd + should_be_in_rub):.3f}")
        self.beauty_report.set_expected_currencies_balance(should_be_in_usd, should_be_in_rub)

        can_transferm_to_usd = should_be_in_usd - usd_positions_sum - free_usd
        can_transferm_to_rub = should_be_in_rub - rub_positions_sum - free_rub

        LOGGER.info(f"Can be added without extend limits "
                    f"to USD: {can_transferm_to_usd} ({can_transferm_to_usd.to('rub')}) , "
                    f"to RUB {can_transferm_to_rub}")
        # since to_invest is not equal to free_cash (hold for taxes)
        # this assertion has no sense
        # assert can_transferm_to_rub + can_transferm_to_usd <= total * 0.0001, \
        #     f"Disbalanced: can_add_to_usd: {can_transferm_to_usd}, can_add_to_rub: {can_transferm_to_rub}"
        can_transferm_to_rub = min(free_usd, max(can_transferm_to_rub, 0 * USD))
        can_transferm_to_usd = min(free_rub, max(can_transferm_to_usd, 0 * RUB))
        LOGGER.info(f"Can be added without extend limits "
                    f"to USD: {can_transferm_to_usd} ({can_transferm_to_usd.to('rub')}) , "
                    f"to RUB {can_transferm_to_rub}")

        if DO_NOT_BY_CURRENCIES:
            return

        usd_rub: Currency = usd_rub_instrument()
        if can_transferm_to_usd > can_transferm_to_rub:
            LOGGER.info(
                f"I will exchange {can_transferm_to_usd.to('rub')} to {can_transferm_to_usd.to('usd')}.")
            self.to_invest["rub"] = self.to_invest["rub"] - can_transferm_to_usd
            assert self.to_invest["rub"] >= 0, self.to_invest["rub"]
            self.beauty_report.transfer_from = 'rub'
            self.beauty_report.transfer_to = 'usd'
            self.beauty_report.transfer_amount = can_transferm_to_usd.to('rub')

            transaction_lots = int(can_transferm_to_usd / usd_rub.lot_price)
            if transaction_lots > 0:
                self.orders.append(
                    OrderToPlace.buy(
                        figi=usd_rub.figi,
                        ticker=usd_rub.ticker,
                        lots=transaction_lots,
                        price=usd_rub.price,
                        lot_size=usd_rub.lot_size,
                        show_name=usd_rub.show_name,
                    )
                )
        else:
            LOGGER.info(
                f"I will exchange {can_transferm_to_rub.to('usd')} to {can_transferm_to_rub.to('rub')}.")
            self.to_invest["usd"] = self.to_invest["usd"] - can_transferm_to_rub
            assert self.to_invest["usd"] >= 0
            self.beauty_report.transfer_from = 'usd'
            self.beauty_report.transfer_to = 'rub'
            self.beauty_report.transfer_amount = can_transferm_to_rub.to('usd')

            transaction_lots = int(can_transferm_to_rub / usd_rub.lot_price)
            if transaction_lots > 0:
                self.orders.append(OrderToPlace.sell(
                    figi=usd_rub.figi,
                    ticker=usd_rub.ticker,
                    lots=transaction_lots,
                    price=usd_rub.price,
                    lot_size=usd_rub.lot_size,
                    show_name=usd_rub.show_name,
                ))
        LOGGER.debug(f"to invest: {self.to_invest}")
        LOGGER.debug("Currencies balance finished.")

    def distribute_between_shares(self, to_invest, currency):
        interest_shares_tickers = self.strategy.borders[currency].keys()

        interest_shares = [Share(SiteApi.share_by_ticker(t)) for t in interest_shares_tickers]
        interest_shares = [s for s in interest_shares if s.is_valid()]

        borders = self.strategy.borders[currency]

        yields = [s.price / borders[s.ticker] for s in interest_shares]
        yields_map = {s: s.price / borders[s.ticker] for s in interest_shares}

        Row = namedtuple("Row", "ticker price border yield_ status")
        report_table = []

        def report(share: Share, msg):
            report_table.append(Row(
                share.ticker, share.price, borders[share.ticker], yields_map[share], msg
            ))

        order = np.argsort(yields)
        shares_ordered = [interest_shares[i] for i in order]
        left = self.create_positions_for_instruments(to_invest, shares_ordered, report)

        LOGGER.debug("\n".join([
            "Интересующие акции: ",
            tabulate(report_table, headers=Row._fields)
        ]))
        return left

    def create_positions_for_instruments(self,
                                         to_invest: Money,
                                         instruments_ordered: List[Instrument],
                                         report) -> Money:
        class NotEnoughMoneyForMinimalTransaction(Exception):
            pass

        class TransactionIsTooSmall(Exception):
            ...

        class Overbought(Exception):
            pass

        class NotEnoughMoneyForALot(Exception):
            pass

        left = to_invest.copy()

        currency = to_invest.currency

        for instrument in instruments_ordered:
            try:
                if left <= self.strategy.minimal_transaction:
                    raise NotEnoughMoneyForMinimalTransaction()

                payment_summary = self.payment_summary(instrument.company_name)
                if (monthly_left := (self.strategy.limits.monthly - payment_summary.bought_last_month)) <= 0:
                    raise Overbought(f"monthly ({payment_summary.bought_last_month.to('rub')})")
                if (overall_left := (self.strategy.limits.overall - payment_summary.actual_cost)) <= 0:
                    raise Overbought(f"overall in current prices ({payment_summary.actual_cost.to('rub')})")
                if (bought_left := (self.strategy.limits.overall - payment_summary.cost_in_original_prices)) <= 0:
                    LOGGER.warning(f"company {instrument.company_name} is overbought overall "
                                   f"in original prices {payment_summary.cost_in_original_prices.to('rub')} "
                                   f"with limit {self.strategy.limits.overall.to('rub')}.")
                    raise Overbought(
                        f"overall in original prices ({payment_summary.cost_in_original_prices.to('rub')})")

                transaction = min(monthly_left, overall_left, bought_left,
                                  left - CONFIG.reserve_on_rounding * RUB,
                                  # Если слишком точно предскажем,
                                  # то из-за движений рынка может оказаться недостаточно средств для покупки
                                  )

                lots = int(transaction / (instrument.lot_size * instrument.price))
                figi = self.client.figi_from_ticker(instrument.ticker)
                already_placed_lots = self.client.lots_requested_to_buy(figi)
                lots = lots - already_placed_lots
                if lots < 1:
                    raise NotEnoughMoneyForALot()
                transaction = lots * instrument.lot_size * instrument.price
                if transaction < self.strategy.minimal_transaction:
                    raise TransactionIsTooSmall(f'{transaction}')
                self.orders.append(
                    OrderToPlace.buy(
                        figi=figi,
                        ticker=instrument.ticker,
                        price=instrument.price.to(currency),
                        lots=lots,
                        lot_size=instrument.lot_size,
                        show_name=instrument.show_name,
                    )
                )
                report(instrument, f"{lots} lots will be bought.")
                left -= transaction
            except NotEnoughMoneyForMinimalTransaction as e:
                report(instrument, repr(e))
                break
            except (OpenApiError, Overbought, NotEnoughMoneyForALot, TransactionIsTooSmall) as e:
                report(instrument, repr(e))
        return left

    def distribute_between_bonds(self, to_invest, currency):
        interest_bonds: List[Bond] = get_suitable_bonds(currency)
        Row = namedtuple("Row", "name ticker yield_ status")
        report_table = []
        yields = [d.yield_ for d in interest_bonds]

        order = np.argsort(yields)[::-1]
        bonds_ordered = [interest_bonds[i] for i in order]

        def report(bond: Bond, msg):
            report_table.append(Row(
                bond.show_name, bond.ticker, bond.yield_, msg
            ))

        left = self.create_positions_for_instruments(to_invest, bonds_ordered, report)

        LOGGER.debug("\n".join([
            "Bond statuses:",
            tabulate(report_table, headers=Row._fields)
        ]))
        return left

    def find_shares_to_sell(self):
        time_shift = timedelta(days=365 * 4)
        # time_shift = time_shift or timedelta(days=365 * 2)

        strategy = load_strategy_from_file(self.client.portfolio_cost_no_nkd)
        tickers_in_strategies = []
        for _, borders_for_currency in strategy.borders.items():
            tickers_in_strategies.extend(list(borders_for_currency.keys()))
        for position in self.client.filter_positions(instrument_type='Stock'):
            if position.ticker in tickers_in_strategies:
                continue
            last_bought_date = self.client.last_bought_date(position.figi, name=position.name)
            if last_bought_date is None:
                if position.figi in NEVER_BOUGHT_SHARES:
                    last_bought_date = NEVER_BOUGHT_SHARES[position.figi]['last bought date']
                else:
                    last_bought_date = self.client.first_operation(position.figi).date
            if last_bought_date > datetime.now(tz=last_bought_date.tzinfo) - time_shift:
                continue
            self.orders.append(OrderToPlace.sell(
                figi=position.figi,
                ticker=position.ticker,
                lots=position.lots,
                price=position.price_from_candles,
                lot_size=position.lot_size,
                show_name=position.name,
            ))

    def to_shares_to_bonds(self, to_distribute, in_shares, in_bonds, shares_weight, bonds_weight, currency):
        assert to_distribute >= 0
        assert shares_weight >= 0
        assert bonds_weight >= 0
        total_w = shares_weight + bonds_weight
        shares_weight, bonds_weight = shares_weight / total_w, bonds_weight / total_w
        LOGGER.info(f'expected in {currency} shares {shares_weight:.3f} in bonds {bonds_weight:.3f}.')
        total_now = in_bonds + in_shares
        in_shares_weight_now, in_bonds_weight_now = in_shares / total_now, in_bonds / total_now
        LOGGER.info(f'  actual in {currency} shares {in_shares_weight_now:.3f} in bonds {in_bonds_weight_now:.3f}')
        self.beauty_report.set_balance_shares_bonds(currency, shares_weight, bonds_weight,
                                                    in_shares_weight_now, in_bonds_weight_now)
        total = in_shares + in_bonds + to_distribute
        should_be_in_shares, should_be_in_bonds = total * shares_weight, total * bonds_weight
        assert should_be_in_shares >= 0
        assert should_be_in_bonds >= 0
        left = to_distribute
        to_shares = min(left, max(Money(0, currency), should_be_in_shares - in_shares))
        left -= to_shares
        to_bonds = min(left, max(Money(0, currency), should_be_in_bonds - in_bonds))
        left -= to_bonds
        assert left < RUB, left
        assert left + to_shares + to_bonds == to_distribute
        return to_shares, to_bonds


def configure_logging():
    Path("logs").mkdir(exist_ok=True)
    with open("logger_config.yml") as inn:
        dictConfig(yaml.safe_load(inn))


def get_suitable_bonds(currency, acceptable_years_to_maturity=15):
    interest_bonds = []
    Row = namedtuple("Row", "name ticker rejectionReason")
    report_table = []

    class BondNotSuits(Exception):
        pass

    for bond in SiteApi.get_all_bonds():
        try:
            if bond.currency != currency.lower():
                continue
            if bond.subordinated:
                raise BondNotSuits("subordinated")
            if bond.floating_coupon:
                raise BondNotSuits("has floating coupon")
            if not bond.reliable:
                raise BondNotSuits("is not reliable")
            last_acceptable_maturity_year = datetime.now().year + acceptable_years_to_maturity
            if bond.maturity_year > last_acceptable_maturity_year:
                raise BondNotSuits(f"has duration more than {acceptable_years_to_maturity} years "
                                   f"(last acceptable year is {last_acceptable_maturity_year})")
            if bond.rate < 2:
                raise BondNotSuits("has rate less than 2")
            if bond.currency == "rub" and bond.yield_ < 5:
                raise BondNotSuits("has yield less than 5")
            if bond.currency == "usd" and bond.yield_ < 2:
                raise BondNotSuits("has yield less than 2")
            if bond.days_to_maturity < 45:
                raise BondNotSuits("Bond matures in less then 45 days.")
            bond.fake = 'fake_field'
            interest_bonds.append(bond)
        except KeyError as e:
            LOGGER.error(f"{bond} has no field {e}")
            continue
        except BondNotSuits as e:
            report_table.append(Row(bond.show_name, bond.ticker, repr(e)))

    LOGGER.debug("\n".join([
        "Rejection reasons: ",
        tabulate(report_table, headers=Row._fields)
    ]))
    return interest_bonds


@lru_cache()
def load_strategy_from_file(overall_cost) -> Strategy:
    return Strategy.load(overall_cost)


def _main():
    to_invest = {
        "any": 0 * RUB,
        "usd": 1500 * USD,
        "rub": 100_000 * RUB + 150_000 * RUB
    }
    free_cash = to_invest
    with Client.broker() as client:
        client.free_cash = lambda: free_cash

        Adviser(client).analyze_portfolio()


def get_bonds_for_iis():
    interest_bonds: List[Bond] = get_suitable_bonds('rub',
                                                    acceptable_years_to_maturity=2)
    yields = [d.yield_ for d in interest_bonds]

    order = np.argsort(yields)[::-1]
    bonds_ordered = [interest_bonds[i] for i in order]
    for bond in bonds_ordered:
        print(bond.yield_, bond.show_name, bond.ticker)


if __name__ == "__main__":
    configure_logging()
    _main()

    # find_shares_to_sell()

    # analyze_current_portfolio()

    # get_bonds_for_iis()

    # with Client.broker() as client:
    #     free_cash = client.free_cash()
    #     free_cash['rub'] = free_cash['rub'] * 100
    #     LOGGER.info(f"free cash: {', '.join(map(str, free_cash.values()))}.")
    #
    #     construct_positions_with_respect_to_strategy(free_cash, client)
