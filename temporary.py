from datetime import datetime

DO_NOT_BY_CURRENCIES = True  # Тинёк разгребает санкции

AVERAGE_POSITIONS_PRICES = {
    'MTSS': {
        'currency': 'rub',
        'value': 343,
    },
    'PARA': {
        'currency': 'usd',
        'value': 0,  # были получены в результате корпоративного события.
    },
    'US92556H2067': {
        'currency': 'usd',
        'value': 0,  # были получены в результате корпоративного события.
    },
    'POGR': {
        'currency': 'rub',
        'value': 10, # почти от балды взятая цена.
    }
}

NEVER_BOUGHT_SHARES = {
    'BBG011386VF4': {
        'description': "WBD (Warner Brothers.Discowery) отделилась от AT&T",
        'last bought date': datetime.fromisoformat('2022-04-19'),
    },
    'BBG004S681W1': {
        'description': "MTSS Получены в рамках принудительной конвертации АДР.",
        'last bought date': datetime.fromisoformat('2022-09-11'),
    },
}

EQUITY_WITHOUT_PRICES = {
    'RU000A0JY023': 445,
}
