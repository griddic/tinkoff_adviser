from pathlib import Path

import yaml


class Config:
    def __init__(self):
        self._secrets_loaded = None

    @property
    def _secrets_dir(self) -> Path:
        return Path(__file__).parent / "secrets"

    @property
    def decrypted_dir(self) -> Path:
        return self._secrets_dir / "decrypted"

    @property
    def encrypted_dir(self) -> Path:
        return self._secrets_dir / "encrypted"

    @property
    def _secrets(self):
        if self._secrets_loaded is None:
            with open(self.decrypted_dir / "secrets.yaml") as inn:
                self._secrets_loaded = yaml.safe_load(inn)
        return self._secrets_loaded

    @property
    def tinkoff_main_token(self):
        return self._secrets['tinkoff']['tokens']['main']

    @property
    def tinkoff_sandbox_token(self):
        return self._secrets['tinkoff']['tokens']['sandbox']

    @property
    def telegram_token(self):
        return self._secrets['telegram']['token']

    @property
    def telegram_user(self):
        return self._secrets['telegram']['user_id']

    @property
    def strategy(self):
        with open(self.decrypted_dir / "strategy.yaml") as inn:
            return yaml.safe_load(inn)

    @property
    def reserve_on_rounding(self):
        return 100

    @property
    def hold_for_taxes(self):
        return 20_000


CONFIG = Config()
