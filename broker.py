import logging
from decimal import Decimal
from enum import Enum
from pathlib import Path
from typing import List

import yaml

from money import Money
from open_api import Client

LOGGER = logging.getLogger(Path(__file__).name)


class OrderSide(Enum):
    SELL = "Sell"
    BUY = "Buy"


class OrderToPlace:
    def __init__(self,
                 *, figi, ticker, lots, price, side: OrderSide, lot_size, show_name, currency=None):
        self._figi = figi
        self._ticker = ticker
        self._lots = lots
        if isinstance(price, Money):
            self._price = price.amount
            self._currency = price.currency
        else:
            self._price = price
            self._currency = currency
        self._side = side
        self._lot_size = lot_size
        self._show_name = show_name

    @property
    def show_name(self):
        return self._show_name

    @property
    def price(self):
        return self._price

    @property
    def figi(self):
        return self._figi

    @property
    def ticker(self):
        return self._ticker

    @property
    def lots(self):
        return self._lots

    @property
    def side(self):
        return self._side

    @property
    def volume(self):
        return self._price * self._lot_size * self._lots

    @classmethod
    def sell(cls, *, figi, ticker, lots, price, lot_size, show_name, currency=None):
        return cls(figi=figi,
                   ticker=ticker,
                   lots=lots,
                   price=price,
                   side=OrderSide.SELL,
                   lot_size=lot_size,
                   currency=currency,
                   show_name=show_name,
                   )

    @classmethod
    def buy(cls, *, figi, ticker, lots, price, lot_size, show_name, currency=None):
        return cls(figi=figi,
                   ticker=ticker,
                   lots=lots,
                   price=price,
                   side=OrderSide.BUY,
                   lot_size=lot_size,
                   currency=currency,
                   show_name=show_name,
                   )

    @classmethod
    def from_dict(cls, data):
        instance = cls(
            figi=data['figi'],
            ticker=data['ticker'],
            lots=data['lots'],
            price=data['price'],
            side=OrderSide(data['side']),
            lot_size=data['lot_size'],
            currency=data['currency'],
            show_name=data['show_name'],
        )
        return instance

    @classmethod
    def from_yaml_file(cls, file_name):
        with open(file_name) as descriptor:
            data = yaml.safe_load(descriptor)
        return list(map(cls.from_dict, data))

    def validate(self):
        assert self._figi
        assert self._ticker
        assert self._price
        assert self._lots
        assert self._lots > 0
        assert self._side
        assert isinstance(self._side, OrderSide)
        assert self._lot_size > 0

    def to_dict(self):
        data = {
            'figi': self._figi,
            'ticker': self._ticker,
            'lots': self._lots,
            'price': self._price,
            'side': self._side.value,
            'lot_size': self._lot_size,
            'currency': self._currency,
            'show_name': self._show_name,
        }
        return data

    @classmethod
    def to_yaml_file(cls, orders, file_path):
        for order in orders:
            assert isinstance(order, cls)
            cls.validate(order)
        data = list(map(cls.to_dict, orders))
        with open(file_path, 'w') as out:
            yaml.dump(data, out)

    def __repr__(self):
        if self._currency:
            return f"{self._side.value}({self._ticker}, {Money(self.volume, self._currency)}, " \
                   f"{Money(self.price, self._currency)})"
        return f"{self._side.value}({self._ticker}, {self.volume}, {self.price})"


def log_exception(original_function):
    def wrapper(*args, **kwargs):
        try:
            original_function(*args, **kwargs)
        # pylint: disable=broad-except
        except Exception as e:
            LOGGER.error(e)

    return wrapper


class Broker:
    def __init__(self):
        self._client = Client.broker()

    @property
    def client(self):
        return self._client

    def adapt_price_to_instrument(self, order: OrderToPlace):
        assert isinstance(order, OrderToPlace)
        price = Decimal(str(order.price))

        instrument_info = self.client.get_instrument_by_figi(order.figi)
        increment = Decimal(str(instrument_info["minPriceIncrement"]))
        additional_increments = int((float(price) * 0.001) / float(increment))
        # new_price = price + increment * additional_increments
        new_price = increment * (additional_increments + price // increment)
        return float(new_price)

    @log_exception
    def place_order(self, order: OrderToPlace):
        price_adopted = self.adapt_price_to_instrument(order)

        self.client.place_limit_order(order.figi,
                                      order.side.value,
                                      order.lots,
                                      price_adopted)

    def place_orders(self, orders: List[OrderToPlace]):
        LOGGER.debug(f"Orders: {self.client.orders()}")
        for order in orders:
            self.place_order(order)
        LOGGER.debug(f"Orders: {self.client.orders()}")

    def place_orders_from_file(self, file_path):
        self.place_orders(OrderToPlace.from_yaml_file(file_path))
